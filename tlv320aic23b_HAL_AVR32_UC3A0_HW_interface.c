/**
 * \file
 *
 * \brief Hardware Abstraction Layer for codec tlv320aic23b running on\n
 * AVR32 UC3A0 architecture
 *
 * Contains basic low level functions which depend on user architecture and\n
 * hardware. Thanks to this layer is possible use common higher level driver\n
 * and apply it thru many architectures and devices. Also this allow easy\n
 * updates independent to higher layer (new features and so on).
 *
 * Created:  09.12.2013\n
 * Modified: 11.03.2014
 *
 * \version 1.0.2
 * \author Martin Stejskal
 */

#include "tlv320aic23b_HAL_AVR32_UC3A0_HW_interface.h"

//=================| Definition which user should not change |=================
/**
 * \brief Define when timeout occurs
 *
 * When waiting for send rest of TX data this constant define how long should\n
 * function should wait. Value should be calculated for most applications.\n
 * However user can set it explicitly here, but it is not recommended.
 */
#define TLV320AIC23B_TWI_TIMEOUT        \
  (250*(TLV320AIC23B_TWI_CLK_SOURCE/TLV320AIC23B_TWI_BAUDRATE))

//================================| Functions |================================
tlv320aic23b_status_t tlv320aic23b_HAL_init(void)
{
  // For backup IRQ flags
  uint32_t flags;

  // Variable as mask
  uint32_t   mask;

  // Pointer to TWI address
  volatile avr32_twi_t *p_twi;
  p_twi = TLV320AIC23B_TWI_DEVICE;


  //=================================| GPIO |==================================

  // Set pins for SPI0 device
  static const gpio_map_t TLV320AIC23B_GPIO_MAP = {
      {TLV320AIC23B_TWI_SDA_PIN,     TLV320AIC23B_TWI_SDA_FUNCTION},
      {TLV320AIC23B_TWI_SCL_PIN,    TLV320AIC23B_TWI_SCL_FUNCTION}
  };

  // Assign I/O to SPI
  if( gpio_enable_module(
      TLV320AIC23B_GPIO_MAP,
                sizeof(TLV320AIC23B_GPIO_MAP) / sizeof(TLV320AIC23B_GPIO_MAP[0]))
      != TLV320AIC23B_OK)
  {
    return TLV320AIC23B_ERROR_TWI_INVALID_PARAMETER;
  }

  //==================================| PM |===================================
  // Enable clock do device (in default should be on, but just for case)

  // Get and clear global interrupt
  flags = cpu_irq_save();

  /*
   * Poll MSKRDY before changing mask rather than after, as it's
   * highly unlikely to actually be cleared at this point.
   */
  while (!(AVR32_PM.poscsr & (1U << AVR32_PM_POSCSR_MSKRDY))) {
          /* Do nothing */
  }

  // Enable the clock to flash and PBA bridge
  mask = *(&AVR32_PM.cpumask + AVR32_PM_CLK_GRP_HSB);
  mask |= 1U << (AVR32_FLASHC_CLK_HSB % 32);
  mask |= 1U << (AVR32_HMATRIX_CLK_HSB_PBA_BRIDGE % 32);
  *(&AVR32_PM.cpumask + AVR32_PM_CLK_GRP_HSB) = mask;

  // Enable clock to TWI and GPIO in PBA
  mask = *(&AVR32_PM.cpumask + AVR32_PM_CLK_GRP_PBA);
  mask |= 1U << (AVR32_TWI_CLK_PBA % 32);
  mask |= 1U << (AVR32_GPIO_CLK_PBA % 32);
  *(&AVR32_PM.cpumask + AVR32_PM_CLK_GRP_PBA) = mask;

  // Restore global interrupt flags
  cpu_irq_restore(flags);

  //==================================| TWI |==================================
  // Configure TWI module

  // Control register

  // Reset TWI module
  p_twi->CR.swrst = 1;

  // Set start/stop conditions
  p_twi->CR.start = 1;
  p_twi->CR.stop  = 0;
  // Wait until TXCOMP will be 1
  while(!p_twi->SR.txcomp);
  // Now we can switch to master mode

  // Disable slave mode
  p_twi->CR.sven  = 0;
  p_twi->CR.svdis = 1;

  // Set master mode
  p_twi->CR.msdis = 0;
  p_twi->CR.msen  = 1;


  // Master mode register
  p_twi->MMR.iadrsz = 0;        // No internal device address
  p_twi->MMR.mread  = 0;        // Master write direction
  p_twi->MMR.dadr   = TLV320AIC23B_TWI_ADDRESS;

  // Slave mode register - no change

  // Internal address register - no change

  // Clock waveform generator register (set baudrate/speed)
  p_twi->CWGR.ckdiv = TLV320AIC23B_TWI_CKDIV;
  p_twi->CWGR.chdiv = TLV320AIC23B_TWI_CLHDIV;
  p_twi->CWGR.cldiv = TLV320AIC23B_TWI_CLHDIV;

  // Disable all interrupts
  p_twi->ier = 0;
  p_twi->idr = ~0;

  // OK, all should be ready

  return TLV320AIC23B_OK;
}



tlv320aic23b_status_t tlv320aic23b_HAL_send_data(uint16_t i_data)
{
  // Pointer to TWI address
  volatile avr32_twi_t *p_twi;
  p_twi = TLV320AIC23B_TWI_DEVICE;

  // Counter for timeout
  uint32_t i_cnt_timeout = 0;

  /* For backup original slave address (for case, that TWI will use another
   * driver.
   */
  uint8_t i_backup_address;


  // Wait until TX is ready and when TX is complete
  while(!p_twi->SR.txcomp)
  {
    // Increase timeout counter
    i_cnt_timeout++;
    // Test timeout counter
    if(i_cnt_timeout > TLV320AIC23B_TWI_TIMEOUT)
    {
      // If higher -> return error value
      return TLV320AIC23B_ERROR_TWI_TIMEOUT;
    }
  }
  // Reset counter - just for case
  i_cnt_timeout = 0;




  // Backup slave address
  i_backup_address = p_twi->MMR.dadr;

  // Set slave address - must calculate with option that TWI use another driver
  p_twi->MMR.dadr = TLV320AIC23B_TWI_ADDRESS;

  // Send first byte (MSB first)
  p_twi->THR.txdata = i_data>>8;


  // Wait until TX is done - during this, check for NACK (data)
  while(!p_twi->SR.txrdy)
  {
    // Again, check for timeout
    // Increase timeout counter
    i_cnt_timeout++;
    // Test timeout counter
    if(i_cnt_timeout > TLV320AIC23B_TWI_TIMEOUT)
    {
      // Restore slave address
      p_twi->MMR.dadr = i_backup_address;

      // If higher -> return error value
      return TLV320AIC23B_ERROR_TWI_TIMEOUT;
    }

    // Check for NACK
    if(p_twi->SR.nack)
    {
      // Restore slave address
      p_twi->MMR.dadr = i_backup_address;

      return TLV320AIC23B_ERROR_TWI_NACK;
    }
  }
  // Reset counter - just for case
  i_cnt_timeout = 0;



  // Send next byte (LSB last)
  p_twi->THR.txdata = i_data & 0xFF;

  // Wait until TX is done - during this, check for NACK (data)
  while(!p_twi->SR.txrdy)
  {
    // Again, check for timeout
    // Increase timeout counter
    i_cnt_timeout++;
    // Test timeout counter
    if(i_cnt_timeout > TLV320AIC23B_TWI_TIMEOUT)
    {
      // Restore slave address
      p_twi->MMR.dadr = i_backup_address;

      // If higher -> return error value
      return TLV320AIC23B_ERROR_TWI_TIMEOUT;
    }

    // Check for NACK
    if(p_twi->SR.nack)
    {
      // Restore slave address
      p_twi->MMR.dadr = i_backup_address;

      return TLV320AIC23B_ERROR_TWI_NACK;
    }
  }

  // Restore slave address
  p_twi->MMR.dadr = i_backup_address;

  return TLV320AIC23B_OK;
}




/*
tlv320aic23b_status_t tlv320aic23b_HAL_read_config(
                                                t_tlv_options *p_tlv_options)
{
  // So far there is not any implementation -> return error
  return TLV320AIC23B_ERROR;
}


tlv320aic23b_status_t tlv320aic23b_HAL_write_config(
                                                t_tlv_options *p_tlv_options)
{
  // So far there is not any implementation -> return error
  return TLV320AIC23B_ERROR;
}
*/

