/**
 * \file
 *
 * \brief Driver for audio code TLV320AIC23B
 *
 * Created  20.8.2013
 * Modified 25.2.2014
 *
 * Try avoid set "Simultaneous update" until you really know what are you\n
 * doing!
 *
 * \version 0.4.3
 * \author Martin Stejskal
 */


#ifndef _TLV320AIC23B_H_
#define _TLV320AIC23B_H_
	/* Tested settings:
	 * 48 kHz @ 16b @ 12.288 MHz
	 * In level: 0dB (not muted)
	 * Out level: 0dB (not muted, zero cross enabled)
	 * Analog path:
	 *   DAC enabled
	 *   Bypass in and out - disabled
	 *   Sidetone - disabled
	 *   input - line in
	 *   MIC muted
	 *   MIC boost - disabled
	 * Digital audio path:
	 *   DAC soft mute - disabled
	 *   De-emphasis control - disabled
	 *   ADC high-pass filter - disabled
	 * Power down control:
	 *   device - on
	 *   clock - on
	 *   oscillator - off (if you want crystal instead, set this to ON)
	 *   outputs - on
	 *   DAC - on
	 *   ADC - on
	 *   MIC input - on
	 *   LINE input - on
	 * Audio interface format
	 *   Master - true
	 *   DAC left/right swap - disabled
	 *   DAC left/right phase - disabled
	 *   Input bit length - 16 bit
	 *   Data format - I2S
	 * Sample rate:
	 *   CLK in  div - 1x (no change)
	 *   CLK out div - 1/2x
	 *   Sample rate - set according to datasheet (48 kHz)
	 *   Base oversampling rate - 256
	 *   USB/normal - normal
	 */
/*-----------------------------------------*
 |               Options                   |
 *-----------------------------------------*/
//! \brief Address of TLV codec
#define TLV_I2C_ADDRESS				0b0011010

//! \brief Define where settings will be saved (first word, length is 20B)
#define TLV_EEPROM_START_ADDRESS	0x0001

/**
 * \name Basic TLV codec settings
 *
 * @}
 */
/// \brief Default value for input volume (0b10111 = 0dB)
#define TLV_DEFAULT_VOLUME_IN		0b10111
/**
 * \brief Left input mute
 *
 * Options: true, false
 */
#define TLV_DEFAULT_L_IN_MUTE					false
/**
 * \brief Right input mute
 *
 * Options: true, false
 */
#define TLV_DEFAULT_R_IN_MUTE					false

/// \brief Default value for output volume (0b1111001 = 0dB ; min: 0b0110001)
#define TLV_DEFAULT_VOLUME_OUT		0b1010101
/**
 * \brief Left headphone mute
 *
 * Options: true, false
 */
#define TLV_DEFAULT_L_HEADPHONE_MUTE			false
/**
 * \brief Right headphone mute
 *
 * Options: true, false
 */
#define TLV_DEFAULT_R_HEADPHONE_MUTE			false
/**
 * \brief Microphone mute
 *
 * Options: true, false
 */
#define TLV_DEFAULT_MIC_MUTE					true
/**
 * \brief Microphone boost 20dB
 *
 * Options: true, false
 */
#define TLV_DEFAULT_MIC_BOOST					false
/**
 * \brief Default codec input select
 *
 * Options: TLV_LINE_IN_CH, TLV_MIC_CH
 */
#define TLV_DEFAULT_INPUT_SELECT				TLV_LINE_IN_CH



/// @}





/**
 * \brief Option for generic driver support (true 1, false 0)
 *
 * Metadata for generic driver: TLV320AIC23_metadata
 */
#define TLV_support_generic_driver				true

/// \brief Option for LCD display NOKIA 5110
#define TLV_support_N5110_LCD					false










/**
 * \name Advanced default codec settings
 *
 * Because cadec offer many options it is good choice to define default values.
 *
 * @{
 */
/**
 * \brief Data format
 *
 *  Options: TLV_FRMT_MSB_FIRST_RIGHT_ALIGNED,\n
 *  TLV_FRMT_MSB_FIRST_LEFT_ALIGNED, TLV_FRMT_I2S_MSB_FIRST, TLV_FRMT_DSP
 */
#define TLV_DEFAULT_DATA_FORMAT					TLV_FRMT_I2S_MSB_FIRST

/**
 * \brief Bit length (word width)
 *
 * Options: TLV_16BIT, TLV_20BIT, TLV_24BIT, TLV_32BIT
 */
#define TLV_DEFAULT_INPUT_BIT_LENGTH			TLV_16BIT

/**
 * \brief Master/slave switch
 *
 * There programmer have two options:\n
 * 1) TLV will operate as master -> set true\n
 * 2) TLV will operate as slave  -> set false\n
 * \n
 * Important note: because is possible to change TLV master/slave mode, in\n
 * some designs that means to set I/O pins correctly. So, please, look to\n
 * your device board driver to avoid complications ;)
 */
#define TLV_DEFAULT_MASTER_MODE						false

/**
 * \brief Define witch sample rate state is default
 *
 * For complete list please search for "Sample rate states" in .h file
 */
#define TLV_DEFAULT_SAMPLE_RATE_STATE	\
	TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_48kHz_DAC_48kHz
//	TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_44_1kHz_DAC_44_1kHz

/**
 * \brief Device power
 *
 * Options: TLV_PWR_ON (device will be on), TLV_PWR_OFF (power down mode)
 */
#define TLV_DEFAULT_DEVICE_PWR						TLV_PWR_ON
/**
 * \brief Clock power
 *
 * Options: TLV_PWR_ON, TLV_PWR_OFF
 */
#define TLV_DEFAULT_CLOCK_PWR						TLV_PWR_ON
/**
 * \brief Oscillator power
 *
 * Options: TLV_PWR_ON, TLV_PWR_OFF
 */
#define TLV_DEFAULT_OSCILLATOR_PWR					TLV_PWR_OFF
/**
 * \brief Outputs power
 *
 * Options: TLV_PWR_ON, TLV_PWR_OFF
 */
#define TLV_DEFAULT_OUTPUTS_PWR						TLV_PWR_ON
/**
 * \brief DAC power
 *
 * Options: TLV_PWR_ON, TLV_PWR_OFF
 */
#define TLV_DEFAULT_DAC_PWR							TLV_PWR_ON
/**
 * \brief ADC power
 *
 * Options: TLV_PWR_ON, TLV_PWR_OFF
 */
#define TLV_DEFAULT_ADC_PWR							TLV_PWR_ON
/**
 * \brief Microphone input power
 *
 * Options: TLV_PWR_ON, TLV_PWR_OFF
 */
#define TLV_DEFAULT_MIC_INPUT_PWR					TLV_PWR_OFF
/**
 * \brief Line input power
 */
#define TLV_DEFAULT_LINE_INPUT_PWR					TLV_PWR_ON
/**
 * \brief Clock divider for MCLK coming to pin XTI/MCLK
 *
 * Options: true (divider 2), false (no divider)
 */
#define TLV_DEFAULT_CLK_IN_DIVIDER				false
/**
 * \brief Clock divider for CLKOUT pin
 *
 * Options: true (divider 2), false (no divider)
 */
#define TLV_DEFAULT_CLK_OUT_DIVIDER					true
/**
 * \brief Left output headphone zero cross
 *
 * Options: true, false
 */
#define TLV_DEFAULT_L_OUT_HEADPHONE_ZERO_CROSS		true
/**
 * \brief Right output headphone zero cross
 *
 * Options: true, false
 */
#define TLV_DEFAULT_R_OUT_HEADPHONE_ZERO_CROSS		true
/**
 * \brief Bypass ("connect") input and output
 *
 * Options: true, false
 */
#define TLV_DEFAULT_BYPASS_IN_OUT					false
/**
 * \brief Sidetone from microphone
 *
 * Options: TLV_SIDETONE_DISABLED, TLV_SIDETONE_MINUS_18dB,\n
 * TLV_SIDETONE_MINUS_12dB, TLV_SIDETONE_MINUS_9dB, TLV_SIDETONE_MINUS_6dB,\n
 * TLV_SIDETONE_0dB
 */
#define TLV_DEFAULT_SIDETONE						TLV_SIDETONE_DISABLED

/**
 * \brief DAC soft mute
 *
 * Options: true, false
 */
#define TLV_DEFAULT_DAC_SOFT_MUTE					false
/**
 * \brief De-emphasis
 *
 * Options: TLV_DEEMP_DISABLED, TLV_DEEMP_32kHz, TLV_DEEMP_44_1kHz,\n
 * TLV_DEEMP_48kHz
 */
#define TLV_DEFAULT_DEEMP							TLV_DEEMP_DISABLED
/**
 * \brief ADC high-pass filter
 *
 * Options: true, false
 */
#define TLV_DEFAULT_ADC_HP_FILTER					false
/**
 * \brief DAC swap left and right channel
 *
 * Options: true, false
 */
#define TLV_DEFAULT_SWAP_LR							false
/**
 * \brief DAC left/right phase
 *
 * Options: true (1), false (0)\n
 * 0 - Right channel on, LRCIN high\n
 * 1 - Right channel on, LRCIN low\n
 * DSP mode\n
 * 0 = MSB is available on 1st BCLK rising edge after LRCIN rising edge\n
 * 1 = MSB is available on 2nd BCLK rising edge after LRCIN rising edge *
 */
#define TLV_DEFAULT_PHASE_LR						false
/**
 * \brief DAC select (Analog audio path)
 *
 * Options: true (recommended), false
 */
#define TLV_DEFAULT_DAC_SELECT						true
/**
 * \brief Interface activation
 *
 * Options: true (recommended), false
 */
#define TLV_DEFAULT_INTERFACE_ACTIVATION			true

/// @}


/*-----------------------------------------*
 |               Includes                  |
 *-----------------------------------------*/
// Operations with EEPROM
#include <avr/eeprom.h>

// Program memory options
#include <avr/pgmspace.h>

// Timing - sometimes is need time delay between packets
#include <util/delay.h>

// Data types (expected in I2C driver too, but just for case)
#include <inttypes.h>

// Sending data thru hardware I2C
#include "ALPS_I2C_8bit.h"

// Basic bit operations, that can simplify program
//#include "bit_operations.h"

#if TLV_support_N5110_LCD == true
// When error occurs, then is shown to LCD
#include "ALPS_LCD_5110_SPI_SW_emulated_8bit.h"
#endif

#if TLV_support_generic_driver == true
// If used generic driver, get actual structures
#include "ALPS_generic_driver.h"
// Also say compiler, that there exist settings on flash
extern const gd_config_struct TLV320AIC23_config_table_flash[] PROGMEM;
// And metadata
extern const gd_metadata TLV320AIC23_metadata;

#else
/**
 * \brief Error codes common with generic driver
 *
 * Please note, that this enum MUST be same with as enum in generic driver!
 */
#ifndef _GENERIC_DRIVER_H_
typedef enum{
	GD_SUCCESS =                   0,
	GD_FAIL =                      1,
	GD_INCORRECT_PARAMETER =       2,
	GD_INCORRECT_CMD_ID =          3,
	GD_CMD_ID_NOT_EQUAL_IN_FLASH = 4,
	GD_INCORRECT_DEVICE_ID =       5
} GD_RES_CODE;
#endif
#endif
/*-----------------------------------------*
 |               Definitions               |
 *-----------------------------------------*/
/**
 * \brief Define minimum software delay between packets
 * If delay is too low, communication over I2C can failed - TLV does not have\n
 * enough time to save data to registers).
 */
#define TLV_PACKET_DELAY			_delay_us(50)

/**
 * \brief Calculated last used EEPROM address by this driver
 *
 * We have 10 configure words. Plus one for virtual values. Every word is\n
 * 16 bit wide (2B) -> 10*2. But indexing start from 0 and program read\n
 * 16 bits (2B), so final value is decreased by 2.
 */
#define TLV_EEPROM_END_ADDRESS		(TLV_EEPROM_START_ADDRESS + (11*2)-2)
/**
 * \name TLV320AIC23B register addresses
 *
 * Because addresses for codec registers are 7 bit and their width is 9 bit\n
 * communication is a little bit complex. So, there are names for registers
 *
 * @{
 */
//! \brief Left line input channel volume control
#define TLV_REG_L_IN_VOL		0
//! \brief Right line input channel volume control
#define TLV_REG_R_IN_VOL		1
//! \brief Left channel headphone volume control
#define TLV_REG_L_OUT_VOL		2
//! \brief Right channel headphone volume control
#define TLV_REG_R_OUT_VOL		3
//! \brief Analog audio path control
#define TLV_REG_ANA_PATH		4
//! \brief Digital audio path control
#define TLV_REG_DIG_PATH		5
//! \brief Power down control
#define TLV_REG_PWR_DWN			6
//! \brief Digital audio interface format
#define TLV_REG_INTERFCE_FRMT	7
//! \brief Sample rate control
#define TLV_REG_SMPL_RATE		8
//! \brief Digital interface activation
#define TLV_REG_INTERFCE_ACTIV	9
//! \brief Reset
#define TLV_REG_RESET			15

/** \brief Define how many rotations must be done to shift address to "right\n
 * place". Also this number is width for data
 */
#define TLV_REG_SHIFT			9
/// @}


/// \brief Define value, when codec set headphone output as muted
#define TLV_HEADPHONE_MUTE_VALUE		0b0110000

/**
 * \name Bit positions in TLV register (and EEPROM memory) and bit masks
 *
 * Instead of writing bit per bit we can use symbolic names. Also there are\n
 * defined bit masks for converting data from TLV codec format (same format\n
 * is used for EEPROM) to t_tlv_options structure. Mask just define how many\n
 * LSB bits are valid after shift by TLV_xyz_BIT
 *
 * @{
 */
//! \brief Check bit in EEPROM
#define TLV_EEPROM_CHECK_BIT	15
//! \brief Left/right simultaneous volume/mute update: 0 = Disabled
#define TLV_LRS_BIT			8
#define TLV_LRS_MASK		0x0001
//! \brief Left line input mute 0 = Normal 1 = Muted
#define TLV_LIM_BIT			7
#define TLV_LIM_MASK		0x0001
//! \brief Left line input volume control (5 bits ; 10111 = 0 dB default)
#define TLV_LIV_BIT			0
#define TLV_LIV_MASK		0x001F
//-----------------------------------------------------------------------------
//! \brief Right/left simultaneous volume/mute update: 0 = Disabled
#define TLV_RLS_BIT			8
#define TLV_RLS_MASK		0x0001
//! \brief Right line input mute 0 = Normal 1 = Muted
#define TLV_RIM_BIT			7
#define TLV_RIM_MASK		0x0001
//! \brief Right line input volume control (5 bits ; 10111 = 0 dB default)
#define TLV_RIV_BIT			0
#define TLV_RIV_MASK		0x001F
//-----------------------------------------------------------------------------
//! \brief Left-channel zero-cross detect. Zero-cross detect 0 = Off 1 = On
#define TLV_LZC_BIT			7
#define TLV_LZC_MASK		0x0001
//! \brief Left Headphone volume control (1111001 = 0 dB default)
#define TLV_LHV_BIT			0
#define TLV_LHV_MASK		0x007F
//-----------------------------------------------------------------------------
//! \brief Right-channel zero-cross detect. Zero-cross detect 0 = Off 1 = On
#define TLV_RZC_BIT			7
#define TLV_RZC_MASK		0x0001
//! \brief Right headphone volume control (1111001 = 0 dB default)
#define TLV_RHV_BIT			0
#define TLV_RHV_MASK		0x007F
//-----------------------------------------------------------------------------
//! \brief STA[2:0] and STE define added sidetone. Please refer datasheet (p22)
#define TLV_STA_BIT			6
#define TLV_STA_MASK		0x0007
//! \brief STA[2:0] and STE define added sidetone. Please refer datasheet (p22)
#define TLV_STE_BIT			5
#define TLV_STE_MASK		0x0001
//! \brief STA[2:0] and STE together (can be used for one configure variable)
#define TLV_STE_STA_BIT		5
#define TLV_STE_STA_MASK	0x000F
//! \brief DAC select: 0 = DAC off 1 = DAC selected
#define TLV_DAC_BIT			4
#define TLV_DAC_MASK		0x0001
//! \brief Bypass: 0 = Disabled 1 = Enabled
#define TLV_BYP_BIT			3
#define TLV_BYP_MASK		0x0001
//! \brief Input select for ADC 0 = Line 1 = Microphone
#define TLV_INSEL_BIT		2
#define TLV_INSEL_MASK		0x0001
//! \brief Microphone mute 0 = Normal 1 = Muted
#define TLV_MICM_BIT		1
#define TLV_MICM_MASK		0x0001
//! \brief Microphone boost 0=dB 1 = 20dB
#define TLV_MICB_BIT		0
#define TLV_MICB_MASK		0x0001
//-----------------------------------------------------------------------------
//! \brief DAC soft mute: 0 = Disabled 1 = Enabled
#define TLV_DACM_BIT		3
#define TLV_DACM_MASK		0x0001
/** \brief De-emphasis control 00 = Disabled
 * 01 = 32 kHz 10 = 44.1 kHz 11 = 48 kHz (2 bits)
 */
#define TLV_DEEMP_BIT		1
#define TLV_DEEMP_MASK		0x0003
//! \brief ADC high-pass filter 1 = Disabled 0 = Enabled
#define TLV_ADCHP_BIT		0
#define TLV_ADCHP_MASK		0x0001
//-----------------------------------------------------------------------------
//! \brief Device power 0 = On 1 = Off
#define TLV_PWR_OFF_BIT			7
#define TLV_PWR_OFF_MASK		0x0001
//! \brief Clock 0 = On 1 = Off
#define TLV_PWR_CLK_BIT			6
#define TLV_PWR_CLK_MASK		0x0001
//! \brief Oscillator 0 = On 1 = Off
#define TLV_PWR_OSC_BIT			5
#define TLV_PWR_OSC_MASK		0x0001
//! \brief Outputs 0 = On 1 = Off
#define TLV_PWR_OUT_BIT			4
#define TLV_PWR_OUT_MASK		0x0001
//! \brief DAC 0 = On 1 = Off
#define TLV_PWR_DAC_BIT			3
#define TLV_PWR_DAC_MASK		0x0001
//! \brief ADC 0 = On 1 = Off
#define TLV_PWR_ADC_BIT			2
#define TLV_PWR_ADC_MASK		0x0001
//! \brief Microphone input 0 = On 1 = Off
#define TLV_PWR_MIC_BIT			1
#define TLV_PWR_MIC_MASK		0x0001
//! \brief Line input 0 = On 1 = Off
#define TLV_PWR_LINE_BIT		0
#define TLV_PWR_LINE_MASK		0x0001
//-----------------------------------------------------------------------------
//! \brief Master/slave mode 0 = Slave 1 = Master
#define TLV_MS_BIT			6
#define TLV_MS_MASK			0x0001
//! \brief DAC left/right swap 0 = Disabled 1 = Enabled
#define TLV_LRSWAP_BIT		5
#define TLV_LRSWAP_MASK		0x0001
/**
 * \brief DAC left/right phase:\n
 * 0 = Right channel on, LRCIN high\n
 * 1 = Right channel on, LRCIN low
 */
#define TLV_LRP_BIT			4
#define TLV_LRP_MASK		0x0001
//! \brief Input bit length 00 = 16 bit 01 = 20 bit 10 = 24 bit 11 = 32 bit
#define TLV_IWL_BIT			2
#define TLV_IWL_MASK		0x0003
/**
 * \brief Data format\n
 * 11 = DSP format, frame sync followed by two data words\n
 * 10 = I2S format, MSB first, left � 1 aligned\n
 * 01 = MSB first, left aligned\n
 * 00 = MSB first, right aligned\n
 */
#define TLV_FORMAT_BIT		0
#define TLV_FORMAT_MASK		0x0003
//-----------------------------------------------------------------------------
//! \brief Clock input divider 0 = MCLK 1 = MCLK/2
#define TLV_CLKIN_BIT		6
#define TLV_CLKIN_MASK		0x0001
//! \brief Clock output divider 0 = MCLK 1 = MCLK/2
#define TLV_CLKOUT_BIT		7
#define TLV_CLKOUT_MASK		0x0001
/**
 * \brief Sampling rate control (see Sections 3.3.2.1 AND 3.3.2.2 in \n
 * datasheet). 4 bits
 */
#define TLV_SR_BIT			2
#define TLV_SR_MASK			0x000F
/**
 * \brief Base oversampling rate:\n
 * USB mode: 0 = 250 fs 1 = 272 fs\n
 * Normal mode: 0 = 256 fs 1 = 384 fs\n
 */
#define TLV_BOSR_BIT		1
#define TLV_BOSR_MASK		0x0001
//! \brief USB/Normal Clock mode select: 0 = Normal 1 = USB
#define TLV_USB_NORMAL_BIT	0
//-----------------------------------------------------------------------------
//! \brief Activate digital interface
#define TLV_ACT_BIT			0
#define TLV_ACT_MASK		0x0001
/// @}


/**
 * \name Error codes for TLV
 *
 * \brief Overview of error codes
 *
 * @{
 */
//! \brief No error - all OK
#define TLV_OK							0
//! \brief Data in EEPROM not valid (TLV_EEPROM_CHECK_BIT not clear)
#define TLV_ERROR_DATA_IN_EEPROM_NOT_VALID		4
//! \brief Invalid value of input parameter
#define TLV_ERROR_INVALID_INPUT_PARAM			5
/// @}

/**
 * \name Names for left, right and both channels
 *
 * @{
 */
//! \brief Name for left channel
#define TLV_LEFT		1
//! \brief Name for right channel
#define TLV_RIGHT		2
//! \brief Name for both channels
#define TLV_BOTH		3
/// @}
/**
 * \name Symbolic name for inputs
 *
 * @{
 */
//! \brief Name for line in channel
#define TLV_LINE_IN_CH	0
//! \brief Name for mic channel
#define TLV_MIC_CH		1
/**
 * @}
 *
 * \name Power states
 *
 * @{
 */
#define TLV_PWR_ON			0
#define TLV_PWR_OFF			1
/// @}
/**
 * \name Input bit length
 *
 * For user comfort there are named binary values for codec settings
 * @{
 */
/// \brief 16 bit wide samples
#define TLV_16BIT		0b00
/// \brief 20 bit wide samples
#define TLV_20BIT		0b01
/// \brief 24 bit wide samples
#define TLV_24BIT		0b10
/// \brief 32 bit wide samples
#define TLV_32BIT		0b11
/// @}
/**
 * \name Data format
 *
 * Symbolic names for data format
 *
 * @{
 */
/// \brief MSB first, right aligned
#define TLV_FRMT_MSB_FIRST_RIGHT_ALIGNED		0b00
/// \brief MSB first, left aligned
#define TLV_FRMT_MSB_FIRST_LEFT_ALIGNED			0b01
/// \brief I2S format, MSB first, left - 1 aligned
#define TLV_FRMT_I2S_MSB_FIRST					0b10
/// \brief DSP format, frame sync followed by two data words
#define TLV_FRMT_DSP							0b11
/// @}
/**
 * \name Sampling rate modes
 *
 * @{
 */
/// \brief Clock for TLV - normal mode
#define TLV_SR_MODE_NORMAL						0
/// \brief Clock for TLV - USB mode
#define TLV_SR_MODE_USB							1
/// @}
/**
 * \name Sidetone options (for TLV register)
 *
 * @{
 */
/// \brief Sidetone disabled
#define TLV_SIDETONE_DISABLED					0b0000
/// \brief Sidetone -18 dB
#define TLV_SIDETONE_MINUS_18dB					0b1011
/// \brief Sidetone -12 dB
#define TLV_SIDETONE_MINUS_12dB					0b1010
/// \brief Sidetone -9  dB
#define TLV_SIDETONE_MINUS_9dB					0b1001
/// \brief Sidetone -6  dB
#define TLV_SIDETONE_MINUS_6dB					0b1000
/// \brief Sidetone 0dB
#define TLV_SIDETONE_0dB						0b1100
/// @}
/**
 * \name De-emphasis options
 *
 * @{
 */
/// \brief De-emphasis disabled
#define TLV_DEEMP_DISABLED						0b00
/// \brief De-emphasis 32 kHz
#define TLV_DEEMP_32kHz							0b01
/// \brief De-emphasis 44.1 kHz
#define TLV_DEEMP_44_1kHz						0b10
/// \brief De-emphasis 48 kHz
#define TLV_DEEMP_48kHz							0b11
/// @}
/**
 * \name Sample rate states
 *
 * Because driver do not know on witch frequency will codec work, driver can\n
 * not determine possible sample rates. So there is list of possible sample\n
 * rate states and user select one of them (because user know on witch clock\n
 * will codec run). Number of every option is basically command ID. When this\n
 * number is used as command ID, called function set sample rate parameters\n
 * according this variable. For example if\n
 * TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_96kHz_DAC_96kHz is defined as 9, then\n
 * when user call set_settings(metadata, 9, 0) in generic driver, called\n
 * function set configuration bits for MCLK 12 MHz, Fadc = 96 kHz and\n
 * Fdac = 96 kHz.
 *
 * @{
 */
/// \brief MCLK = 12      MHz, Fadc = 96    kHz, Fdac = 96    kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_96kHz_DAC_96kHz	17	// CMD ID
/// \brief Define first option for sample rate list
#define TLV_SAMPLE_RATE_FIRST_OPTION	\
		TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_96kHz_DAC_96kHz

/// \brief MCLK = 12      MHz, Fadc = 88.2  kHz, Fdac = 88.2  kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_88_2kHz_DAC_88_2kHz	\
	TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_96kHz_DAC_96kHz+1

/// \brief MCLK = 12      MHz, Fadc = 48    kHz, Fdac = 48    kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_48kHz_DAC_48kHz		\
	TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_88_2kHz_DAC_88_2kHz+1

/// \brief MCLK = 12      MHz, Fadc = 44.1  kHz, Fdac = 44.1  kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_44_1kHz_DAC_44_1kHz	\
	TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_48kHz_DAC_48kHz+1

/// \brief MCLK = 12      MHz, Fadc = 32    kHz, Fdac = 32    kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_32kHz_DAC_32kHz		\
	TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_44_1kHz_DAC_44_1kHz+1

/// \brief MCLK = 12      MHz, Fadc = 8.021 kHz, Fdac = 8.021 kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8_021kHz_DAC_8_021kHz	\
	TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_32kHz_DAC_32kHz+1

/// \brief MCKL = 12      MHz, Fadc = 8     kHz, Fdac = 8     kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8kHz_DAC_8kHz			\
	TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8_021kHz_DAC_8_021kHz+1

/// \brief MCLK = 12      MHz, Fadc = 48    kHz, Fdac = 8     kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_48kHz_DAC_8kHz			\
	TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8kHz_DAC_8kHz+1

/// \brief MCLK = 12      MHz, Fadc = 44.1  kHz, Fdac = 8.021 kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_44_1kHz_DAC_8_021kHz	\
		TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_48kHz_DAC_8kHz+1

/// \brief MCLK = 12      MHz, Fadc = 8     kHz, Fdac = 48    kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8kHz_DAC_48kHz			\
	TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_44_1kHz_DAC_8_021kHz+1

/// \brief MCLK = 12      MHz, Fadc = 8.021 kHz, Fdac = 44.1  kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8_021kHz_DAC_44_1kHz	\
	TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8kHz_DAC_48kHz+1

/// \brief MCLK = 12.288  MHz, Fadc = 96    kHz, Fdac = 96    kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_96kHz_DAC_96kHz	\
	TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8_021kHz_DAC_44_1kHz+1

/// \brief MCLK = 12.288  MHz, Fadc = 48    kHz, Fdac = 48    kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_48kHz_DAC_48kHz	\
	TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_96kHz_DAC_96kHz+1

/// \brief MCLK = 12.288  MHz, Fadc = 32    kHz, Fdac = 32    kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_32kHz_DAC_32kHz	\
	TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_48kHz_DAC_48kHz+1

/// \brief MCLK = 12.288  MHz, Fadc = 8     kHz, Fadc = 8     kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_8kHz_DAC_8kHz		\
	TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_32kHz_DAC_32kHz+1

/// \brief MCLK = 12.288  MHz, Fadc = 48    kHz, Fdac = 8     kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_48kHz_DAC_8kHz		\
	TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_8kHz_DAC_8kHz+1

/// \brief MCLK = 12.288  MHz, Fadc = 8     kHz, Fdac = 48    kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_8kHz_DAC_48kHz		\
	TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_48kHz_DAC_8kHz+1

/// \brief MCLK = 11.2896 MHz, Fadc = 88.2  kHz, Fdac = 88.2  kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_88_2kHz_DAC_88_2kHz	\
	TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_8kHz_DAC_48kHz+1

/// \brief MCLK = 11.2896 MHz, Fadc = 44.1  kHz, Fdac = 44.1  kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_44_1kHz_DAC_44_1kHz	\
	TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_88_2kHz_DAC_88_2kHz+1

/// \brief MCLK = 11.2896 MHz, Fadc = 8.021 kHz, Fdac = 8.021 kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_8_021kHz_DAC_8_021kHz	\
	TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_44_1kHz_DAC_44_1kHz+1

/// \brief MCLK = 11.2896 MHz, Fadc = 44.1  kHz, Fdac = 8.021 kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_44_1kHz_DAC_8_021kHz	\
	TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_8_021kHz_DAC_8_021kHz+1

/// \brief MCLK = 11.2896 MHz, Fadc = 8.021 kHz, Fdac = 44.1  kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_8_021kHz_DAC_44_1kHz	\
	TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_44_1kHz_DAC_8_021kHz+1

/// \brief MCLK = 18.432  MHz, Fadc = 96    kHz, Fdac = 96    kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_96kHz_DAC_96kHz		\
	TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_8_021kHz_DAC_44_1kHz+1

/// \brief MCLK = 18.432  MHz, Fadc = 48    kHz, Fdac = 48    kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_48kHz_DAC_48kHz		\
	TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_96kHz_DAC_96kHz+1

/// \brief MCLK = 18.432  MHz, Fadc = 32    kHz, Fdac = 32    kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_32kHz_DAC_32kHz		\
	TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_48kHz_DAC_48kHz+1

/// \brief MCLK = 18.432  MHz, Fadc = 8     kHz, Fdac = 8     kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_8kHz_DAC_8kHz			\
	TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_32kHz_DAC_32kHz+1

/// \brief MCLK = 18.432  MHz, Fadc = 48    kHz, Fdac = 8     kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_48kHz_DAC_8kHz			\
	TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_8kHz_DAC_8kHz+1

/// \brief MCLK = 18.432  MHz, Fadc = 8     kHz, Fdac = 48    kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_8kHz_DAC_48kHz			\
	TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_48kHz_DAC_8kHz+1

/// \brief MCLK = 16.9344 MHz, Fadc = 88.2  kHz, Fdac = 88.2  kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_88_2kHz_DAC_88_2kHz		\
	TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_8kHz_DAC_48kHz+1

/// \brief MCLK = 16.9344 MHz, Fadc = 44.1  kHz, Fdac = 44.1  kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_44_1kHz_DAC_44_1kHz		\
	TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_88_2kHz_DAC_88_2kHz+1

/// \brief MCLK = 16.9344 MHz, Fadc = 8.021 kHz, Fdac = 8.021 kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_8_021kHz_DAC_8_021kHz		\
	TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_44_1kHz_DAC_44_1kHz+1

/// \brief MCLK = 16.9344 MHz, Fadc = 44.1  kHz, Fdac = 8.021 kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_44_1kHz_DAC_8_021kHz		\
	TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_8_021kHz_DAC_8_021kHz+1

/// \brief MCLK = 16.9344 MHz, Fadc = 8.021 kHz, Fdac = 44.1  kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_8_021kHz_DAC_44_1kHz		\
	TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_44_1kHz_DAC_8_021kHz+1

/// \brief Define last option in sample rate list
#define TLV_SAMPLE_RATE_LAST_OPTION	\
		TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_8_021kHz_DAC_44_1kHz
/// @}
/*-----------------------------------------*
 |              Structures                 |
 +-----------------------------------------+
 |             Global variables            |
 *-----------------------------------------*/
/**
 * \brief Structure for TLV flags and settings
 *
 * Because saving configure flags as bytes would consume a lot of memory,\n
 * it is better use structure with masking flags.\n
 * Because it is hard to rewrite whole code to give functions this structure\n
 * as argument, this value is global (at least for this file). In the end it\n
 * save a little memory, because this variable is there "for ever" in\n
 * application run.
 */
typedef struct{
	// LEFT INPUT
	uint8_t i_L_in_volume;
	uint8_t i_L_in_mute;

	// RIGHT INPUT
	uint8_t i_R_in_volume;
	uint8_t i_R_in_mute;

	// LEFT OUTPUT
	uint8_t i_L_out_headphone_volume;
	uint8_t i_L_out_headphone_zero_cross;

	// RIGHT OUTPUT
	uint8_t i_R_out_headphone_volume;
	uint8_t i_R_out_headphone_zero_cross;

	// --- Analog audio path ---
	uint8_t i_dac_select;
	uint8_t i_bypass_in_to_out;
	uint8_t i_mic_sidetone_to_in_and_out;
	uint8_t i_input_select;
	uint8_t i_mic_mute;
	uint8_t i_mic_boost_20dB;

	// --- Digital audio path ---
	uint8_t i_dac_soft_mute;
	uint8_t i_deemp;
	uint8_t i_adc_high_pass_filter;

	// --- Power down control ---
	uint8_t i_device_pwr_down;
	uint8_t i_clock_pwr_down;
	uint8_t i_oscillator_pwr_down;
	uint8_t i_outputs_pwr_down;
	uint8_t i_dac_pwr_down;
	uint8_t i_adc_pwr_down;
	uint8_t i_mic_pwr_down;
	uint8_t i_line_pwr_down;

	// --- Audio interface format ---
	uint8_t i_master_mode;
	uint8_t i_dac_left_right_swap;
	// DAC LR PHASE -> please refer TLV320AIC23B datasheet
	uint8_t i_dac_left_right_phase;
	uint8_t i_input_bit_length;
	uint8_t i_data_format;

	// --- SAMPLE RATE ---
	uint8_t i_clk_in_divider;
	uint8_t i_clk_out_divider;
	// SAMPLING RATE CONTROLL -> please refer TLV320AIC23B datasheet
	uint8_t i_sampling_rate;
	// BASE OVERSAMPLING RATE -> please refer TLV320AIC23B datasheet
	uint8_t i_base_oversampling_rate;
	// CLOCK MODE SELECT -> please refer TLV320AIC23B datasheet
	uint8_t i_clock_mode_select;	// 0 - normal ; 1 - USB

	// --- Digital interface activation ---
	uint8_t i_digital_interface_activation;
}t_tlv_options;



typedef struct{
	// --- Additional variables - not directly connected with TLV registers ---

	/* Define in witch state is actual settings is. This number is just command
	 * ID, so GUI just call get_settings(cmd id any of sample rate state)
	 * and as value get command ID. When call get_settings(gotten cmd ID)
	 * user get description of actual settings
	 */
	uint8_t i_sampling_rate_state;
	/* Mute flag for left and right headphone. Because mute flag is not in
	 * codec, driver make own virtual mute flag, so it can easily offer generic
	 * driver simple information, instead of volume value
	 */
	uint8_t i_L_headphone_mute_flag;
	uint8_t i_R_headphone_mute_flag;

	/* Because sidetone value on codec is not easy to set (bit combination is
	 * little bit tricky), there is virtual variable in witch will be settings
	 * more transparent. Functions then recalculate this value to value
	 * suitable for TLV register
	 */
	uint8_t i_mic_sidetone_to_in_and_out_virtual;
}t_tlv_virtual_options;

/*-----------------------------------------*
 |                  Macros                 |
 *-----------------------------------------*/
/** \brief Check if i_status is not equal to I2C_OK (0). If yes, then there\n
 * was some problem -> print error code and return
 */
#if TLV_support_N5110_LCD == true
#define TLV_CHECK_I2C_STATUS	\
if(i_status != I2C_OK)\
{\
	LCD_write_string_and_uint8_dec("I2C ERROR:", i_status);\
	return GD_FAIL;\
}\
TLV_PACKET_DELAY

#else
#define TLV_CHECK_I2C_STATUS	\
if(i_status != I2C_OK)\
{\
	return GD_FAIL;\
}\

#endif
/**
 * \brief When user input invalid parameter this macro can be used
 */
#if TLV_support_N5110_LCD == true
#define TLV_MACRO_INVALID_INPUT_PARAM	\
		LCD_write_string_and_uint8_dec("TLV INV PAR:",\
		TLV_ERROR_INVALID_INPUT_PARAM);\
		return GD_INCORRECT_PARAMETER

#else
#define TLV_MACRO_INVALID_INPUT_PARAM	\
		return GD_INCORRECT_PARAMETER

#endif

/**
 * \brief
 */
#if TLV_support_N5110_LCD == true
#define TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(sr_state)	\
		i_status = TLV_calc_sample_rate_parameters( sr_state );\
		if(i_status != GD_SUCCESS)\
		{\
			LCD_write_string("CALC SR PARAM x");\
			return GD_FAIL;\
		}\
		i_status = TLV_set_codec_register(TLV_REG_SMPL_RATE);\
		if(i_status != GD_SUCCESS)\
		{\
			LCD_write_string_and_uint8_dec("Set reg:", i_status);\
			return GD_FAIL;\
		}\
		t_tlv_virtual_settings.i_sampling_rate_state = (sr_state);\
		return GD_SUCCESS
#else
#define TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(sr_state)	\
		i_status = TLV_calc_sample_rate_parameters( sr_state );\
		if(i_status != GD_SUCCESS)\
		{\
			return GD_FAIL;\
		}\
		i_status = TLV_set_codec_register(TLV_REG_SMPL_RATE);\
		if(i_status != GD_SUCCESS)\
		{\
			return GD_FAIL;\
		}\
		t_tlv_virtual_settings.i_sampling_rate_state = (sr_state);\
		return GD_SUCCESS
#endif

/**
 * \name Macros, that "create" 16 bit wide value for I2C or EEPROM
 *
 * Because settings is in different format, we need some process that\n
 * "align" bits to 16 bit wide variable. First is written register address.\n
 * Then are added other bits
 * @{
 */
/// \brief Create packet for: Left input channel volume control
#define TLV_CREATE_UINT16_REG_L_IN_VOL	\
i_tmp =  TLV_REG_L_IN_VOL<<TLV_REG_SHIFT;\
i_tmp |= t_tlv_settings.i_L_in_volume<<TLV_LIV_BIT;\
i_tmp |= t_tlv_settings.i_L_in_mute  <<TLV_LIM_BIT

/// \brief Create packet for: Right input channel volume control
#define TLV_CREATE_UINT16_REG_R_IN_VOL	\
i_tmp =  TLV_REG_R_IN_VOL<<TLV_REG_SHIFT;\
i_tmp |= t_tlv_settings.i_R_in_volume<<TLV_RIV_BIT;\
i_tmp |= t_tlv_settings.i_R_in_mute  <<TLV_RIM_BIT

/// \brief Create packet for: Left channel headphone volume control
#define TLV_CREATE_UINT16_REG_L_OUT_VOL	\
i_tmp =  TLV_REG_L_OUT_VOL<<TLV_REG_SHIFT;\
i_tmp |= t_tlv_settings.i_L_out_headphone_volume    <<TLV_LHV_BIT;\
i_tmp |= t_tlv_settings.i_L_out_headphone_zero_cross<<TLV_LZC_BIT

/// \brief Create packet for: Right channel headphone volume control
#define TLV_CREATE_UINT16_REG_R_OUT_VOL	\
i_tmp =  TLV_REG_R_OUT_VOL<<TLV_REG_SHIFT;\
i_tmp |= t_tlv_settings.i_R_out_headphone_volume    <<TLV_RHV_BIT;\
i_tmp |= t_tlv_settings.i_R_out_headphone_zero_cross<<TLV_RZC_BIT

/// \brief Create packet for: Analog audio path control
#define TLV_CREATE_UINT16_REG_ANA_PATH	\
i_tmp =  TLV_REG_ANA_PATH<<TLV_REG_SHIFT;\
i_tmp |= t_tlv_settings.i_dac_select                <<TLV_DAC_BIT;\
i_tmp |= t_tlv_settings.i_bypass_in_to_out          <<TLV_BYP_BIT;\
i_tmp |= t_tlv_settings.i_mic_sidetone_to_in_and_out<<TLV_STE_STA_BIT;\
i_tmp |= t_tlv_settings.i_input_select              <<TLV_INSEL_BIT;\
i_tmp |= t_tlv_settings.i_mic_mute                  <<TLV_MICM_BIT;\
i_tmp |= t_tlv_settings.i_mic_boost_20dB            <<TLV_MICB_BIT

/// \brief Create packet for: Digital audio path control
#define TLV_CREATE_UINT16_REG_DIG_PATH	\
i_tmp =  TLV_REG_DIG_PATH<<TLV_REG_SHIFT;\
i_tmp |= t_tlv_settings.i_dac_soft_mute             <<TLV_DACM_BIT;\
i_tmp |= t_tlv_settings.i_deemp                     <<TLV_DEEMP_BIT;\
i_tmp |= t_tlv_settings.i_adc_high_pass_filter      <<TLV_ADCHP_BIT

/// \brief Create packet for: Power down control
#define TLV_CREATE_UINT16_REG_PWR_DWN	\
i_tmp =  TLV_REG_PWR_DWN<<TLV_REG_SHIFT;\
i_tmp |= t_tlv_settings.i_device_pwr_down           <<TLV_PWR_OFF_BIT;\
i_tmp |= t_tlv_settings.i_clock_pwr_down            <<TLV_PWR_CLK_BIT;\
i_tmp |= t_tlv_settings.i_oscillator_pwr_down       <<TLV_PWR_OSC_BIT;\
i_tmp |= t_tlv_settings.i_outputs_pwr_down          <<TLV_PWR_OUT_BIT;\
i_tmp |= t_tlv_settings.i_dac_pwr_down              <<TLV_PWR_DAC_BIT;\
i_tmp |= t_tlv_settings.i_adc_pwr_down              <<TLV_PWR_ADC_BIT;\
i_tmp |= t_tlv_settings.i_mic_pwr_down              <<TLV_PWR_MIC_BIT;\
i_tmp |= t_tlv_settings.i_line_pwr_down             <<TLV_PWR_LINE_BIT

/// \brief Create packet for: Audio interface format
#define TLV_CREATE_UINT16_REG_INTERFCE_FRMT	\
i_tmp =  TLV_REG_INTERFCE_FRMT<<TLV_REG_SHIFT;\
i_tmp |= t_tlv_settings.i_master_mode               <<TLV_MS_BIT;\
i_tmp |= t_tlv_settings.i_dac_left_right_swap       <<TLV_LRSWAP_BIT;\
i_tmp |= t_tlv_settings.i_dac_left_right_phase      <<TLV_LRP_BIT;\
i_tmp |= t_tlv_settings.i_input_bit_length          <<TLV_IWL_BIT;\
i_tmp |= t_tlv_settings.i_data_format               <<TLV_FORMAT_BIT

/// \brief Create packet for: Sample rate control
#define TLV_CREATE_UINT16_REG_SMPL_RATE	\
i_tmp =  TLV_REG_SMPL_RATE<<TLV_REG_SHIFT;\
i_tmp |= t_tlv_settings.i_clk_in_divider            <<TLV_CLKIN_BIT;\
i_tmp |= t_tlv_settings.i_clk_out_divider           <<TLV_CLKOUT_BIT;\
i_tmp |= t_tlv_settings.i_sampling_rate             <<TLV_SR_BIT;\
i_tmp |= t_tlv_settings.i_base_oversampling_rate    <<TLV_BOSR_BIT;\
i_tmp |= t_tlv_settings.i_clock_mode_select         <<TLV_USB_NORMAL_BIT

/// \brief Create packet for: Digital interface activation
#define TLV_CREATE_UINT16_REG_INTERFCE_ACTIV	\
i_tmp =  TLV_REG_INTERFCE_ACTIV<<TLV_REG_SHIFT;\
i_tmp |= t_tlv_settings.i_digital_interface_activation<<TLV_ACT_BIT
/// @}
/*-----------------------------------------*
 |           Function prototypes           |
 *-----------------------------------------*/

// --- High level functions ---
GD_RES_CODE TLV_init(void);

GD_RES_CODE TLV_restore_default_settings(void);

GD_RES_CODE TLV_write_actual_settings_to_codec(void);
/*---------------------------------------------------------------------------*
 |                                                                           |
 *---------------------------------------------------------------------------*/
GD_RES_CODE TLV_set_volume_in_L(uint8_t i_volume_value);
GD_RES_CODE TLV_set_volume_in_R(uint8_t i_volume_value);
GD_RES_CODE TLV_set_volume_in_LR(uint8_t i_volume_value);
GD_RES_CODE TLV_set_volume_in(uint8_t i_channel, uint8_t i_volume_value);

GD_RES_CODE TLV_set_mute_line_in_L(uint8_t i_mute_flag);
GD_RES_CODE TLV_set_mute_line_in_R(uint8_t i_mute_flag);
GD_RES_CODE TLV_set_mute_line_in_LR(uint8_t i_mute_flag);

GD_RES_CODE TLV_set_mute_line_in_enable_L(void);
GD_RES_CODE TLV_set_mute_line_in_enable_R(void);
GD_RES_CODE TLV_set_mute_line_in_enable_LR(void);
GD_RES_CODE TLV_set_mute_line_in_enable(uint8_t i_channel);

GD_RES_CODE TLV_set_mute_line_in_disable_L(void);
GD_RES_CODE TLV_set_mute_line_in_disable_R(void);
GD_RES_CODE TLV_set_mute_line_in_disable_LR(void);
GD_RES_CODE TLV_set_mute_line_in_disable(uint8_t i_channel);
/*---------------------------------------------------------------------------*
 |                                                                           |
 *---------------------------------------------------------------------------*/
GD_RES_CODE TLV_set_volume_headphones_L(uint8_t i_volume_value);
GD_RES_CODE TLV_set_volume_headphones_R(uint8_t i_volume_value);
GD_RES_CODE TLV_set_volume_headphones_LR(uint8_t i_volume_value);
GD_RES_CODE TLV_set_volume_headphones(uint8_t i_channel, uint8_t i_volume_value);

GD_RES_CODE TLV_set_mute_headphones_L(uint8_t i_mute_flag);
GD_RES_CODE TLV_set_mute_headphones_R(uint8_t i_mute_flag);
GD_RES_CODE TLV_set_mute_headphones_LR(uint8_t i_mute_flag);

GD_RES_CODE TLV_set_mute_headphones_enable_L(void);
GD_RES_CODE TLV_set_mute_headphones_enable_R(void);
GD_RES_CODE TLV_set_mute_headphones_enable_LR(void);
GD_RES_CODE TLV_set_mute_headphones_enable(uint8_t i_channel);

GD_RES_CODE TLV_set_mute_headphones_disable_L(void);
GD_RES_CODE TLV_set_mute_headphones_disable_R(void);
GD_RES_CODE TLV_set_mute_headphones_disable_LR(void);
GD_RES_CODE TLV_set_mute_headphones_disable(uint8_t i_channel);

GD_RES_CODE TLV_set_zero_cross_headphones_L(uint8_t i_zero_cross_flag);
GD_RES_CODE TLV_set_zero_cross_headphones_R(uint8_t i_zero_cross_flag);
GD_RES_CODE TLV_set_zero_cross_headphones_LR(uint8_t i_zero_cross_flag);
/*---------------------------------------------------------------------------*
 |                                                                           |
 *---------------------------------------------------------------------------*/
GD_RES_CODE TLV_set_mute_mic(uint8_t i_mute_flag);
GD_RES_CODE TLV_set_mute_mic_enable(void);
GD_RES_CODE TLV_set_mute_mic_disable(void);

GD_RES_CODE TLV_set_mic_boost_20dB(uint8_t i_boost_flag);
/*---------------------------------------------------------------------------*
 |                                                                           |
 *---------------------------------------------------------------------------*/
GD_RES_CODE TLV_set_sidetone_from_mic(uint8_t i_sidetone_virtual_value);

GD_RES_CODE TLV_set_DAC_on_off(uint8_t i_dac_flag);

GD_RES_CODE TLV_set_bypass_in_to_out(uint8_t i_bypass_flag);

GD_RES_CODE TLV_set_input_select(uint8_t i_selected_input);
/*---------------------------------------------------------------------------*
 |                                                                           |
 *---------------------------------------------------------------------------*/
GD_RES_CODE TLV_set_DAC_soft_mute(uint8_t i_soft_mute_flag);

GD_RES_CODE TLV_set_deemphasis(uint8_t i_deemp_mode);

GD_RES_CODE TLV_set_ADC_high_pass_filter(uint8_t i_hp_filter_flag);
/*---------------------------------------------------------------------------*
 |                                                                           |
 *---------------------------------------------------------------------------*/
GD_RES_CODE TLV_set_pwr_down_device(uint8_t i_pwr_dwn_flag);
GD_RES_CODE TLV_set_pwr_down_clk(uint8_t i_pwr_dwn_flag);
GD_RES_CODE TLV_set_pwr_down_oscillator(uint8_t i_pwr_dwn_flag);
GD_RES_CODE TLV_set_pwr_down_outputs(uint8_t i_pwr_dwn_flag);
GD_RES_CODE TLV_set_pwr_down_dac(uint8_t i_pwr_dwn_flag);
GD_RES_CODE TLV_set_pwr_down_adc(uint8_t i_pwr_dwn_flag);
GD_RES_CODE TLV_set_pwr_down_mic(uint8_t i_pwr_dwn_flag);
GD_RES_CODE TLV_set_pwr_down_line_in(uint8_t i_pwr_dwn_flag);
/*---------------------------------------------------------------------------*
 |                                                                           |
 *---------------------------------------------------------------------------*/
GD_RES_CODE TLV_set_master_mode(uint8_t i_master_flag);

GD_RES_CODE TLV_set_swap_left_and_right_channel(uint8_t i_swap_flag);

GD_RES_CODE TLV_set_left_right_phase(uint8_t i_phase_flag);

GD_RES_CODE TLV_set_input_bit_length(uint8_t i_bit_length_mode);

GD_RES_CODE TLV_set_data_format(uint8_t i_data_format_mode);
/*---------------------------------------------------------------------------*
 |                                                                           |
 *---------------------------------------------------------------------------*/
GD_RES_CODE TLV_set_MCLK_12MHz_ADC_96kHz_DAC_96kHz(void);
GD_RES_CODE TLV_set_MCLK_12MHz_ADC_88_2kHz_DAC_88_2kHz(void);
GD_RES_CODE TLV_set_MCLK_12MHz_ADC_48kHz_DAC_48kHz(void);
GD_RES_CODE TLV_set_MCLK_12MHz_ADC_44_1kHz_DAC_44_1kHz(void);
GD_RES_CODE TLV_set_MCLK_12MHz_ADC_32kHz_DAC_32kHz(void);
GD_RES_CODE TLV_set_MCLK_12MHz_ADC_8_021kHz_DAC_8_021kHz(void);
GD_RES_CODE TLV_set_MCLK_12MHz_ADC_8kHz_DAC_8kHz(void);
GD_RES_CODE TLV_set_MCLK_12MHz_ADC_48kHz_DAC_8kHz(void);
GD_RES_CODE TLV_set_MCLK_12MHz_ADC_44_1kHz_DAC_8_021kHz(void);
GD_RES_CODE TLV_set_MCLK_12MHz_ADC_8kHz_DAC_48kHz(void);
GD_RES_CODE TLV_set_MCLK_12MHz_ADC_8_021kHz_DAC_44_1kHz(void);

GD_RES_CODE TLV_set_MCLK_12_288MHz_ADC_96kHz_DAC_96kHz(void);
GD_RES_CODE TLV_set_MCLK_12_288MHz_ADC_48kHz_DAC_48kHz(void);
GD_RES_CODE TLV_set_MCLK_12_288MHz_ADC_32kHz_DAC_32kHz(void);
GD_RES_CODE TLV_set_MCLK_12_288MHz_ADC_8kHz_DAC_8kHz(void);
GD_RES_CODE TLV_set_MCLK_12_288MHz_ADC_48kHz_DAC_8kHz(void);
GD_RES_CODE TLV_set_MCLK_12_288MHz_ADC_8kHz_DAC_48kHz(void);

GD_RES_CODE TLV_set_MCLK_11_2896MHz_ADC_88_2kHz_DAC_88_2kHz(void);
GD_RES_CODE TLV_set_MCLK_11_2896MHz_ADC_44_1kHz_DAC_44_1kHz(void);
GD_RES_CODE TLV_set_MCLK_11_2896MHz_ADC_8_021kHz_DAC_8_021kHz(void);
GD_RES_CODE TLV_set_MCLK_11_2896MHz_ADC_44_1kHz_DAC_8_021kHz(void);
GD_RES_CODE TLV_set_MCLK_11_2896MHz_ADC_8_021kHz_DAC_44_1kHz(void);

GD_RES_CODE TLV_set_MCLK_18_432MHz_ADC_96kHz_DAC_96kHz(void);
GD_RES_CODE TLV_set_MCLK_18_432MHz_ADC_48kHz_DAC_48kHz(void);
GD_RES_CODE TLV_set_MCLK_18_432MHz_ADC_32kHz_DAC_32kHz(void);
GD_RES_CODE TLV_set_MCLK_18_432MHz_ADC_8kHz_DAC_8kHz(void);
GD_RES_CODE TLV_set_MCLK_18_432MHz_ADC_48kHz_DAC_8kHz(void);
GD_RES_CODE TLV_set_MCLK_18_432MHz_ADC_8kHz_DAC_48kHz(void);

GD_RES_CODE TLV_set_MCLK_16_9344MHz_ADC_88_2kHz_DAC_88_2kHz(void);
GD_RES_CODE TLV_set_MCLK_16_9344MHz_ADC_44_1kHz_DAC_44_1kHz(void);
GD_RES_CODE TLV_set_MCLK_16_9344MHz_ADC_8_021kHz_DAC_8_021kHz(void);
GD_RES_CODE TLV_set_MCLK_16_9344MHz_ADC_44_1kHz_DAC_8_021kHz(void);
GD_RES_CODE TLV_set_MCLK_16_9344MHz_ADC_8_021kHz_DAC_44_1kHz(void);
/*---------------------------------------------------------------------------*
 |                                                                           |
 *---------------------------------------------------------------------------*/
GD_RES_CODE TLV_set_sample_rate_state(uint8_t i_sample_rate_state);

GD_RES_CODE TLV_set_clk_in_divider(uint8_t i_divider_flag);

GD_RES_CODE TLV_set_clk_out_divider(uint8_t i_divider_flag);


GD_RES_CODE TLV_set_sampling_rate(uint8_t i_sample_rate_mode);

GD_RES_CODE TLV_set_base_oversampling_rate(uint8_t i_bosr_mode);

GD_RES_CODE TLV_set_clock_mode(uint8_t i_clock_mode);
/*---------------------------------------------------------------------------*
 |                                                                           |
 *---------------------------------------------------------------------------*/
GD_RES_CODE TLV_set_interface_activation(uint8_t i_activation_flag);
/*---------------------------------------------------------------------------*
 |                                                                           |
 *---------------------------------------------------------------------------*/
// --- Low level functions ---
GD_RES_CODE TLV_restart(void);

GD_RES_CODE TLV_default_settings_to_cfg(void);

GD_RES_CODE TLV_write_settings_to_EEPROM(void);

GD_RES_CODE TLV_read_settings_from_EEPROM(void);

GD_RES_CODE TLV_set_codec_register(uint8_t i_register_address);

GD_RES_CODE TLV_calc_sample_rate_parameters(uint8_t i_sample_rate_state);

GD_RES_CODE TLV_calc_sidetone_bits_for_TLV_register(
											uint8_t i_sidetone_virtual_value);

#ifndef true
#define true 1
#endif
#ifndef false
#define false 0
#endif

/*-----------------------------------------*
 |         Some checks of settings         |
 *-----------------------------------------*/
#if (TLV_support_N5110_LCD != true) && (TLV_support_N5110_LCD != false)
#error "Please select one of the option for support_N5110_LCD: true or false"
#endif
#if (TLV_support_generic_driver != true) &&\
	(TLV_support_generic_driver != false)
#error "Please select one of the option forTLV_support_generic_driver:\
true or false"
#endif

#if TLV_DEFAULT_VOLUME_OUT <= TLV_HEADPHONE_MUTE_VALUE
#error "Sorry. You can not set value lower than TLV_HEADPHONE_MUTE_VALUE.\
	If You want mute headphone output, please set TLV_DEFAULT_L_HEADPHONE_MUTE\
	and TLV_DEFAULT_R_HEADPHONE_MUTE to true."
#endif

#if (TLV_DEFAULT_INPUT_BIT_LENGTH != TLV_16BIT) &&\
	(TLV_DEFAULT_INPUT_BIT_LENGTH != TLV_20BIT) &&\
	(TLV_DEFAULT_INPUT_BIT_LENGTH != TLV_24BIT) &&\
	(TLV_DEFAULT_INPUT_BIT_LENGTH != TLV_32BIT)
#error "Invalid option in TLV_DEFAULT_INPUT_BIT_LENGTH! Please select valid\
	option."
#endif

#if (TLV_DEFAULT_DATA_FORMAT != TLV_FRMT_MSB_FIRST_RIGHT_ALIGNED) &&\
	(TLV_DEFAULT_DATA_FORMAT != TLV_FRMT_MSB_FIRST_LEFT_ALIGNED) &&\
	(TLV_DEFAULT_DATA_FORMAT != TLV_FRMT_I2S_MSB_FIRST) &&\
	(TLV_DEFAULT_DATA_FORMAT != TLV_FRMT_DSP)
#error "Invalid option in TLV_DEFAULT_DATA_FORMAT! Please select valid\
	option."
#endif

#if (TLV_DEFAULT_BASE_OVERSAMPLING_RATE != 0) &&\
	(TLV_DEFAULT_BASE_OVERSAMPLING_RATE != 1)
#error "TLV_DEFAULT_BASE_OVERSAMPLING_RATE must be 0 or 1!"
#endif

#if (TLV_DEFAULT_CLOCK_MODE != TLV_SR_MODE_NORMAL) &&\
	(TLV_DEFAULT_CLOCK_MODE != TLV_SR_MODE_USB)
#error "TLV_DEFAULT_CLOCK_MODE must be set to NORMAL or USB!"
#endif

#if (TLV_DEFAULT_MASTER_MODE != true) && (TLV_DEFAULT_MASTER_MODE != false)
#error "TLV_DEFAULT_MASTER_MODE must be true or false!"
#endif

/// \todo MORE checks
#endif
