/**
 * \file
 *
 * \brief Driver for audio code TLV320AIC23B
 *
 * Created  20.8.2013
 * Modified 25.2.2014
 *
 * \version 0.4.3
 * \author Martin Stejskal
 */

#include "tlv320aic23b.h"
/*-----------------------------------------*
 |             Global variables            |
 *-----------------------------------------*/
/// \brief Status "register". When not "0", then there is problem
static uint8_t i_status = 0;

/// \brief Structure for settings
static t_tlv_options t_tlv_settings;

/**
 * \brief Structure for "virtual settings" - data are not directly in TLV\n
 * registers
 */
static t_tlv_virtual_options t_tlv_virtual_settings;

/**
 * \name Backup variables
 *
 * \brief When we mute one of out channels, then we need backup original\n
 *  value (because mute means: "Set low volume"). So there they are...
 *
 * @{
 */
//! \brief Backup variable for left out channel
static uint8_t i_volume_out_backup_L;
//! \brief Backup variable for right out channel
static uint8_t i_volume_out_backup_R;
/// @}

/*-----------------------------------------*
 |         Generic driver support          |
 *-----------------------------------------*/
#if TLV_support_generic_driver == true
/**
 * \brief Configure table for device
 */
const gd_config_struct TLV320AIC23_config_table_flash[] PROGMEM =
	{
		{
				0,								// Command ID
				"Ctrl_Initialize TLV320AIC23",	// Name
				"Set hardware and load settings",	// Descriptor
				void_type,						// Input data type
				{.data_uint32 = 0},				// Minimum input value
				{.data_uint32 = 0},				// Maximum input value
				void_type,						// Output data type
				{.data_uint32 = 0},				// Minimum output value
				{.data_uint32 = 0},				// Maximum output value
				(GD_DATA_VALUE*)&gd_void_value,	// Output value
				TLV_init						/* Function, that should be
				 	 	 	 	 	 	 	 	 * called
				 	 	 	 	 	 	 	 	 */
		},
		{
				1,
				"Ctrl_Restore default settings TLV320AIC23",
				"Use settings defined on flash",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_restore_default_settings
		},
		{
				2,
				"Volume line in L",
				"",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 31},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 31},
				(GD_DATA_VALUE*)&t_tlv_settings.i_L_in_volume,
				TLV_set_volume_in_L
		},
		{
				3,
				"Volume line in R",
				"",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 31},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 31},
				(GD_DATA_VALUE*)&t_tlv_settings.i_R_in_volume,
				TLV_set_volume_in_R
		},
		{
				4,
				"Mute line in L",
				"0 - mute disabled ; 1 - mute enabled",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_settings.i_L_in_mute,
				TLV_set_mute_line_in_L
		},
		{
				5,
				"Mute line in R",
				"0 - mute disabled ; 1 - mute enabled",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_settings.i_R_in_mute,
				TLV_set_mute_line_in_R
		},
		{
				6,
				"Volume headphones L",
				"",
				uint8_type,
				{.data_uint8 = TLV_HEADPHONE_MUTE_VALUE+1},
				{.data_uint8 = 127},
				uint8_type,
				{.data_uint8 = TLV_HEADPHONE_MUTE_VALUE+1},
				{.data_uint8 = 127},
				(GD_DATA_VALUE*)&i_volume_out_backup_L,
				TLV_set_volume_headphones_L
		},
		{
				7,
				"Volume headphones R",
				"",
				uint8_type,
				{.data_uint8 = TLV_HEADPHONE_MUTE_VALUE+1},
				{.data_uint8 = 127},
				uint8_type,
				{.data_uint8 = TLV_HEADPHONE_MUTE_VALUE+1},
				{.data_uint8 = 127},
				(GD_DATA_VALUE*)&i_volume_out_backup_R,
				TLV_set_volume_headphones_R
		},
		{
				8,
				"Mute headphones L",
				"0 - mute disabled ; 1 - mute enabled",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_virtual_settings.i_L_headphone_mute_flag,
				TLV_set_mute_headphones_L
		},
		{
				9,
				"Mute headphones R",
				"0 - mute disabled ; 1 - mute enabled",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_virtual_settings.i_R_headphone_mute_flag,
				TLV_set_mute_headphones_R
		},
		{
				10,
				"Mute microphone",
				"0 - mute disabled ; 1 - mute enabled",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_settings.i_mic_mute,
				TLV_set_mute_mic
		},
		{
				11,
				"Mic boost 20dB",
				"0 - boost off ; 1 - boost on",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_settings.i_mic_boost_20dB,
				TLV_set_mic_boost_20dB
		},
		{
				12,
				"Input select",
				"0 - Line in ; 1 - Mic",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_settings.i_input_select,
				TLV_set_input_select
		},
		{
				13,
				"Data format",
				"0 - MSB first,R align ; 1 - MSB first,L align ; "
				"2 - I2S ; 3 - DSP",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 3},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 3},
				(GD_DATA_VALUE*)&t_tlv_settings.i_data_format,
				TLV_set_data_format
		},
		{
				14,
				"Bit length",
				"0 - 16b ; 1 - 20b ; 2 - 24b ; 3 - 32b",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 3},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 3},
				(GD_DATA_VALUE*)&t_tlv_settings.i_input_bit_length,
				TLV_set_input_bit_length
		},
		{
				15,
				"Master/slave mode",
				"0 - slave mode ; 1 - master mode",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_settings.i_master_mode,
				TLV_set_master_mode
		},
		{
				16,
				"CODEC_FREQ",
				"Select ADC, DAC and MCL frequencies:",
				group_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				uint8_type,
				{.data_uint16 = TLV_SAMPLE_RATE_FIRST_OPTION},
				{.data_uint16 = TLV_SAMPLE_RATE_LAST_OPTION},
				(GD_DATA_VALUE*)&t_tlv_virtual_settings.i_sampling_rate_state,
				gd_void_function
		},
		{
				/* This CMD ID must be defined at
				 * TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_96kHz_DAC_96kHz
				 * It is make program more transparent and easy to modify
				 */
				TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_96kHz_DAC_96kHz,
				"{CODEC_FREQ}FADC_96k_FDAC_96k_MCLK12MHZ",
				"Fadc=96kHz ; Fdac=96kHz @ MCLK=12MHz -> 125x",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_12MHz_ADC_96kHz_DAC_96kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_88_2kHz_DAC_88_2kHz,
				"{CODEC_FREQ}FADC_88.2k_FDAC_88.2k_MCLK12MHZ",
				"Fadc=88.2kHz ; Fdac=88.2kHz @ MCLK=12MHz -> 136x",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_12MHz_ADC_88_2kHz_DAC_88_2kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_48kHz_DAC_48kHz,
				"{CODEC_FREQ}FADC_48k_FDAC_48k_MCLK12MHZ",
				"Fadc=48kHz ; Fdac=48kHz @ MCLK=12MHz -> 250x",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_12MHz_ADC_48kHz_DAC_48kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_44_1kHz_DAC_44_1kHz,
				"{CODEC_FREQ}FADC_44.1k_FDAC_44.1k_MCLK12MHZ",
				"Fadc=44.1kHz ; Fdac=44.1kHz @ MCLK=12MHz -> 272x",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_12MHz_ADC_44_1kHz_DAC_44_1kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_32kHz_DAC_32kHz,
				"{CODEC_FREQ}FADC_32k_FDAC_32k_MCLK12MHZ",
				"Fadc=32kHz ; Fdac=32kHz @ MCLK=12MHz -> 375x",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_12MHz_ADC_32kHz_DAC_32kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8_021kHz_DAC_8_021kHz,
				"{CODEC_FREQ}FADC_8.021k_FDAC_8.021k_MCLK12MHZ",
				"Fadc=8.021kHz ; Fdac=8.021kHz @ MCLK=12MHz -> 1496x",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_12MHz_ADC_8_021kHz_DAC_8_021kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8kHz_DAC_8kHz,
				"{CODEC_FREQ}FADC_8k_FDAC_8k_MCLK12MHZ",
				"Fadc=8kHz ; Fdac=8kHz @ MCLK=12MHz -> 1500x",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_12MHz_ADC_8kHz_DAC_8kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_48kHz_DAC_8kHz,
				"{CODEC_FREQ}FADC_48k_FDAC_8k_MCLK12MHZ",
				"Fadc=48kHz (250x); Fdac=8kHz (1500x) @ MCLK=12MHz",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_12MHz_ADC_48kHz_DAC_8kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_44_1kHz_DAC_8_021kHz,
				"{CODEC_FREQ}FADC_44.1k_FDAC_8.021k_MCLK12MHZ",
				"Fadc=44.1kHz (272x); Fdac=8.021kHz (1496x) @ MCLK=12MHz",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_12MHz_ADC_44_1kHz_DAC_8_021kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8kHz_DAC_48kHz,
				"{CODEC_FREQ}FADC_8k_FDAC_48k_MCLK12MHZ",
				"Fadc=8kHz (1500x); Fdac=48kHz (250x) @ MCLK=12MHz",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_12MHz_ADC_8kHz_DAC_48kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8_021kHz_DAC_44_1kHz,
				"{CODEC_FREQ}FADC_8.021k_FDAC_44.1k_MCLK12MHZ",
				"Fadc=8.021kHz (1496x); Fdac=44.1kHz (272x) @ MCLK=12MHz",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_12MHz_ADC_8_021kHz_DAC_44_1kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_96kHz_DAC_96kHz,
				"{CODEC_FREQ}FADC_96k_FDAC_96k_MCLK12.288MHZ",
				"Fadc=96kHz ; Fdac=96kHz @ MCLK=12.288MHz -> 128x",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_12_288MHz_ADC_96kHz_DAC_96kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_48kHz_DAC_48kHz,
				"{CODEC_FREQ}FADC_48k_FDAC_48k_MCLK12.288MHZ",
				"Fadc=48kHz ; Fdac=48kHz @ MCLK=12.288MHz -> 256x",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_12_288MHz_ADC_48kHz_DAC_48kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_32kHz_DAC_32kHz,
				"{CODEC_FREQ}FADC_32k_FDAC_32k_MCLK12.288MHZ",
				"Fadc=32kHz ; Fdac=32kHz @ MCLK=12.288MHz -> 384x",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_12_288MHz_ADC_32kHz_DAC_32kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_8kHz_DAC_8kHz,
				"{CODEC_FREQ}FADC_8k_FDAC_8k_MCLK12.288MHZ",
				"Fadc=8kHz ; Fdac=8kHz @ MCLK=12.288MHz -> 1536x",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_12_288MHz_ADC_8kHz_DAC_8kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_48kHz_DAC_8kHz,
				"{CODEC_FREQ}FADC_48k_FDAC_8k_MCLK12.288MHZ",
				"Fadc=48kHz (256x); Fdac=8kHz (1536x) @ MCLK=12.288MHz",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_12_288MHz_ADC_48kHz_DAC_8kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_8kHz_DAC_48kHz,
				"{CODEC_FREQ}FADC_8k_FDAC_48k_MCLK12.288MHZ",
				"Fadc=8kHz (1536x); Fdac=48kHz (256x) @ MCLK=12.288MHz",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_12_288MHz_ADC_8kHz_DAC_48kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_88_2kHz_DAC_88_2kHz,
				"{CODEC_FREQ}FADC_88.2k_FDAC_88.2k_MCLK11.2896MHZ",
				"Fadc=88.2kHz ; Fdac=88.2kHz @ MCLK=11.2896MHz -> 128x",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_11_2896MHz_ADC_88_2kHz_DAC_88_2kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_44_1kHz_DAC_44_1kHz,
				"{CODEC_FREQ}FADC_44.1k_FDAC_44.1k_MCLK11.2896MHZ",
				"Fadc=44.1kHz ; Fdac=44.1kHz @ MCLK=11.2896MHz -> 256x",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_11_2896MHz_ADC_44_1kHz_DAC_44_1kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_8_021kHz_DAC_8_021kHz,
				"{CODEC_FREQ}FADC_8.021k_FDAC_8.021k_MCLK11.2896MHZ",
				"Fadc=8.021kHz ; Fdac=8.021kHz @ MCLK=11.2896MHz -> 1407x",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_11_2896MHz_ADC_8_021kHz_DAC_8_021kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_44_1kHz_DAC_8_021kHz,
				"{CODEC_FREQ}FADC_44.1k_FDAC_8.021k_MCLK11.2896MHZ",
				"Fadc=44.1kHz (256x); Fdac=8.021kHz (1407x) @ MCLK=11.2896MHz",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_11_2896MHz_ADC_44_1kHz_DAC_8_021kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_8_021kHz_DAC_44_1kHz,
				"{CODEC_FREQ}FADC_8.021k_FDAC_44.1k_MCLK11.2896MHZ",
				"Fadc=8.021kHz (1407x); Fdac=44.1kHz (256x) @ MCLK=11.2896MHz",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_11_2896MHz_ADC_8_021kHz_DAC_44_1kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_96kHz_DAC_96kHz,
				"{CODEC_FREQ}FADC_96k_FDAC_96k_MCLK18.432MHZ",
				"Fadc=96kHz ; Fdac=96kHz @ MCLK=18.432MHz -> 192x",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_18_432MHz_ADC_96kHz_DAC_96kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_48kHz_DAC_48kHz,
				"{CODEC_FREQ}FADC_48k_FDAC_48k_MCLK18.432MHZ",
				"Fadc=48kHz ; Fdac=48kHz @ MCLK=18.432MHz -> 384x",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_18_432MHz_ADC_48kHz_DAC_48kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_32kHz_DAC_32kHz,
				"{CODEC_FREQ}FADC_32k_FDAC_32k_MCLK18.432MHZ",
				"Fadc=32kHz ; Fdac=32kHz @ MCLK=18.432MHz -> 576x",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_18_432MHz_ADC_32kHz_DAC_32kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_8kHz_DAC_8kHz,
				"{CODEC_FREQ}FADC_8k_FDAC_8k_MCLK18.432MHZ",
				"Fadc=8kHz ; Fdac=8kHz @ MCLK=18.432MHz -> 2304x",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_18_432MHz_ADC_8kHz_DAC_8kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_48kHz_DAC_8kHz,
				"{CODEC_FREQ}FADC_48k_FDAC_8k_MCLK18.432MHZ",
				"Fadc=48kHz (384x); Fdac=8kHz (2304x) @ MCLK=18.432MHz",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_18_432MHz_ADC_48kHz_DAC_8kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_8kHz_DAC_48kHz,
				"{CODEC_FREQ}FADC_8k_FDAC_48k_MCLK18.432MHZ",
				"Fadc=8kHz (2304x); Fdac=48kHz (384x) @ MCLK=18.432MHz",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_18_432MHz_ADC_8kHz_DAC_48kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_88_2kHz_DAC_88_2kHz,
				"{CODEC_FREQ}FADC_88.2k_FDAC_88.2k_MCLK16.9344MHZ",
				"Fadc=88.2kHz ; Fdac=88.2kHz @ MCLK=16.9344MHz -> 192x",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_16_9344MHz_ADC_88_2kHz_DAC_88_2kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_44_1kHz_DAC_44_1kHz,
				"{CODEC_FREQ}FADC_44.1k_FDAC_44.1k_MCLK16.9344MHZ",
				"Fadc=44.1kHz ; Fdac=44.1kHz @ MCLK=16.9344MHz -> 384x",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_16_9344MHz_ADC_44_1kHz_DAC_44_1kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_8_021kHz_DAC_8_021kHz,
				"{CODEC_FREQ}FADC_8.021k_FDAC_8.021k_MCLK16.9344MHZ",
				"Fadc=8.021kHz ; Fdac=8.021kHz @ MCLK=16.9344MHz -> 2111x",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_16_9344MHz_ADC_8_021kHz_DAC_8_021kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_44_1kHz_DAC_8_021kHz,
				"{CODEC_FREQ}FADC_44.1k_FDAC_8.021k_MCLK16.9344MHZ",
				"Fadc=44.1kHz (384x); Fdac=8.021kHz (2111x) @ MCLK=16.9344MHz",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_16_9344MHz_ADC_44_1kHz_DAC_8_021kHz
		},
		{
				TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_8_021kHz_DAC_44_1kHz,
				"{CODEC_FREQ}FADC_8.021k_FDAC_44.1k_MCLK16.9344MHZ",
				"Fadc=8.021kHz (2111x); Fdac=44.1kHz (384x) @ MCLK=16.9344MHz",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_set_MCLK_16_9344MHz_ADC_8_021kHz_DAC_44_1kHz
		},
		{
				50,	// TLV_SAMPLE_RATE_LAST_OPTION+1
				"Device power down",
				"0 - powered ; 1 - power down",
				uint8_type,
				{.data_uint32 = 0},
				{.data_uint32 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_settings.i_device_pwr_down,
				TLV_set_pwr_down_device
		},
		{
				51,
				"Clock power down",
				"0 - powered ; 1 - power down",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_settings.i_clock_pwr_down,
				TLV_set_pwr_down_clk
		},
		{
				52,
				"Oscillator power down",
				"0 - powered ; 1 - power down",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_settings.i_oscillator_pwr_down,
				TLV_set_pwr_down_oscillator
		},
		{
				53,
				"Outputs power down",
				"0 - powered ; 1 - power down",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_settings.i_outputs_pwr_down,
				TLV_set_pwr_down_outputs
		},
		{
				54,
				"DAC power down",
				"0 - powered ; 1 - power down",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_settings.i_dac_pwr_down,
				TLV_set_pwr_down_dac
		},
		{
				55,
				"ADC power down",
				"0 - powered ; 1 - power down",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_settings.i_adc_pwr_down,
				TLV_set_pwr_down_adc
		},
		{
				56,
				"Microphone power down",
				"0 - powered ; 1 - power down",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_settings.i_mic_pwr_down,
				TLV_set_pwr_down_mic
		},
		{
				57,
				"Line in power down",
				"0 - powered ; 1 - power down",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_settings.i_line_pwr_down,
				TLV_set_pwr_down_line_in
		},
		{
				58,
				"Clock in divider",
				"0 - disabled ; 1 - enabled",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_settings.i_clk_in_divider,
				TLV_set_clk_in_divider
		},
		{
				59,
				"Clock out divider",
				"0 - disabled ; 1 - enabled",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_settings.i_clk_out_divider,
				TLV_set_clk_out_divider
		},
		{
				60,
				"Headphone zero cross L",
				"0 - disabled ; 1 - enabled",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_settings.i_L_out_headphone_zero_cross,
				TLV_set_zero_cross_headphones_L
		},
		{
				61,
				"Headphone zero cross R",
				"0 - disabled ; 1 - enabled",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_settings.i_R_out_headphone_zero_cross,
				TLV_set_zero_cross_headphones_R
		},
		{
				62,
				"Bypass line in and output",
				"0 - bypass off ; 1 - bypass on",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_settings.i_bypass_in_to_out,
				TLV_set_bypass_in_to_out
		},
		{
				63,
				"Sidetone from microphone",
				"0 - sidetone off ; 1 - sidetone on",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 5},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 5},
				(GD_DATA_VALUE*)&t_tlv_virtual_settings.i_mic_sidetone_to_in_and_out_virtual,
				TLV_set_sidetone_from_mic
		},
		{
				64,
				"DAC soft mute",
				"0 - mute disabled ; 1 - mute enabled",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_settings.i_dac_soft_mute,
				TLV_set_DAC_soft_mute
		},
		{
				65,
				"De-emphasis",
				"0 - disabled ; 1 - 32kHz ; 2 - 44.1kHz ; 3 - 48kHz",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 3},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 3},
				(GD_DATA_VALUE*)&t_tlv_settings.i_deemp,
				TLV_set_deemphasis
		},
		{
				66,
				"ADC high-pass filter",
				"0 - disabled ; 1 - enabled",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_settings.i_adc_high_pass_filter,
				TLV_set_ADC_high_pass_filter
		},
		{
				67,
				"DAC swap left and right channel",
				"0 - swap disabled ; 1 - swap enabled",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_settings.i_dac_left_right_swap,
				TLV_set_swap_left_and_right_channel
		},
		{
				68,
				"DAC phase left and right channel",
				"",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_settings.i_dac_left_right_phase,
				TLV_set_left_right_phase
		},
		{
				69,
				"DAC select",
				"0 - DAC off ; 1 - DAC selected",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_settings.i_dac_select,
				TLV_set_DAC_on_off
		},
		{
				70,
				"Interface activation",
				"0 - interface off ; 1 - interface active",
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				uint8_type,
				{.data_uint8 = 0},
				{.data_uint8 = 1},
				(GD_DATA_VALUE*)&t_tlv_settings.i_digital_interface_activation,
				TLV_set_interface_activation
		},
		{
				71,
				"Ctrl_Save actual settings of TLV320AIC23",
				"Write actual settings to EEPROM",
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				void_type,
				{.data_uint32 = 0},
				{.data_uint32 = 0},
				(GD_DATA_VALUE*)&gd_void_value,
				TLV_write_settings_to_EEPROM
		},
	};
// \brief Maximum command ID (is defined by last command)
#define TLV_MAX_CMD_ID		71

const gd_metadata TLV320AIC23_metadata =
{
	// Max ID
	TLV_MAX_CMD_ID,
	// Description
	"Audio Codec TLV320AIC23B v0.4.3",
	// Pointer to configuration table on flash
	(gd_config_struct*)&TLV320AIC23_config_table_flash[0],
	// Serial number
	0x02
};



#endif


/**
 * \brief Initialize TLV codec and load settings from EEPROM if valid
 *
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_init(void)
{
	// Try restart TLV codec
	if( TLV_restart() != GD_SUCCESS)
	{
#if TLV_support_N5110_LCD == true
		LCD_write_string("TLV not found :[\n Trying again");
#endif
		// OK, let�s give TLV one more chance
		// Small time delay
		TLV_PACKET_DELAY;

		if( TLV_restart() != GD_SUCCESS)
		{
#if TLV_support_N5110_LCD == true
			LCD_write_string("TLV320AIC23B\nNot found!");
#endif
			return GD_FAIL;
		}
	}

	/* Read settings from EEPROM (if not valid, load default). Return value
	 * not important
	 */
	TLV_read_settings_from_EEPROM();

	// Write configuration to codec and return error code
	return TLV_write_actual_settings_to_codec();
}


/**
 * \brief Load default settings
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_restore_default_settings(void)
{
	// Load default settings to variables and check if all OK
	if(TLV_default_settings_to_cfg() != GD_SUCCESS)
	{
		return GD_FAIL;
	}

	// Write settings to codec and return error code
	return TLV_write_actual_settings_to_codec();
}


/**
 * \brief Write complete settings to codec
 *
 * Collect all configurations and set all registers in TLV codec
 *
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_write_actual_settings_to_codec(void)
{
	// Small time delay
	TLV_PACKET_DELAY;

	// Register: Left line input channel volume control
	i_status = TLV_set_codec_register(TLV_REG_L_IN_VOL);
	// Test if there was any problem (macro)
	TLV_CHECK_I2C_STATUS;

	// Register: Right line input channel volume control
	i_status = TLV_set_codec_register(TLV_REG_R_IN_VOL);
	TLV_CHECK_I2C_STATUS;

	// Register: Left channel headphone volume control
	i_status = TLV_set_codec_register(TLV_REG_L_OUT_VOL);
	TLV_CHECK_I2C_STATUS;

	// Register: Right channel headphone volume control
	i_status = TLV_set_codec_register(TLV_REG_R_OUT_VOL);
	TLV_CHECK_I2C_STATUS;

	// Register: Analog audio path control
	i_status = TLV_set_codec_register(TLV_REG_ANA_PATH);
	TLV_CHECK_I2C_STATUS;

	// Register: Digital audio path control
	i_status = TLV_set_codec_register(TLV_REG_DIG_PATH);
	TLV_CHECK_I2C_STATUS;

	// Register: Power down control
	i_status = TLV_set_codec_register(TLV_REG_PWR_DWN);
	TLV_CHECK_I2C_STATUS;

	// Register: Digital audio interface format
	i_status = TLV_set_codec_register(TLV_REG_INTERFCE_FRMT);
	TLV_CHECK_I2C_STATUS;

	// Register: Sample rate control
	i_status = TLV_set_codec_register(TLV_REG_SMPL_RATE);
	TLV_CHECK_I2C_STATUS;

	// Register: Digital interface activation
	i_status = TLV_set_codec_register(TLV_REG_INTERFCE_ACTIV);
	TLV_CHECK_I2C_STATUS;

	// All seems to be OK
	return GD_SUCCESS;
}
/*---------------------------------------------------------------------------*
 |                                                                           |
 |                                                                           |
 |                                                                           |
 *---------------------------------------------------------------------------*/
/**
 * \brief Allow set volume for line in right channel
 * @param i_volume_value 5 bit value.
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_volume_in_L(uint8_t i_volume_value)
{
	// Test input parameters if not higher than maximum (5 bits)
	if(i_volume_value > TLV_LIV_MASK )
	{
		TLV_MACRO_INVALID_INPUT_PARAM;
	}

	// Backup actual volume
	uint8_t i_backup_tmp = t_tlv_settings.i_L_in_volume;

	// Change value
	t_tlv_settings.i_L_in_volume = i_volume_value;

	// And try to set volume
	i_status = TLV_set_codec_register(TLV_REG_L_IN_VOL);

	// Check status - if fail restore settings and return error code
	if(i_status != GD_SUCCESS)
	{
		t_tlv_settings.i_L_in_volume = i_backup_tmp;
		return GD_FAIL;
	}

	return GD_SUCCESS;
}

/**
 * \brief Allow set volume for line in left channel
 * @param i_volume_value 5 bit value.
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_volume_in_R(uint8_t i_volume_value)
{
	// Test input parameters if not higher than maximum (5 bits)
	if(i_volume_value > TLV_LIV_MASK )
	{
		TLV_MACRO_INVALID_INPUT_PARAM;
	}

	uint8_t i_backup_tmp = t_tlv_settings.i_R_in_volume;
	t_tlv_settings.i_R_in_volume = i_volume_value;
	i_status = TLV_set_codec_register(TLV_REG_R_IN_VOL);
	if(i_status != GD_SUCCESS)
	{
		t_tlv_settings.i_R_in_volume = i_backup_tmp;
		return GD_FAIL;
	}
	return GD_SUCCESS;
}

/**
 * \brief Allow set volume for line in left and right channel
 * @param i_volume_value 5 bit value.
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_volume_in_LR(uint8_t i_volume_value)
{
	// Set L channel
	i_status = TLV_set_volume_in_L(i_volume_value);
	if(i_status != GD_SUCCESS)
	{
		return GD_FAIL;
	}
	// Set R channel (if error, add it)
	i_status = TLV_set_volume_in_R(i_volume_value);
	if(i_status != GD_SUCCESS)
	{
		return GD_FAIL;
	}
	return GD_SUCCESS;
}


/**
 * \brief Allow set volume for line in
 * @param i_channel Options: TLV_LEFT, TLV_RIGHT and TLV_BOTH
 * @param i_volume_value 5 bit value.
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_volume_in(	uint8_t i_channel,
								uint8_t i_volume_value)
{
	switch(i_channel)
	{
	// Try set volume on left channel
	case TLV_LEFT:
		i_status = TLV_set_volume_in_L(i_volume_value);
		if(i_status != GD_SUCCESS)
		{
			return GD_FAIL;
		}
		break;

	// Try set volume on right channel
	case TLV_RIGHT:
		i_status = TLV_set_volume_in_R(i_volume_value);
		if(i_status != GD_SUCCESS)
		{
			return GD_FAIL;
		}
		break;

	// Try to set volume on both channels
	case TLV_BOTH:
		// Set L channel
		i_status = TLV_set_volume_in_L(i_volume_value);
		if(i_status != GD_SUCCESS)
		{
			return GD_FAIL;
		}
		// Set R channel (if error, add it)
		i_status = TLV_set_volume_in_R(i_volume_value);
		if(i_status != GD_SUCCESS)
		{
			return GD_FAIL;
		}
		break;

	// Else there is invalid option -> just show error
	default:
		TLV_MACRO_INVALID_INPUT_PARAM;
	}

	return GD_SUCCESS;
}



/**
 * \brief According to mute_flag enable, or disable mute on left input
 * @param i_mute_flag 1 - enable mute ; 0 - disable mute
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mute_line_in_L(uint8_t i_mute_flag)
{
	// Test for input value
	if(i_mute_flag > 1)
	{
		TLV_MACRO_INVALID_INPUT_PARAM;
	}

	if(i_mute_flag == 0)
	{
		return TLV_set_mute_line_in_disable_L();
	}
	else
	{
		return TLV_set_mute_line_in_enable_L();
	}
}

/**
 * \brief According to mute_flag enable, or disable mute on right input
 * @param i_mute_flag 1 - enable mute ; 0 - disable mute
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mute_line_in_R(uint8_t i_mute_flag)
{
	// Test for input value
	if(i_mute_flag > 1)
	{
		TLV_MACRO_INVALID_INPUT_PARAM;
	}

	if(i_mute_flag == 0)
	{
		return TLV_set_mute_line_in_disable_R();
	}
	else
	{
		return TLV_set_mute_line_in_enable_R();
	}
}

/**
 * \brief According to mute_flag enable, or disable mute on left and right input
 * @param i_mute_flag 1 - enable mute ; 0 - disable mute
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mute_line_in_LR(uint8_t i_mute_flag)
{
	// Test for input value
	if(i_mute_flag > 1)
	{
		TLV_MACRO_INVALID_INPUT_PARAM;
	}

	if(i_mute_flag == 0)
	{
		i_status = TLV_set_mute_line_in_disable_L();
		if(i_status != GD_SUCCESS)
		{
			return GD_FAIL;
		}
		i_status = TLV_set_mute_line_in_disable_R();
		if(i_status != GD_SUCCESS)
		{
			return GD_FAIL;
		}
		return GD_SUCCESS;
	}
	else
	{
		i_status = TLV_set_mute_line_in_enable_L();
		if(i_status != GD_SUCCESS)
		{
			return GD_FAIL;
		}
		i_status = TLV_set_mute_line_in_enable_R();
		if(i_status != GD_SUCCESS)
		{
			return GD_FAIL;
		}
		return GD_SUCCESS;
	}
}

/**
 * \brief Mute left input channel
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mute_line_in_enable_L(void)
{
	uint8_t i_backup_tmp = t_tlv_settings.i_L_in_mute;
	t_tlv_settings.i_L_in_mute = true;
	i_status = TLV_set_codec_register(TLV_REG_L_IN_VOL);
	if(i_status != GD_SUCCESS)
	{
		t_tlv_settings.i_L_in_mute = i_backup_tmp;
		return GD_FAIL;
	}
	return GD_SUCCESS;
}


/**
 * \brief Mute right input channel
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mute_line_in_enable_R(void)
{
	uint8_t i_backup_tmp = t_tlv_settings.i_R_in_mute;
	t_tlv_settings.i_R_in_mute = true;
	i_status = TLV_set_codec_register(TLV_REG_R_IN_VOL);
	if(i_status != GD_SUCCESS)
	{
		t_tlv_settings.i_R_in_mute = i_backup_tmp;
		return GD_FAIL;
	}
	return GD_SUCCESS;
}
/**
 * \brief Mute left and right input channel
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mute_line_in_enable_LR(void)
{
	i_status = TLV_set_mute_line_in_enable_L();
	if(i_status != GD_SUCCESS)
	{
		return GD_FAIL;
	}
	i_status = TLV_set_mute_line_in_enable_R();
	if(i_status != GD_SUCCESS)
	{
		return GD_FAIL;
	}
	return GD_SUCCESS;
}

/**
 * \brief Mute selected input channel
 * @param i_channel Options: TLV_LEFT, TLV_RIGHT and TLV_BOTH
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mute_line_in_enable(uint8_t i_channel)
{
	switch(i_channel)
	{
	case TLV_LEFT:
		i_status = TLV_set_mute_line_in_enable_L();
		if(i_status != GD_SUCCESS)
		{
			return GD_FAIL;
		}
		break;
	case TLV_RIGHT:
		i_status = TLV_set_mute_line_in_enable_R();
		if(i_status != GD_SUCCESS)
		{
			return GD_FAIL;
		}
		break;
	case TLV_BOTH:
		i_status = TLV_set_mute_line_in_enable_L();
		if(i_status != GD_SUCCESS)
		{
			return GD_FAIL;
		}
		i_status = TLV_set_mute_line_in_enable_R();
		if(i_status != GD_SUCCESS)
		{
			return GD_FAIL;
		}
		break;
	default:
		TLV_MACRO_INVALID_INPUT_PARAM;
	}

	return GD_SUCCESS;
}

/**
 * \brief Disable mute on left input channel
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mute_line_in_disable_L(void)
{
	uint8_t i_backup_tmp = t_tlv_settings.i_L_in_mute;
	t_tlv_settings.i_L_in_mute = false;
	i_status = TLV_set_codec_register(TLV_REG_L_IN_VOL);
	if(i_status != GD_SUCCESS)
	{
		t_tlv_settings.i_L_in_mute = i_backup_tmp;
		return GD_FAIL;
	}
	return GD_SUCCESS;
}

/**
 * \brief Disable mute on right input channel
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mute_line_in_disable_R(void)
{
	uint8_t i_backup_tmp = t_tlv_settings.i_R_in_mute;
	t_tlv_settings.i_R_in_mute = false;
	i_status = TLV_set_codec_register(TLV_REG_R_IN_VOL);
	if(i_status != GD_SUCCESS)
	{
		t_tlv_settings.i_R_in_mute = i_backup_tmp;
		return GD_FAIL;
	}
	return GD_SUCCESS;
}

/**
 * \brief Disable mute on left and right input channel
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mute_line_in_disable_LR(void)
{
	i_status = TLV_set_mute_line_in_disable_L();
	if(i_status != GD_SUCCESS)
	{
		return GD_FAIL;
	}
	i_status = TLV_set_mute_line_in_disable_R();
	if(i_status != GD_SUCCESS)
	{
		return GD_FAIL;
	}
	return GD_SUCCESS;
}

/**
 * \brief Disable mute on selected input channel
 * @param i_channel Options: TLV_LEFT, TLV_RIGHT and TLV_BOTH
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mute_line_in_disable(uint8_t i_channel)
{
	// Just clean mute bit
	switch(i_channel)
	{
	case TLV_LEFT:
		i_status = TLV_set_mute_line_in_disable_L();
		if(i_status != GD_SUCCESS)
		{
			return GD_FAIL;
		}
		break;
	case TLV_RIGHT:
		i_status = TLV_set_mute_line_in_disable_R();
		if(i_status != GD_SUCCESS)
		{
			return GD_FAIL;
		}
		break;
	case TLV_BOTH:
		i_status = TLV_set_mute_line_in_disable_L();
		if(i_status != GD_SUCCESS)
		{
			return GD_FAIL;
		}
		i_status = TLV_set_mute_line_in_disable_R();
		if(i_status != GD_SUCCESS)
		{
			return GD_FAIL;
		}
		break;
	default:
		TLV_MACRO_INVALID_INPUT_PARAM;
	}

	return GD_SUCCESS;
}
/*---------------------------------------------------------------------------*
 |                                                                           |
 |                                                                           |
 |                                                                           |
 *---------------------------------------------------------------------------*/
/**
 * \brief Allow set volume for headphones left channel
 * @param i_volume_value 7 bit value
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_volume_headphones_L(uint8_t i_volume_value)
{
	/* Because for output volume there is not mute flag, mute is done by
	 * setting volume to 0b00110000. So, previous volume setting should be
	 * saved. That is the reason of using i i_volume_out_backup_L and
	 * i_volume_out_backup_R as global variables (must be shared with
	 * TLV_mute_headphones_disable)
	 */
	// Test input parameters if not higher than maximum and lower than mute value
	if( (i_volume_value > TLV_LHV_MASK) ||
		(i_volume_value <= TLV_HEADPHONE_MUTE_VALUE))
	{
		return GD_INCORRECT_PARAMETER;
	}
	/* At volume settings for headphones it is a little bit complicated. If
	 * mute flag is set, do not change volume on TLV. Just change variables.
	 * Else normally write volume to TLV immediately.
	 */
	if(t_tlv_virtual_settings.i_L_headphone_mute_flag != 0)
	{
		// If mute flag is set - just update volume value here
		i_volume_out_backup_L = i_volume_value;
		return GD_SUCCESS;
	}

	// If mute flag is not set
	t_tlv_settings.i_L_out_headphone_volume = i_volume_value;
	i_status = TLV_set_codec_register(TLV_REG_L_OUT_VOL);
	TLV_CHECK_I2C_STATUS;

	// If OK, then load actual value to i_volume_out_backup_L
	i_volume_out_backup_L = i_volume_value;

	return GD_SUCCESS;
}

/**
 * \brief Allow set volume for headphones right channel
 * @param i_volume_value 7 bit value
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_volume_headphones_R(uint8_t i_volume_value)
{
	/* Because for output volume there is not mute flag, mute is done by
	 * setting volume to 0b00110000. So, previous volume setting should be
	 * saved. That is the reason of using i i_volume_out_backup_L and
	 * i_volume_out_backup_R as global variables (must be shared with
	 * TLV_mute_headphones_disable)
	 */
	// Test input parameters if not higher than maximum and lower than mute value
	if( (i_volume_value > TLV_LHV_MASK) ||
		(i_volume_value <= TLV_HEADPHONE_MUTE_VALUE))
	{
		return GD_INCORRECT_PARAMETER;
	}
	/* At volume settings for headphones it is a little bit complicated. If
	 * mute flag is set, do not change volume on TLV. Just change variables.
	 * Else normally write volume to TLV immediately.
	 */
	if(t_tlv_virtual_settings.i_R_headphone_mute_flag != 0)
	{
		// If mute flag is set - just update volume value here
		i_volume_out_backup_R = i_volume_value;
		return GD_SUCCESS;
	}

	// If mute flag is not set
	t_tlv_settings.i_R_out_headphone_volume = i_volume_value;
	i_status = TLV_set_codec_register(TLV_REG_R_OUT_VOL);
	TLV_CHECK_I2C_STATUS;

	// If OK, then load actual value to i_volume_out_backup_L
	i_volume_out_backup_R = i_volume_value;

	return GD_SUCCESS;
}

/**
 * \brief Allow set volume for headphones left and right channel
 * @param i_volume_value 7 bit value
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_volume_headphones_LR(uint8_t i_volume_value)
{
	i_status = TLV_set_volume_headphones_L(i_volume_value);
	if(i_status != GD_SUCCESS)
	{
		return GD_FAIL;
	}
	i_status = TLV_set_volume_headphones_R(i_volume_value);
	if(i_status != GD_SUCCESS)
	{
		return GD_FAIL;
	}
	return GD_SUCCESS;
}

/**
 * \brief Allow set volume for headphones
 * @param i_channel Options: TLV_LEFT, TLV_RIGHT and TLV_BOTH
 * @param i_volume_value 7 bit value. Used mask to get only 7 bits!
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_volume_headphones(uint8_t i_channel, uint8_t i_volume_value)
{
	switch(i_channel)
	{
	// Try set volume on left channel
	case TLV_LEFT:
		i_status = TLV_set_volume_headphones_L(i_volume_value);
		break;
	// Try to set volume on right channel
	case TLV_RIGHT:
		i_status = TLV_set_volume_headphones_R(i_volume_value);
		break;
	// Try to set volume on both channels
	case TLV_BOTH:
		i_status = TLV_set_volume_headphones_LR(i_volume_value);
		break;
	// Else there is invalid option -> just show error
	default:
		TLV_MACRO_INVALID_INPUT_PARAM;
	}

	return i_status;
}


/**
 * \brief According to mute_flag enable, or disable mute on left headphone
 * @param i_mute_flag 1 - enable mute ; 0 - disable mute
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mute_headphones_L(uint8_t i_mute_flag)
{
	// Test for input value
	if(i_mute_flag > 1)
	{
		TLV_MACRO_INVALID_INPUT_PARAM;
	}

	if(i_mute_flag == 0)
	{
		return TLV_set_mute_headphones_disable_L();
	}
	else
	{
		return TLV_set_mute_headphones_enable_L();
	}
}

/**
 * \brief According to mute_flag enable, or disable mute on right headphone
 * @param i_mute_flag 1 - enable mute ; 0 - disable mute
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mute_headphones_R(uint8_t i_mute_flag)
{
	// Test for input value
	if(i_mute_flag > 1)
	{
		TLV_MACRO_INVALID_INPUT_PARAM;
	}

	if(i_mute_flag == 0)
	{
		return TLV_set_mute_headphones_disable_R();
	}
	else
	{
		return TLV_set_mute_headphones_enable_R();
	}
}

/**
 * \brief According to mute_flag enable, or disable mute on both headphones
 * @param i_mute_flag 1 - enable mute ; 0 - disable mute
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mute_headphones_LR(uint8_t i_mute_flag)
{
	// Test for input value
	if(i_mute_flag > 1)
	{
		TLV_MACRO_INVALID_INPUT_PARAM;
	}

	if(i_mute_flag == 0)
	{
		return TLV_set_mute_headphones_disable_LR();
	}
	else
	{
		return TLV_set_mute_headphones_enable_LR();
	}
}

/**
 * \brief Mute left headphone
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mute_headphones_enable_L(void)
{
	/* Because for output volume there is not mute flag, mute is done by
	 * setting volume to 0b00110000. So, previous volume setting should be
	 * saved. That is the reason of using i i_volume_out_backup_L and
	 * i_volume_out_backup_R as global variables (must be shared with
	 * TLV_mute_headphones_disable)
	 */
	// Check for mute flag
	if(t_tlv_virtual_settings.i_L_headphone_mute_flag != 0)
	{
		// If mute flag already set -> just re-send data to TLV
		t_tlv_settings.i_L_out_headphone_volume = TLV_HEADPHONE_MUTE_VALUE;
		i_status = TLV_set_codec_register(TLV_REG_L_OUT_VOL);
		TLV_CHECK_I2C_STATUS;
	}
	else
	{
		// Mute flag not set yet. So, first try to send data to TLV
		t_tlv_settings.i_L_out_headphone_volume = TLV_HEADPHONE_MUTE_VALUE;
		i_status = TLV_set_codec_register(TLV_REG_L_OUT_VOL);
		TLV_CHECK_I2C_STATUS;

		// If not failed, then set mute flag to t_tlv_settings
		t_tlv_virtual_settings.i_L_headphone_mute_flag = true;
	}

	return GD_SUCCESS;
}

/**
 * \brief Mute right headphone
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mute_headphones_enable_R(void)
{
	/* Because for output volume there is not mute flag, mute is done by
	 * setting volume to 0b00110000. So, previous volume setting should be
	 * saved. That is the reason of using i i_volume_out_backup_L and
	 * i_volume_out_backup_R as global variables (must be shared with
	 * TLV_mute_headphones_disable)
	 */
	// Check for mute flag
	if(t_tlv_virtual_settings.i_R_headphone_mute_flag != 0)
	{
		// If mute flag already set -> just re-send data to TLV
		t_tlv_settings.i_R_out_headphone_volume = TLV_HEADPHONE_MUTE_VALUE;
		i_status = TLV_set_codec_register(TLV_REG_R_OUT_VOL);
		TLV_CHECK_I2C_STATUS;
	}
	else
	{
		// Mute flag not set yet. So, first try to send data to TLV
		t_tlv_settings.i_R_out_headphone_volume = TLV_HEADPHONE_MUTE_VALUE;
		i_status = TLV_set_codec_register(TLV_REG_R_OUT_VOL);
		TLV_CHECK_I2C_STATUS;

		// If not failed, then set mute flag to t_tlv_settings
		t_tlv_virtual_settings.i_R_headphone_mute_flag = true;
	}

	return GD_SUCCESS;
}

/**
 * \brief Mute left and right headphone
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mute_headphones_enable_LR(void)
{
	i_status = TLV_set_mute_headphones_enable_L();
	if(i_status != GD_SUCCESS)
	{
		return GD_FAIL;
	}
	i_status = TLV_set_mute_headphones_enable_R();
	if(i_status != GD_SUCCESS)
	{
		return GD_FAIL;
	}
	return i_status;
}


/**
 * \brief Enable mute on selected output channel
 * @param i_channel Options: TLV_LEFT, TLV_RIGHT and TLV_BOTH
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mute_headphones_enable(uint8_t i_channel)
{
	switch(i_channel)
	{
	case TLV_LEFT:
		TLV_set_mute_headphones_enable_L();
		break;
	case TLV_RIGHT:
		TLV_set_mute_headphones_enable_R();
		break;
	case TLV_BOTH:
		TLV_set_mute_headphones_enable_LR();
		break;
	default:
		TLV_MACRO_INVALID_INPUT_PARAM;
	}

	return GD_SUCCESS;
}

/**
 * \brief Disable mute on left headphone
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mute_headphones_disable_L(void)
{
	// Just load volume value to codec. If success, set mute flag too
	t_tlv_settings.i_L_out_headphone_volume = i_volume_out_backup_L;
	i_status = TLV_set_codec_register(TLV_REG_L_OUT_VOL);
	TLV_CHECK_I2C_STATUS;

	// If OK, set mute flag
	t_tlv_virtual_settings.i_L_headphone_mute_flag = false;

	return GD_SUCCESS;
}

/**
 * \brief Disable mute on right headphone
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mute_headphones_disable_R(void)
{
	// Just load volume value to codec. If success, set mute flag too
	t_tlv_settings.i_R_out_headphone_volume = i_volume_out_backup_R;
	i_status = TLV_set_codec_register(TLV_REG_R_OUT_VOL);
	TLV_CHECK_I2C_STATUS;

	// If OK, set mute flag
	t_tlv_virtual_settings.i_R_headphone_mute_flag = false;

	return GD_SUCCESS;
}

/**
 * \brief Disable mute on left and right headphone
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mute_headphones_disable_LR(void)
{
	i_status = TLV_set_mute_headphones_disable_L();
	if(i_status != GD_SUCCESS)
	{
		return GD_FAIL;
	}
	i_status = TLV_set_mute_headphones_disable_R();
	if(i_status != GD_SUCCESS)
	{
		return GD_FAIL;
	}
	return i_status;
}

/**
 * \brief Disable mute on selected output channel
 * @param i_channel Options: TLV_LEFT, TLV_RIGHT and TLV_BOTH
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mute_headphones_disable(uint8_t i_channel)
{
	/* Because for output volume there is not mute flag, mute is done by
	 * setting volume to 0b00110000. So, previous volume setting should be
	 * saved. That is the reason of using i i_volume_out_backup_L and
	 * i_volume_out_backup_R as global variables (must be shared with
	 * TLV_mute_headphones_enable)
	 */
	switch(i_channel)
	{
	case TLV_LEFT:
		i_status = TLV_set_mute_headphones_disable_L();
		break;
	case TLV_RIGHT:
		i_status = TLV_set_mute_headphones_disable_R();
		break;
	case TLV_BOTH:
		i_status = TLV_set_mute_headphones_disable_LR();
		break;
	default:
		TLV_MACRO_INVALID_INPUT_PARAM;
	}

	return i_status;
}

/**
 * \brief Allow set zero cross bit for left channel
 * @param i_zero_cross_flag 1 - enable ; 0 - disable
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_zero_cross_headphones_L(uint8_t i_zero_cross_flag)
{
	// Test input value
	if(i_zero_cross_flag >1)
	{
		TLV_MACRO_INVALID_INPUT_PARAM;
	}

	// If OK, then backup actual value and try to set actual
	uint8_t i_backup_tmp = t_tlv_settings.i_L_out_headphone_zero_cross;
	// Set new value and send data
	t_tlv_settings.i_L_out_headphone_zero_cross= i_zero_cross_flag;
	i_status = TLV_set_codec_register(TLV_REG_L_OUT_VOL);
	// Test status
	if(i_status != GD_SUCCESS)
	{
		// Restore value
		t_tlv_settings.i_L_out_headphone_zero_cross = i_backup_tmp;
		return GD_FAIL;
	}

	return GD_SUCCESS;
}

/**
 * \brief Allow set zero cross bit for right channel
 * @param i_zero_cross_flag 1 - enable ; 0 - disable
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_zero_cross_headphones_R(uint8_t i_zero_cross_flag)
{
	// Test input value
	if(i_zero_cross_flag >1)
	{
		TLV_MACRO_INVALID_INPUT_PARAM;
	}

	// If OK, then backup actual value and try to set actual
	uint8_t i_backup_tmp = t_tlv_settings.i_R_out_headphone_zero_cross;
	// Set new value and send data
	t_tlv_settings.i_R_out_headphone_zero_cross= i_zero_cross_flag;
	i_status = TLV_set_codec_register(TLV_REG_R_OUT_VOL);
	// Test status
	if(i_status != GD_SUCCESS)
	{
		// Restore value
		t_tlv_settings.i_R_out_headphone_zero_cross = i_backup_tmp;
		return GD_FAIL;
	}

	return GD_SUCCESS;
}

/**
 * \brief Allow set zero cross bit for left and right channel
 * @param i_zero_cross_flag 1 - enable ; 0 - disable
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_zero_cross_headphones_LR(uint8_t i_zero_cross_flag)
{
	i_status = TLV_set_zero_cross_headphones_L(i_zero_cross_flag);
	// Test
	if(i_status != GD_SUCCESS)
	{
		return GD_FAIL;
	}
	i_status = TLV_set_zero_cross_headphones_R(i_zero_cross_flag);
	if(i_status != GD_SUCCESS)
	{
		return GD_FAIL;
	}
	return GD_SUCCESS;
}
/*---------------------------------------------------------------------------*
 |                                                                           |
 |                                                                           |
 |                                                                           |
 *---------------------------------------------------------------------------*/
/**
 * \brief According to mute_flag enable, or disable mute on microphone
 * @param i_mute_flag 1 - enable mute ; 0 - disable mute
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mute_mic(uint8_t i_mute_flag)
{
	// Test for input value
	if(i_mute_flag > 1)
	{
		TLV_MACRO_INVALID_INPUT_PARAM;
	}

	if(i_mute_flag == 0)
	{
		return TLV_set_mute_mic_disable();
	}
	else
	{
		return TLV_set_mute_mic_enable();
	}
}

/**
 * \brief Enable mute microphone
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mute_mic_enable(void)
{
	// Backup actual value
	uint8_t i_backup_tmp = t_tlv_settings.i_mic_mute;
	t_tlv_settings.i_mic_mute = true;
	// Send data thru I2C - register errors
	i_status = TLV_set_codec_register(TLV_REG_ANA_PATH);
	// If not success -> FAIL
	if(i_status != GD_SUCCESS)
	{
		// Restore origin value
		t_tlv_settings.i_mic_mute = i_backup_tmp;
		return GD_FAIL;
	}
	return GD_SUCCESS;
}

/**
 * \brief Disable mute microphone
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mute_mic_disable(void)
{
	// Backup actual value
	uint8_t i_backup_tmp = t_tlv_settings.i_mic_mute;
	t_tlv_settings.i_mic_mute = false;
	// Send data thru I2C - register errors
	i_status = TLV_set_codec_register(TLV_REG_ANA_PATH);
	// If not success -> FAIL
	if(i_status != GD_SUCCESS)
	{
		// Restore origin value
		t_tlv_settings.i_mic_mute = i_backup_tmp;
		return GD_FAIL;
	}
	return GD_SUCCESS;
}

/**
 * \brief Allow to enable and disable +20dB boost for microphone
 * @param i_boost_flag 1 - enable boost ; 0 - disable boost
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_mic_boost_20dB(uint8_t i_boost_flag)
{
	// Test input parameters
	if(i_boost_flag > 1)
	{
		TLV_MACRO_INVALID_INPUT_PARAM;
	}

	// Backup actual settings
	uint8_t i_backup_tmp = t_tlv_settings.i_mic_boost_20dB;

	// Load value to t_tlv_settings
	t_tlv_settings.i_mic_boost_20dB = i_boost_flag;
	// Send data over I2C
	i_status = TLV_set_codec_register(TLV_REG_ANA_PATH);
	// Check result
	if(i_status != GD_SUCCESS)
	{
		// If data sending failed
		// Restore origin settings
		t_tlv_settings.i_mic_boost_20dB = i_backup_tmp;
		// And return FAIL
		return GD_FAIL;
	}
	// If not failed, return SUCCESS
	return GD_SUCCESS;
}

/*---------------------------------------------------------------------------*
 |                                                                           |
 |                                                                           |
 |                                                                           |
 *---------------------------------------------------------------------------*/
/**
 * \brief Set sidetone from microphone
 *
 * Input format for variable is not same as format in TLV register! Please\n
 * take a look to parameter values. This is simplified version of setting\n
 * sidetone.
 *
 * @param i_sidetone_virtual_value 0: disabled; 1: -18dB ; 2: -12dB ; 3 -9dB;\n
 * 4 -6dB ; 5 0dB
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_sidetone_from_mic(uint8_t i_sidetone_virtual_value)
{
	// Recalculate value for TLV register
	i_status = TLV_calc_sidetone_bits_for_TLV_register(
													i_sidetone_virtual_value);
	// Test return value
	if(i_status != GD_SUCCESS)
	{
		return i_status;
	}
	// If OK, send over I2C
	i_status = TLV_set_codec_register(TLV_REG_ANA_PATH);
	if(i_status != GD_SUCCESS)
	{
		return i_status;
	}
	// OK, if data send, update virtual_value
	t_tlv_virtual_settings.i_mic_sidetone_to_in_and_out_virtual =
													i_sidetone_virtual_value;

	return GD_SUCCESS;
}

/**
 * \brief Enable or disable DAC in analog path
 * @param i_dac_flag 1: DAC selected ; 0: DAC off
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_DAC_on_off(uint8_t i_dac_flag)
{
	// Test input value
	if(i_dac_flag > 1)
	{
		return GD_INCORRECT_PARAMETER;
	}

	// If OK, then backup actual value and try to set actual
	uint8_t i_backup_tmp = t_tlv_settings.i_dac_select;
	// Set new value and send data
	t_tlv_settings.i_dac_select = i_dac_flag;
	i_status = TLV_set_codec_register(TLV_REG_ANA_PATH);
	// Test status
	if(i_status != GD_SUCCESS)
	{
		// Restore value
		t_tlv_settings.i_dac_select = i_backup_tmp;

		return GD_FAIL;
	}

	// Else all is OK
	return GD_SUCCESS;
}


/**
 * \brief Bypass line in to output
 * @param i_bypass_flag 1: bypass enabled ; 0: bypass disabled
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_bypass_in_to_out(uint8_t i_bypass_flag)
{
	// Test input variable
	if(i_bypass_flag > 1)
	{
		return GD_INCORRECT_PARAMETER;
	}

	// Backup value and try to set actual value
	uint8_t i_backup_tmp = t_tlv_settings.i_bypass_in_to_out;
	t_tlv_settings.i_bypass_in_to_out = i_bypass_flag;
	i_status = TLV_set_codec_register(TLV_REG_ANA_PATH);
	// Test status
	if(i_status != GD_SUCCESS)
	{
		// Restore value
		t_tlv_settings.i_bypass_in_to_out = i_backup_tmp;
		return GD_FAIL;
	}

	// Else all OK
	return GD_SUCCESS;
}

/**
 * \brief Select input channel (Line in or microphone)
 * @param i_selected_input Options: TLV_LINE_IN_CH (0), TLV_MIC_CH (1)
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_input_select(uint8_t i_selected_input)
{
	// Test input parameters
	if(i_selected_input > 1)
	{
		TLV_MACRO_INVALID_INPUT_PARAM;
	}

	// Backup actual value
	uint8_t i_backup_tmp = t_tlv_settings.i_input_select;
	// Set new  value
	t_tlv_settings.i_input_select = i_selected_input;
	// Set register and check status
	i_status = TLV_set_codec_register(TLV_REG_ANA_PATH);
	if(i_status != GD_SUCCESS)
	{
		// Restore data and return FAIL
		t_tlv_settings.i_input_select = i_backup_tmp;
		return GD_FAIL;
	}
	// Else all OK
	return GD_SUCCESS;
}
/*---------------------------------------------------------------------------*
 |                                                                           |
 |                                                                           |
 |                                                                           |
 *---------------------------------------------------------------------------*/
/**
 * \brief Enable or disable DAC soft mute
 * @param i_soft_mute_flag 0: soft mute disabled ; 1: soft mute enabled
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_DAC_soft_mute(uint8_t i_soft_mute_flag)
{
	// Check input data
	if(i_soft_mute_flag > 1)
	{
		return GD_INCORRECT_PARAMETER;
	}

	// Backup actual value
	uint8_t i_backup_tmp = t_tlv_settings.i_dac_soft_mute;
	// Set new and send
	t_tlv_settings.i_dac_soft_mute = i_soft_mute_flag;
	i_status = TLV_set_codec_register(TLV_REG_DIG_PATH);
	if(i_status != GD_SUCCESS)
	{
		// If fail -> restore origin value
		t_tlv_settings.i_dac_soft_mute = i_backup_tmp;
		return GD_FAIL;
	}
	// Else all OK
	return GD_SUCCESS;
}

/**
 * \brief Set de-emphasis
 * @param i_deemp_mode 0: TLV_DEEMP_DISABLED ; 1: TLV_DEEMP_32kHz ;\n
 * 2: TLV_DEEMP_44_1kHz ; 3: TLV_DEEMP_48kHz
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_set_deemphasis(uint8_t i_deemp_mode)
{
	// Check input data
	if(i_deemp_mode > 3)
	{
		return GD_INCORRECT_PARAMETER;
	}

	// Backup actual value
	uint8_t i_backup_tmp = t_tlv_settings.i_deemp;
	// Set new and send
	t_tlv_settings.i_deemp = i_deemp_mode;
	i_status = TLV_set_codec_register(TLV_REG_DIG_PATH);
	if(i_status != GD_SUCCESS)
	{
		// Restore origin value
		t_tlv_settings.i_deemp = i_backup_tmp;
		return GD_FAIL;
	}
	// Else all OK
	return GD_SUCCESS;
}

/**
 * \brief Allow enable/disable ADC high pass filter
 * @param i_hp_filter_flag 1: filter enabled ; 0: filter disabled
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_ADC_high_pass_filter(uint8_t i_hp_filter_flag)
{
	// Test input value
	if(i_hp_filter_flag > 1)
	{
		return GD_INCORRECT_PARAMETER;
	}

	// Backup actual value
	uint8_t i_backup_tmp = t_tlv_settings.i_adc_high_pass_filter;
	// Set new and send
	t_tlv_settings.i_adc_high_pass_filter = i_hp_filter_flag;
	i_status = TLV_set_codec_register(TLV_REG_DIG_PATH);
	if(i_status != GD_SUCCESS)
	{
		// Restore value
		t_tlv_settings.i_adc_high_pass_filter = i_backup_tmp;
		return GD_FAIL;
	}
	// Else all OK
	return GD_SUCCESS;
}
/*---------------------------------------------------------------------------*
 |                                                                           |
 |                                                                           |
 |                                                                           |
 *---------------------------------------------------------------------------*/
/**
 * \brief Allow power down TLV device
 * @param i_pwr_dwn_flag 1: power down ; 0: power on
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_pwr_down_device(uint8_t i_pwr_dwn_flag)
{
	// Test input value
	if(i_pwr_dwn_flag > 1)
	{
		return GD_INCORRECT_PARAMETER;
	}

	// Backup actual value, set new, try send
	uint8_t i_backup_tmp = t_tlv_settings.i_device_pwr_down;
	t_tlv_settings.i_device_pwr_down = i_pwr_dwn_flag;
	i_status = TLV_set_codec_register(TLV_REG_PWR_DWN);
	if(i_status != GD_SUCCESS)
	{
		// Restore settings
		t_tlv_settings.i_device_pwr_down = i_backup_tmp;
		return GD_FAIL;
	}
	// Else all OK
	return GD_SUCCESS;
}

/**
 * \brief Allow power down TLV clock
 * @param i_pwr_dwn_flag 1: power down ; 0: power on
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_pwr_down_clk(uint8_t i_pwr_dwn_flag)
{
	// Test input value
	if(i_pwr_dwn_flag > 1)
	{
		return GD_INCORRECT_PARAMETER;
	}

	// Backup value, set new, try send
	uint8_t i_backup_tmp = t_tlv_settings.i_clock_pwr_down;
	t_tlv_settings.i_clock_pwr_down = i_pwr_dwn_flag;
	i_status = TLV_set_codec_register(TLV_REG_PWR_DWN);
	if(i_status != GD_SUCCESS)
	{
		// Restore settings
		t_tlv_settings.i_clock_pwr_down = i_backup_tmp;
		return GD_FAIL;
	}

	return GD_SUCCESS;
}

/**
 * \brief Allow power down TLV oscillator
 * @param i_pwr_dwn_flag 1: power down ; 0: power on
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_pwr_down_oscillator(uint8_t i_pwr_dwn_flag)
{
	// Test input value
	if(i_pwr_dwn_flag > 1)
	{
		return GD_INCORRECT_PARAMETER;
	}

	// Backup value, set new, try send
	uint8_t i_backup_tmp = t_tlv_settings.i_oscillator_pwr_down;
	t_tlv_settings.i_oscillator_pwr_down = i_pwr_dwn_flag;
	i_status = TLV_set_codec_register(TLV_REG_PWR_DWN);
	if(i_status != GD_SUCCESS)
	{
		// Restore settings
		t_tlv_settings.i_oscillator_pwr_down = i_backup_tmp;
		return GD_FAIL;
	}

	return GD_SUCCESS;
}

/**
 * \brief Allow power down TLV outputs
 * @param i_pwr_dwn_flag 1: power down ; 0: power on
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_pwr_down_outputs(uint8_t i_pwr_dwn_flag)
{
	// Test input value
	if(i_pwr_dwn_flag > 1)
	{
		return GD_INCORRECT_PARAMETER;
	}

	// Backup value, set new, try send
	uint8_t i_backup_tmp = t_tlv_settings.i_outputs_pwr_down;
	t_tlv_settings.i_outputs_pwr_down = i_pwr_dwn_flag;
	i_status = TLV_set_codec_register(TLV_REG_PWR_DWN);
	if(i_status != GD_SUCCESS)
	{
		// Restore settings
		t_tlv_settings.i_outputs_pwr_down = i_backup_tmp;
		return GD_FAIL;
	}

	return GD_SUCCESS;
}

/**
 * \brief Allow power down TLV DAC
 * @param i_pwr_dwn_flag 1: power down ; 0: power on
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_pwr_down_dac(uint8_t i_pwr_dwn_flag)
{
	// Test input value
	if(i_pwr_dwn_flag > 1)
	{
		return GD_INCORRECT_PARAMETER;
	}

	// Backup value, set new, try send
	uint8_t i_backup_tmp = t_tlv_settings.i_dac_pwr_down;
	t_tlv_settings.i_dac_pwr_down = i_pwr_dwn_flag;
	i_status = TLV_set_codec_register(TLV_REG_PWR_DWN);
	if(i_status != GD_SUCCESS)
	{
		// Restore settings
		t_tlv_settings.i_dac_pwr_down = i_backup_tmp;
		return GD_FAIL;
	}

	return GD_SUCCESS;
}

/**
 * \brief Allow power down TLV ADC
 * @param i_pwr_dwn_flag 1: power down ; 0: power on
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_pwr_down_adc(uint8_t i_pwr_dwn_flag)
{
	// Test input value
	if(i_pwr_dwn_flag > 1)
	{
		return GD_INCORRECT_PARAMETER;
	}

	// Backup value, set new, try send
	uint8_t i_backup_tmp = t_tlv_settings.i_adc_pwr_down;
	t_tlv_settings.i_adc_pwr_down = i_pwr_dwn_flag;
	i_status = TLV_set_codec_register(TLV_REG_PWR_DWN);
	if(i_status != GD_SUCCESS)
	{
		// Restore settings
		t_tlv_settings.i_adc_pwr_down = i_backup_tmp;
		return GD_FAIL;
	}

	return GD_SUCCESS;
}

/**
 * \brief Allow power down TLV microphone input
 * @param i_pwr_dwn_flag 1: power down ; 0: power on
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_pwr_down_mic(uint8_t i_pwr_dwn_flag)
{
	// Test input value
	if(i_pwr_dwn_flag > 1)
	{
		return GD_INCORRECT_PARAMETER;
	}

	// Backup value, set new, try send
	uint8_t i_backup_tmp = t_tlv_settings.i_mic_pwr_down;
	t_tlv_settings.i_mic_pwr_down = i_pwr_dwn_flag;
	i_status = TLV_set_codec_register(TLV_REG_PWR_DWN);
	if(i_status != GD_SUCCESS)
	{
		// Restore settings
		t_tlv_settings.i_mic_pwr_down = i_backup_tmp;
		return GD_FAIL;
	}

	return GD_SUCCESS;
}

/**
 * \brief Allow power down TLV line in
 * @param i_pwr_dwn_flag 1: power down ; 0: power on
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_pwr_down_line_in(uint8_t i_pwr_dwn_flag)
{
	// Test input value
	if(i_pwr_dwn_flag > 1)
	{
		return GD_INCORRECT_PARAMETER;
	}

	// Backup value, set new, try send
	uint8_t i_backup_tmp = t_tlv_settings.i_line_pwr_down;
	t_tlv_settings.i_line_pwr_down = i_pwr_dwn_flag;
	i_status = TLV_set_codec_register(TLV_REG_PWR_DWN);
	if(i_status != GD_SUCCESS)
	{
		// Restore settings
		t_tlv_settings.i_line_pwr_down = i_backup_tmp;
		return GD_FAIL;
	}

	return GD_SUCCESS;
}
/*---------------------------------------------------------------------------*
 |                                                                           |
 |                                                                           |
 |                                                                           |
 *---------------------------------------------------------------------------*/
/**
 * \brief Enable or disable master mode on TLV
 * @param i_master_flag 1: enable master mode ; 0: disable master mode (slave)
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_master_mode(uint8_t i_master_flag)
{
	// Test input value
	if(i_master_flag > 1)
	{
		return GD_INCORRECT_PARAMETER;
	}

	// Backup value, set new, try send
	uint8_t i_backup_tmp = t_tlv_settings.i_master_mode;
	t_tlv_settings.i_master_mode = i_master_flag;
	i_status = TLV_set_codec_register(TLV_REG_INTERFCE_FRMT);
	if(i_status != GD_SUCCESS)
	{
		// Restore settings
		t_tlv_settings.i_master_mode = i_backup_tmp;
		return GD_FAIL;
	}

	return GD_SUCCESS;
}

/**
 * \brief Allow swap left and right channel
 * @param i_swap_flag 1: swap channels ; 0: swap channels disabled
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_swap_left_and_right_channel(uint8_t i_swap_flag)
{
	// Test input value
	if(i_swap_flag > 1)
	{
		return GD_INCORRECT_PARAMETER;
	}

	// Backup value, set new, try send
	uint8_t i_backup_tmp = t_tlv_settings.i_dac_left_right_swap;
	t_tlv_settings.i_dac_left_right_swap = i_swap_flag;
	i_status = TLV_set_codec_register(TLV_REG_INTERFCE_FRMT);
	if(i_status != GD_SUCCESS)
	{
		// Restore
		t_tlv_settings.i_dac_left_right_swap = i_backup_tmp;
		return GD_FAIL;
	}

	return GD_SUCCESS;
}

/**
 * \brief Allow set phase for DAC
 * @param i_phase_flag 0: Right channel on, LRCIN high\n
 * 1: Right channel on, LRCIN low\n
 * DSP mode:\n
 * 1: MSB is available on 2nd BCLK rising edge after LRCIN rising edge\n
 * 0: MSB is available on 1st BCLK rising edge after LRCIN rising edge
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_left_right_phase(uint8_t i_phase_flag)
{
	// Test input parameter
	if(i_phase_flag > 1)
	{
		return GD_INCORRECT_PARAMETER;
	}

	// Backup value, set new, try send
	uint8_t i_backup_tmp = t_tlv_settings.i_dac_left_right_phase;
	t_tlv_settings.i_dac_left_right_phase = i_phase_flag;
	i_status = TLV_set_codec_register(TLV_REG_INTERFCE_FRMT);
	if(i_status != GD_SUCCESS)
	{
		// Restore
		t_tlv_settings.i_dac_left_right_phase = i_backup_tmp;
		return GD_FAIL;
	}

	return GD_SUCCESS;
}


/**
 * \brief Set bit length of word
 * @param i_bit_length_mode 0: 16bit ; 1: 20bit ; 2: 24bit ; 3: 32bit
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_input_bit_length(uint8_t i_bit_length_mode)
{
	// Test input parameter
	if(i_bit_length_mode > 3)
	{
		return GD_INCORRECT_PARAMETER;
	}

	// Backup value, set new, try send
	uint8_t i_backup_tmp = t_tlv_settings.i_input_bit_length;
	t_tlv_settings.i_input_bit_length = i_bit_length_mode;
	i_status = TLV_set_codec_register(TLV_REG_INTERFCE_FRMT);
	if(i_status != GD_SUCCESS)
	{
		t_tlv_settings.i_input_bit_length = i_backup_tmp;
		return GD_FAIL;

	}

	return GD_SUCCESS;
}

/**
 * \brief Set digital interface format for data
 * @param i_data_format_mode 0: MSB first, right aligned\n
 * 1: MSB first, left aligned\n
 * 2: I2S format, MSB first, left - 1 aligned\n
 * 3: DSP format, frame sync followed by two data words
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_data_format(uint8_t i_data_format_mode)
{
	// Test input parameter
	if(i_data_format_mode > 3)
	{
		return GD_INCORRECT_PARAMETER;
	}

	// Backup value, set new, try send
	uint8_t i_backup_tmp = t_tlv_settings.i_data_format;
	t_tlv_settings.i_data_format = i_data_format_mode;
	i_status = TLV_set_codec_register(TLV_REG_INTERFCE_FRMT);
	if(i_status != GD_SUCCESS)
	{
		// Restore
		t_tlv_settings.i_data_format = i_backup_tmp;
		return GD_FAIL;
	}

	return GD_SUCCESS;
}
/*---------------------------------------------------------------------------*
 |                                                                           |
 |                                                                           |
 |                                                                           |
 *---------------------------------------------------------------------------*/
/**
 * \brief Set codec to Fadc = 96kHz ; Fdac = 96kHz @ MCLK 12MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_12MHz_ADC_96kHz_DAC_96kHz(void)
{
	/* Macro, that calculate witch values should be in TLV register
	 * TLV_REG_SMPL_RATE. Then check status calc function. If OK, then try
	 * send data to TLV. If OK, set variable
	 * t_tlv_virtual_settings.i_sampling_rate_state according to actual state.
	 */
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_96kHz_DAC_96kHz);

}

/**
 * \brief Set codec to Fadc = 88.2kHz ; Fdac = 88.2kHz @ MCLK 12MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_12MHz_ADC_88_2kHz_DAC_88_2kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_88_2kHz_DAC_88_2kHz);
}

/**
 * \brief Set codec to Fadc = 48kHz ; Fdac = 48kHz @ MCLK 12MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_12MHz_ADC_48kHz_DAC_48kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_48kHz_DAC_48kHz);
}

/**
 * \brief Set codec to Fadc = 44.1kHz ; Fdac = 44.1kHz @ MCLK 12MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_12MHz_ADC_44_1kHz_DAC_44_1kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_44_1kHz_DAC_44_1kHz);
}

/**
 * \brief Set codec to Fadc = 32kHz ; Fdac = 32kHz @ MCLK 12MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_12MHz_ADC_32kHz_DAC_32kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_32kHz_DAC_32kHz);
}

/**
 * \brief Set codec to Fadc = 8.021kHz ; Fdac = 8.021kHz @ MCLK 12MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_12MHz_ADC_8_021kHz_DAC_8_021kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8_021kHz_DAC_8_021kHz);
}

/**
 * \brief Set codec to Fadc = 8kHz ; Fdac = 8kHz @ MCLK 12MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_12MHz_ADC_8kHz_DAC_8kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8kHz_DAC_8kHz);
}

/**
 * \brief Set codec to Fadc = 48kHz ; Fdac = 8kHz @ MCLK 12MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_12MHz_ADC_48kHz_DAC_8kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_48kHz_DAC_8kHz);
}

/**
 * \brief Set codec to Fadc = 44.1kHz ; Fdac = 8.021kHz @ MCLK 12MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_12MHz_ADC_44_1kHz_DAC_8_021kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_44_1kHz_DAC_8_021kHz);
}

/**
 * \brief Set codec to Fadc = 8kHz ; Fdac = 48kHz @ MCLK 12MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_12MHz_ADC_8kHz_DAC_48kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8kHz_DAC_48kHz);
}

/**
 * \brief Set codec to Fadc = 8.021kHz ; Fdac = 44.1kHz @ MCLK 12MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_12MHz_ADC_8_021kHz_DAC_44_1kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8_021kHz_DAC_44_1kHz);
}

/**
 * \brief Set codec to Fadc = 96kHz ; Fdac = 96kHz @ MCLK 12.288MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_12_288MHz_ADC_96kHz_DAC_96kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_96kHz_DAC_96kHz);
}

/**
 * \brief Set codec to Fadc = 48kHz ; Fdac = 48kHz @ MCLK 12.288MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_12_288MHz_ADC_48kHz_DAC_48kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_48kHz_DAC_48kHz);
}

/**
 * \brief Set codec to Fadc = 32kHz ; Fdac = 32kHz @ MCLK 12.288MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_12_288MHz_ADC_32kHz_DAC_32kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_32kHz_DAC_32kHz);
}

/**
 * \brief Set codec to Fadc = 8kHz ; Fdac = 8kHz @ MCLK 12.288MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_12_288MHz_ADC_8kHz_DAC_8kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_8kHz_DAC_8kHz);
}

/**
 * \brief Set codec to Fadc = 48kHz ; Fdac = 8kHz @ MCLK 12.288MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_12_288MHz_ADC_48kHz_DAC_8kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_48kHz_DAC_8kHz);
}

/**
 * \brief Set codec to Fadc = 8kHz ; Fdac = 48kHz @ MCLK 12.288MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_12_288MHz_ADC_8kHz_DAC_48kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_8kHz_DAC_48kHz);
}

/**
 * \brief Set codec to Fadc = 88.2kHz ; Fdac = 88.2kHz @ MCLK 11.2896MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_11_2896MHz_ADC_88_2kHz_DAC_88_2kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_88_2kHz_DAC_88_2kHz);
}

/**
 * \brief Set codec to Fadc = 44.1kHz ; Fdac = 44.1kHz @ MCLK 11.2896MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_11_2896MHz_ADC_44_1kHz_DAC_44_1kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_44_1kHz_DAC_44_1kHz);
}

/**
 * \brief Set codec to Fadc = 8.021kHz ; Fdac = 8.021kHz @ MCLK 11.2896MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_11_2896MHz_ADC_8_021kHz_DAC_8_021kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_8_021kHz_DAC_8_021kHz);
}

/**
 * \brief Set codec to Fadc = 44.1kHz ; Fdac = 8.021kHz @ MCLK 11.2896MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_11_2896MHz_ADC_44_1kHz_DAC_8_021kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_44_1kHz_DAC_8_021kHz);
}

/**
 * \brief Set codec to Fadc = 8.021kHz ; Fdac = 44.1kHz @ MCLK 11.2896MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_11_2896MHz_ADC_8_021kHz_DAC_44_1kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_8_021kHz_DAC_44_1kHz);
}

/**
 * \brief Set codec to Fadc = 96kHz ; Fdac = 96kHz @ MCLK 18.432MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_18_432MHz_ADC_96kHz_DAC_96kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_96kHz_DAC_96kHz);
}

/**
 * \brief Set codec to Fadc = 48kHz ; Fdac = 48kHz @ MCLK 18.432MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_18_432MHz_ADC_48kHz_DAC_48kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_48kHz_DAC_48kHz);
}

/**
 * \brief Set codec to Fadc = 32kHz ; Fdac = 32kHz @ MCLK 18.432MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_18_432MHz_ADC_32kHz_DAC_32kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_32kHz_DAC_32kHz);
}

/**
 * \brief Set codec to Fadc = 8kHz ; Fdac = 8kHz @ MCLK 18.432MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_18_432MHz_ADC_8kHz_DAC_8kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_8kHz_DAC_8kHz);
}

/**
 * \brief Set codec to Fadc = 48kHz ; Fdac = 8kHz @ MCLK 18.432MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_18_432MHz_ADC_48kHz_DAC_8kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_48kHz_DAC_8kHz);
}

/**
 * \brief Set codec to Fadc = 8kHz ; Fdac = 48kHz @ MCLK 18.432MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_18_432MHz_ADC_8kHz_DAC_48kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_8kHz_DAC_48kHz);
}

/**
 * \brief Set codec to Fadc = 88.2kHz ; Fdac = 88.2kHz @ MCLK 16.9344MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_16_9344MHz_ADC_88_2kHz_DAC_88_2kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_88_2kHz_DAC_88_2kHz);
}

/**
 * \brief Set codec to Fadc = 44.1kHz ; Fdac = 44.1kHz @ MCLK 16.9344MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_16_9344MHz_ADC_44_1kHz_DAC_44_1kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_44_1kHz_DAC_44_1kHz);
}

/**
 * \brief Set codec to Fadc = 8.021kHz ; Fdac = 8.021kHz @ MCLK 16.9344MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_16_9344MHz_ADC_8_021kHz_DAC_8_021kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_8_021kHz_DAC_8_021kHz);
}

/**
 * \brief Set codec to Fadc = 44.1kHz ; Fdac = 8.021kHz @ MCLK 16.9344MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_16_9344MHz_ADC_44_1kHz_DAC_8_021kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_44_1kHz_DAC_8_021kHz);
}

/**
 * \brief Set codec to Fadc = 8.021kHz ; Fdac = 44.1kHz @ MCLK 16.9344MHz
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_MCLK_16_9344MHz_ADC_8_021kHz_DAC_44_1kHz(void)
{
	TLV_MACRO_CALC_TEST_AND_SEND_SAMPLE_RATE_STATE(
			TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_8_021kHz_DAC_44_1kHz);
}
/*---------------------------------------------------------------------------*
 |                                                                           |
 |                                                                           |
 |                                                                           |
 *---------------------------------------------------------------------------*/
/**
 * \brief Set one of predefined sample rate
 * For complete sample rate options, please take a look to\n
 * "Sample rate states" in .h file.
 * @param i_sample_rate_state One of sample rate states. Please search for\n
 * "Sample rate states" in .h file
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_sample_rate_state(uint8_t i_sample_rate_state)
{
	// Try set new configuration
	i_status = TLV_calc_sample_rate_parameters(i_sample_rate_state);
	if(i_status != GD_SUCCESS)
	{
		return i_status;
	}

	// Try load configuration to codec
	i_status = TLV_set_codec_register(TLV_REG_SMPL_RATE);
	if(i_status != GD_SUCCESS)
	{
		return GD_FAIL;
	}

	// If OK, load input parameter also to global variable
	t_tlv_virtual_settings.i_sampling_rate_state = i_sample_rate_state;

	return GD_SUCCESS;
}

/**
 * \brief Set divider for MCLK coming to pin XTI/MCLK
 * @param i_divider_flag 1: divider by 2 ; 0: no division
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_clk_in_divider(uint8_t i_divider_flag)
{
	// Test input parameter
	if(i_divider_flag > 1)
	{
		return GD_INCORRECT_PARAMETER;
	}

	// Backup value, set new, try send
	uint8_t i_backup_tmp = t_tlv_settings.i_clk_in_divider;
	t_tlv_settings.i_clk_in_divider = i_divider_flag;
	i_status = TLV_set_codec_register(TLV_REG_SMPL_RATE);
	if(i_status != GD_SUCCESS)
	{
		// Restore
		t_tlv_settings.i_clk_in_divider = i_backup_tmp;
		return GD_FAIL;
	}

	return GD_SUCCESS;
}

/**
 * \brief Set divider for CLKOUT pin
 * @param i_divider_flag 1: divider by 2 ; 0: no division
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_clk_out_divider(uint8_t i_divider_flag)
{
	// Test input parameter
	if(i_divider_flag > 1)
	{
		return GD_INCORRECT_PARAMETER;
	}

	// Backup value, set new, try send
	uint8_t i_backup_tmp = t_tlv_settings.i_clk_out_divider;
	t_tlv_settings.i_clk_out_divider = i_divider_flag;
	i_status = TLV_set_codec_register(TLV_REG_SMPL_RATE);
	if(i_status != GD_SUCCESS)
	{
		// Restore
		t_tlv_settings.i_clk_out_divider = i_backup_tmp;
		return GD_FAIL;
	}

	return GD_SUCCESS;
}


/**
 * \brief Set sample rate mode. Please read datasheet first!
 * @param i_sample_rate_mode Sample rate bits (4)
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_sampling_rate(uint8_t i_sample_rate_mode)
{
	// Test input parameter
	if(i_sample_rate_mode > 0b1111)
	{
		return GD_INCORRECT_PARAMETER;
	}

	// Backup value, set new, try send
	uint8_t i_backup_tmp = t_tlv_settings.i_sampling_rate;
	t_tlv_settings.i_sampling_rate = i_sample_rate_mode;
	i_status = TLV_set_codec_register(TLV_REG_SMPL_RATE);
	if(i_status != GD_SUCCESS)
	{
		// Restore
		t_tlv_settings.i_sampling_rate = i_backup_tmp;
		return GD_FAIL;
	}

	return GD_SUCCESS;
}

/**
 * \brief Set base oversampling rate bit. Please read datasheet first!
 * @param i_bosr_mode USB mode: 0: 250 fs ; 1: 272 fs\n
 * Normal mode: 0: 256 fs ; 1: 384 fs
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_base_oversampling_rate(uint8_t i_bosr_mode)
{
	// Test input parameter
	if(i_bosr_mode > 1)
	{
		return GD_INCORRECT_PARAMETER;
	}

	// Backup value, set new, try send
	uint8_t i_backup_tmp = t_tlv_settings.i_base_oversampling_rate;
	t_tlv_settings.i_base_oversampling_rate = i_bosr_mode;
	i_status = TLV_set_codec_register(TLV_REG_SMPL_RATE);
	if(i_status != GD_SUCCESS)
	{
		// Restore
		t_tlv_settings.i_base_oversampling_rate = i_backup_tmp;
		return GD_FAIL;
	}

	return GD_SUCCESS;
}

/**
 * \brief Select one of clock mode. Please read datasheet first!
 * @param i_clock_mode 0: Normal ; 1: USB
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_clock_mode(uint8_t i_clock_mode)
{
	// Test input parameter
	if(i_clock_mode > 1)
	{
		return GD_INCORRECT_PARAMETER;
	}

	// Backup value, set new, try send
	uint8_t i_backup_tmp = t_tlv_settings.i_clock_mode_select;
	t_tlv_settings.i_clock_mode_select = i_clock_mode;
	i_status = TLV_set_codec_register(TLV_REG_SMPL_RATE);
	if(i_status != GD_SUCCESS)
	{
		// Restore
		t_tlv_settings.i_clock_mode_select = i_backup_tmp;
		return GD_FAIL;
	}

	return GD_SUCCESS;
}
/*---------------------------------------------------------------------------*
 |                                                                           |
 |                                                                           |
 |                                                                           |
 *---------------------------------------------------------------------------*/
/**
 * \brief Allow enable or disable digital interface
 * @param i_activation_flag 1: enable ; 0: disable
 * @return GD_SUCCESS if all OK
 */
GD_RES_CODE TLV_set_interface_activation(uint8_t i_activation_flag)
{
	// Test input parameter
	if(i_activation_flag > 1)
	{
		return GD_INCORRECT_PARAMETER;
	}

	// Backup value, set new, try send
	uint8_t i_backup_tmp = t_tlv_settings.i_digital_interface_activation;
	t_tlv_settings.i_digital_interface_activation = i_activation_flag;
	i_status = TLV_set_codec_register(TLV_REG_INTERFCE_ACTIV);
	if(i_status != GD_SUCCESS)
	{
		// Restore
		t_tlv_settings.i_digital_interface_activation = i_backup_tmp;
		return GD_FAIL;
	}

	return GD_SUCCESS;
}
/*---------------------------------------------------------------------------*
 |                                                                           |
 |                                                                           |
 |                                                                           |
 *---------------------------------------------------------------------------*/

/**
 * \brief Restart codec. All registers will be set to CODEC DEFAULT!
 * @return Error codes from I2C. If 0, then all is OK
 */
GD_RES_CODE TLV_restart(void)
{
	return I2C_master_send_2x8bits(TLV_I2C_ADDRESS, TLV_REG_RESET<<1, 0);
}

/**
 * \brief Load application default settings to configure variables
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_default_settings_to_cfg(void)
{
	// Load default settings

	// Left input
	t_tlv_settings.i_L_in_volume				= TLV_DEFAULT_VOLUME_IN;
	t_tlv_settings.i_L_in_mute					= TLV_DEFAULT_L_IN_MUTE;

	// Right input
	t_tlv_settings.i_R_in_volume				= TLV_DEFAULT_VOLUME_IN;
	t_tlv_settings.i_R_in_mute					= TLV_DEFAULT_R_IN_MUTE;

	// Left output
	t_tlv_settings.i_L_out_headphone_volume			= TLV_DEFAULT_VOLUME_OUT;
	i_volume_out_backup_L 							= TLV_DEFAULT_VOLUME_OUT;
	t_tlv_virtual_settings.i_L_headphone_mute_flag= TLV_DEFAULT_L_HEADPHONE_MUTE;
	t_tlv_settings.i_L_out_headphone_zero_cross
									= TLV_DEFAULT_L_OUT_HEADPHONE_ZERO_CROSS;

	// Right output
	t_tlv_settings.i_R_out_headphone_volume			= TLV_DEFAULT_VOLUME_OUT;
	i_volume_out_backup_R 							= TLV_DEFAULT_VOLUME_OUT;
	t_tlv_virtual_settings.i_R_headphone_mute_flag= TLV_DEFAULT_R_HEADPHONE_MUTE;
	t_tlv_settings.i_R_out_headphone_zero_cross
									= TLV_DEFAULT_R_OUT_HEADPHONE_ZERO_CROSS;


	// --- Analog audio path ---
	t_tlv_settings.i_dac_select					= TLV_DEFAULT_DAC_SELECT;
	t_tlv_settings.i_bypass_in_to_out			= TLV_DEFAULT_BYPASS_IN_OUT;
	t_tlv_settings.i_mic_sidetone_to_in_and_out	= TLV_DEFAULT_SIDETONE;
	// Virtual sidetone - make it in preprocessor
#if TLV_DEFAULT_SIDETONE == TLV_SIDETONE_DISABLED
	t_tlv_virtual_settings.i_mic_sidetone_to_in_and_out_virtual = 0;
#endif
#if TLV_DEFAULT_SIDETONE == TLV_SIDETONE_MINUS_18dB
	t_tlv_virtual_settings.i_mic_sidetone_to_in_and_out_virtual = 1;
#endif
#if TLV_DEFAULT_SIDETONE == TLV_SIDETONE_MINUS_12dB
	t_tlv_virtual_settings.i_mic_sidetone_to_in_and_out_virtual = 2;
#endif
#if TLV_DEFAULT_SIDETONE == TLV_SIDETONE_MINUS_9dB
	t_tlv_virtual_settings.i_mic_sidetone_to_in_and_out_virtual = 3;
#endif
#if TLV_DEFAULT_SIDETONE == TLV_SIDETONE_MINUS_6dB
	t_tlv_virtual_settings.i_mic_sidetone_to_in_and_out_virtual = 4;
#endif
#if TLV_DEFAULT_SIDETONE == TLV_SIDETONE_0dB
	t_tlv_virtual_settings.i_mic_sidetone_to_in_and_out_virtual = 5;
#endif
	t_tlv_settings.i_input_select				= TLV_DEFAULT_INPUT_SELECT;
	t_tlv_settings.i_mic_mute					= TLV_DEFAULT_MIC_MUTE;
	t_tlv_settings.i_mic_boost_20dB				= TLV_DEFAULT_MIC_BOOST;


	// --- Digital audio path ---
	t_tlv_settings.i_dac_soft_mute				= TLV_DEFAULT_DAC_SOFT_MUTE;
	t_tlv_settings.i_deemp						= TLV_DEFAULT_DEEMP;
	t_tlv_settings.i_adc_high_pass_filter		= TLV_DEFAULT_ADC_HP_FILTER;


	// --- Power down control ---
	t_tlv_settings.i_device_pwr_down			= TLV_DEFAULT_DEVICE_PWR;
	t_tlv_settings.i_clock_pwr_down				= TLV_DEFAULT_CLOCK_PWR;
	t_tlv_settings.i_oscillator_pwr_down		= TLV_DEFAULT_OSCILLATOR_PWR;
	t_tlv_settings.i_outputs_pwr_down			= TLV_DEFAULT_OUTPUTS_PWR;
	t_tlv_settings.i_dac_pwr_down				= TLV_DEFAULT_DAC_PWR;
	t_tlv_settings.i_adc_pwr_down				= TLV_DEFAULT_ADC_PWR;
	t_tlv_settings.i_mic_pwr_down				= TLV_DEFAULT_MIC_INPUT_PWR;
	t_tlv_settings.i_line_pwr_down				= TLV_DEFAULT_LINE_INPUT_PWR;


	// --- Audio interface format ---
	t_tlv_settings.i_master_mode				= TLV_DEFAULT_MASTER_MODE;
	t_tlv_settings.i_dac_left_right_swap		= TLV_DEFAULT_SWAP_LR;
	t_tlv_settings.i_dac_left_right_phase		= TLV_DEFAULT_PHASE_LR;
	t_tlv_settings.i_input_bit_length			= TLV_DEFAULT_INPUT_BIT_LENGTH;
	t_tlv_settings.i_data_format				= TLV_DEFAULT_DATA_FORMAT;


	// --- SAMPLE RATE ---
	t_tlv_settings.i_clk_in_divider				= TLV_DEFAULT_CLK_IN_DIVIDER;
	t_tlv_settings.i_clk_out_divider			= TLV_DEFAULT_CLK_OUT_DIVIDER;
	uint8_t i_status;
	i_status = TLV_calc_sample_rate_parameters(TLV_DEFAULT_SAMPLE_RATE_STATE);
	if(i_status != GD_SUCCESS)
	{
#if TLV_support_N5110_LCD == true
		LCD_write_string("TLV Init FAIL!");
#endif
		return GD_FAIL;
	}

	// --- Digital interface activation ---
	t_tlv_settings.i_digital_interface_activation =
											TLV_DEFAULT_INTERFACE_ACTIVATION;

	return GD_SUCCESS;
}

/**
 * \brief Save actual settings to EEPROM
 *
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_write_settings_to_EEPROM(void)
{
	// Pointer witch define where to write data
	uint16_t * p_EEPROM_address = (uint16_t *)TLV_EEPROM_START_ADDRESS;

	/* Because write configuration bit by bit will decrease lifetime of EEPROM
	 * memory, program cache 2B of settings and when settings is written to
	 * temporary variable, then value of this variable is written to EEPROM
	 */
	uint16_t i_tmp;

	// Left input channel volume control
	TLV_CREATE_UINT16_REG_L_IN_VOL;
		// OK, all needed is in cache -> write to EEPROM
	eeprom_update_word(p_EEPROM_address++, i_tmp);


	// Right input channel volume control
	TLV_CREATE_UINT16_REG_R_IN_VOL;
	eeprom_update_word(p_EEPROM_address++, i_tmp);


	// Left channel headphone volume control
	TLV_CREATE_UINT16_REG_L_OUT_VOL;
	eeprom_update_word(p_EEPROM_address++, i_tmp);


	// Right channel headphone volume control
	TLV_CREATE_UINT16_REG_R_OUT_VOL;
	eeprom_update_word(p_EEPROM_address++, i_tmp);


	// Analog audio path control
	TLV_CREATE_UINT16_REG_ANA_PATH;
	eeprom_update_word(p_EEPROM_address++, i_tmp);


	// Digital audio path control
	TLV_CREATE_UINT16_REG_DIG_PATH;
	eeprom_update_word(p_EEPROM_address++, i_tmp);


	// Power down control
	TLV_CREATE_UINT16_REG_PWR_DWN;
	eeprom_update_word(p_EEPROM_address++, i_tmp);


	// Audio interface format
	TLV_CREATE_UINT16_REG_INTERFCE_FRMT;
	eeprom_update_word(p_EEPROM_address++, i_tmp);


	// Sample rate control
	TLV_CREATE_UINT16_REG_SMPL_RATE;
	eeprom_update_word(p_EEPROM_address++, i_tmp);


	// Digital interface activation
	TLV_CREATE_UINT16_REG_INTERFCE_ACTIV;
	eeprom_update_word(p_EEPROM_address++, i_tmp);

	// Virtual values
	i_tmp = t_tlv_virtual_settings.i_sampling_rate_state;
	eeprom_update_word(p_EEPROM_address++, i_tmp);

	return GD_SUCCESS;
}

/**
 * \brief Try read configure from EEPROM. If configuration in EEPROM is not\n
 * not valid (TLV_EEPROM_CHECK_BIT not clear), then is returned value\n
 * GD_FAIL and is loaded application default settings. Else load valid settings.
 *
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_read_settings_from_EEPROM(void)
{
	// Test if valid settings is in EEPROM (bit TLV_EEPROM_CHECK_BIT is set)
	/* Read from address where should be TLV_EEPROM_CHECK_BIT (last word).
	 */
	uint16_t i_tmp = eeprom_read_word(
			(uint16_t*)(TLV_EEPROM_END_ADDRESS));

	// Test for TLV_EEPROM_CHECK_BIT (If 0 -> valid settings in EEPROM)
	if( (i_tmp & (1<<TLV_EEPROM_CHECK_BIT)) == 0 )
	{
		// pointer witch can read from EEPROM (set first address)
		uint16_t * p_EEPROM_address = (uint16_t *)TLV_EEPROM_START_ADDRESS;


		// Load value from EEPROM (16 bit) and then save data to variables
		i_tmp = eeprom_read_word(p_EEPROM_address++);

		// Left input volume
			// Write it correct data to structure (shift data and use mask)
		t_tlv_settings.i_L_in_volume = (i_tmp>>TLV_LIV_BIT) & TLV_LIV_MASK;
		// Left input mute
		t_tlv_settings.i_L_in_mute = (i_tmp>>TLV_LIM_BIT) & TLV_LIM_MASK;


		// Load next data from EEPROM
		i_tmp = eeprom_read_word(p_EEPROM_address++);
		// Right input volume
		t_tlv_settings.i_R_in_volume = (i_tmp>>TLV_RIV_BIT) & TLV_RIV_MASK;
		// Right input mute
		t_tlv_settings.i_R_in_mute = (i_tmp>>TLV_RIM_BIT) & TLV_RIM_MASK;


		i_tmp = eeprom_read_word(p_EEPROM_address++);
		// Left output volume
		i_volume_out_backup_L = t_tlv_settings.i_L_out_headphone_volume =
				(i_tmp>>TLV_LHV_BIT) &  TLV_LHV_MASK;
		// Left output zero cross
		t_tlv_settings.i_L_out_headphone_zero_cross =
				(i_tmp>>TLV_LZC_BIT) & TLV_LZC_MASK;


		i_tmp = eeprom_read_word(p_EEPROM_address++);
		// Right output volume
		i_volume_out_backup_R = t_tlv_settings.i_R_out_headphone_volume =
				(i_tmp>>TLV_RHV_BIT) & TLV_RHV_MASK;
		// Right output zero cross
		t_tlv_settings.i_R_out_headphone_zero_cross =
				(i_tmp>>TLV_RZC_BIT) & TLV_RZC_MASK;


		// --- Analog audio path ---
		i_tmp = eeprom_read_word(p_EEPROM_address++);
		// DAC select
		t_tlv_settings.i_dac_select = (i_tmp>>TLV_DAC_BIT) & TLV_DAC_MASK;
		// Bypass
		t_tlv_settings.i_bypass_in_to_out = (i_tmp>>TLV_BYP_BIT) & TLV_BYP_MASK;
		// Sidetone
		t_tlv_settings.i_mic_sidetone_to_in_and_out =
				(i_tmp>>TLV_STE_STA_BIT) & TLV_STE_STA_MASK;
		// Input select for ADC
		t_tlv_settings.i_input_select = (i_tmp>>TLV_INSEL_BIT) & TLV_INSEL_MASK;
		// Microphone mute
		t_tlv_settings.i_mic_mute = (i_tmp>>TLV_MICB_MASK) & TLV_MICB_MASK;
		// Microphone boost 20dB
		t_tlv_settings.i_mic_boost_20dB = (i_tmp>>TLV_MICB_BIT) & TLV_MICB_MASK;


		// --- Digital audio path ---
		i_tmp = eeprom_read_word(p_EEPROM_address++);
		// DAC soft mute
		t_tlv_settings.i_dac_soft_mute = (i_tmp>>TLV_DACM_BIT) & TLV_DACM_MASK;
		// De-emphasis control
		t_tlv_settings.i_deemp = (i_tmp>>TLV_DEEMP_BIT) & TLV_DEEMP_MASK;
		// ADC high-pass filter
		t_tlv_settings.i_adc_high_pass_filter =
				(i_tmp>>TLV_ADCHP_BIT) & TLV_ADCHP_MASK;


		// --- Power down control ---
		i_tmp = eeprom_read_word(p_EEPROM_address++);
		// Device power
		t_tlv_settings.i_device_pwr_down =
				(i_tmp>>TLV_PWR_OFF_BIT) & TLV_PWR_OFF_MASK;
		// Clock
		t_tlv_settings.i_clock_pwr_down =
				(i_tmp>>TLV_PWR_CLK_BIT) & TLV_PWR_CLK_MASK;
		// Oscillator
		t_tlv_settings.i_oscillator_pwr_down =
				(i_tmp>>TLV_PWR_OSC_BIT) & TLV_PWR_OSC_MASK;
		// Outputs
		t_tlv_settings.i_outputs_pwr_down =
				(i_tmp>>TLV_PWR_OUT_BIT) & TLV_PWR_OUT_MASK;
		// DAC
		t_tlv_settings.i_dac_pwr_down =
				(i_tmp>>TLV_PWR_DAC_BIT) & TLV_PWR_DAC_MASK;
		// ADC
		t_tlv_settings.i_adc_pwr_down =
				(i_tmp>>TLV_PWR_ADC_BIT) & TLV_PWR_ADC_MASK;
		// MIC
		t_tlv_settings.i_mic_pwr_down =
				(i_tmp>>TLV_PWR_MIC_BIT) & TLV_PWR_MIC_MASK;
		// LINE
		t_tlv_settings.i_line_pwr_down =
				(i_tmp>>TLV_PWR_LINE_BIT) & TLV_PWR_LINE_MASK;


		// --- Audio interface format ---
		i_tmp = eeprom_read_word(p_EEPROM_address++);
		// Master/slave
		t_tlv_settings.i_master_mode = (i_tmp>>TLV_MS_BIT) & TLV_MS_MASK;
		// DAC left/right swap
		t_tlv_settings.i_dac_left_right_swap =
				(i_tmp>>TLV_LRSWAP_BIT) & TLV_LRSWAP_MASK;
		// DAC left/right phase
		t_tlv_settings.i_dac_left_right_phase =
				(i_tmp>>TLV_LRP_BIT) & TLV_LRP_MASK;
		// Input bit length
		t_tlv_settings.i_input_bit_length = (i_tmp>>TLV_IWL_BIT) & TLV_IWL_MASK;
		// Data format
		t_tlv_settings.i_data_format =
				(i_tmp>>TLV_FORMAT_BIT) & TLV_FORMAT_MASK;


		// --- SAMPLE RATE ---
		i_tmp = eeprom_read_word(p_EEPROM_address++);
		// Clock input divider
		t_tlv_settings.i_clk_in_divider =
				(i_tmp>>TLV_CLKIN_BIT) & TLV_CLKIN_MASK;
		// Clock output divider
		t_tlv_settings.i_clk_out_divider =
				(i_tmp>>TLV_CLKOUT_BIT) & TLV_CLKOUT_MASK;
		// Sampling rate control
		t_tlv_settings.i_sampling_rate = (i_tmp>>TLV_SR_BIT) & TLV_SR_MASK;
		// Base oversampling rate
		t_tlv_settings.i_base_oversampling_rate =
				(i_tmp>>TLV_BOSR_BIT) & TLV_BOSR_MASK;


		// --- Digital interface activation ---
		i_tmp = eeprom_read_word(p_EEPROM_address++);
		t_tlv_settings.i_digital_interface_activation =
				(i_tmp>>TLV_ACT_BIT) & TLV_ACT_MASK;

		// --- Viraul variables ---
		i_tmp = eeprom_read_word(p_EEPROM_address++);
		t_tlv_virtual_settings.i_sampling_rate_state = i_tmp;

		// All work as expected
		return GD_SUCCESS;
	}
	else
	{
		// In EEPROM are not valid data -> save default settings to EEPROM
		TLV_default_settings_to_cfg();
		
		TLV_write_settings_to_EEPROM();
		
		// And return correct error code
		return GD_FAIL;
	}
}



/**
 * \brief Set one codec register
 *
 * As parameter is register address. Program find necessary data and collect\n
 * them into 16 bit packet. This packet is then send to TLV codec
 *
 * @param i_register_address Address of register, that will be set
 * @return GD_SUCCESS if all right
 */
inline GD_RES_CODE TLV_set_codec_register(uint8_t i_register_address)
{
	// According to selected register load correct data to temporary variable

	// Temporary variable
	uint16_t i_tmp;


	switch(i_register_address)
	{
	// Left line input channel volume control
	case TLV_REG_L_IN_VOL:
		TLV_CREATE_UINT16_REG_L_IN_VOL;
		break;
	case TLV_REG_R_IN_VOL:
		TLV_CREATE_UINT16_REG_R_IN_VOL;
		break;
	case TLV_REG_L_OUT_VOL:
		TLV_CREATE_UINT16_REG_L_OUT_VOL;
		break;
	case TLV_REG_R_OUT_VOL:
		TLV_CREATE_UINT16_REG_R_OUT_VOL;
		break;
	case TLV_REG_ANA_PATH:
		TLV_CREATE_UINT16_REG_ANA_PATH;
		break;
	case TLV_REG_DIG_PATH:
		TLV_CREATE_UINT16_REG_DIG_PATH;
		break;
	case TLV_REG_PWR_DWN:
		TLV_CREATE_UINT16_REG_PWR_DWN;
		break;
	case TLV_REG_INTERFCE_FRMT:
		TLV_CREATE_UINT16_REG_INTERFCE_FRMT;
		break;
	case TLV_REG_SMPL_RATE:
		TLV_CREATE_UINT16_REG_SMPL_RATE;
		break;
	case TLV_REG_INTERFCE_ACTIV:
		TLV_CREATE_UINT16_REG_INTERFCE_ACTIV;
		break;
	// RESET - always same data. Just send as value zeros
	case TLV_REG_RESET:
		return I2C_master_send_16bits(	TLV_I2C_ADDRESS,
										(TLV_REG_RESET<<TLV_REG_SHIFT));
		break;

	// If input parameter is wrong...
	default:
		return GD_INCORRECT_PARAMETER;
	}

	// And send data thru I2C
	return I2C_master_send_16bits(TLV_I2C_ADDRESS, i_tmp);
}


/**
 * \brief Set clock parameters according to select sample rate state
 *
 * Because driver do not know on witch clock codec actually runs, there is\n
 * list of possible states in TLV320AIC23_config_table_flash. User select one\n
 * of these states, so indirectly driver know on witch clock codec runs,\n
 * witch sampling ADC and DAC frequency user wants. So this function set all\n
 * needed parameters. This function \b NOT \b SEND \b DATA \b TO \b TLV! \n
 * Just set variables!
 *
 * @param i_sample_rate_state Sample rate states are defined in\n
 *  "Sample rate states" section in .h file
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_calc_sample_rate_parameters(uint8_t i_sample_rate_state)
{
	// OK, now according to selected mode set variables
	switch(i_sample_rate_state)
	{
	// --- 12 MHz clock ---
	case TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_96kHz_DAC_96kHz:
		// Save actual sample rate
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		// Set clock mode (Normal/USB)
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_USB;
		// Sample rate
		t_tlv_settings.i_sampling_rate =			0b0111;
		// Oversampling
		t_tlv_settings.i_base_oversampling_rate =	0;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_88_2kHz_DAC_88_2kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_USB;
		t_tlv_settings.i_sampling_rate =			0b1111;
		t_tlv_settings.i_base_oversampling_rate =	1;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_48kHz_DAC_48kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_USB;
		t_tlv_settings.i_sampling_rate =			0b0000;
		t_tlv_settings.i_base_oversampling_rate =	0;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_44_1kHz_DAC_44_1kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_USB;
		t_tlv_settings.i_sampling_rate =			0b1000;
		t_tlv_settings.i_base_oversampling_rate =	1;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_32kHz_DAC_32kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_USB;
		t_tlv_settings.i_sampling_rate =			0b0110;
		t_tlv_settings.i_base_oversampling_rate =	0;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8_021kHz_DAC_8_021kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_USB;
		t_tlv_settings.i_sampling_rate =			0b1011;
		t_tlv_settings.i_base_oversampling_rate =	1;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8kHz_DAC_8kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_USB;
		t_tlv_settings.i_sampling_rate =			0b0011;
		t_tlv_settings.i_base_oversampling_rate =	0;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_48kHz_DAC_8kHz:
		t_tlv_virtual_settings.i_sampling_rate_state = 	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_USB;
		t_tlv_settings.i_sampling_rate =			0b0001;
		t_tlv_settings.i_base_oversampling_rate =	0;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_44_1kHz_DAC_8_021kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_USB;
		t_tlv_settings.i_sampling_rate =			0b1001;
		t_tlv_settings.i_base_oversampling_rate =	1;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8kHz_DAC_48kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_USB;
		t_tlv_settings.i_sampling_rate =			0b0010;
		t_tlv_settings.i_base_oversampling_rate =	0;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8_021kHz_DAC_44_1kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_USB;
		t_tlv_settings.i_sampling_rate =			0b1010;
		t_tlv_settings.i_base_oversampling_rate =	1;
		break;

	// --- 12.288 MHz clock ---
	case TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_96kHz_DAC_96kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_NORMAL;
		t_tlv_settings.i_sampling_rate =			0b0111;
		t_tlv_settings.i_base_oversampling_rate =	0;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_48kHz_DAC_48kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_NORMAL;
		t_tlv_settings.i_sampling_rate =			0b0000;
		t_tlv_settings.i_base_oversampling_rate =	0;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_32kHz_DAC_32kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_NORMAL;
		t_tlv_settings.i_sampling_rate =			0b0110;
		t_tlv_settings.i_base_oversampling_rate =	0;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_8kHz_DAC_8kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_NORMAL;
		t_tlv_settings.i_sampling_rate =			0b0011;
		t_tlv_settings.i_base_oversampling_rate =	0;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_48kHz_DAC_8kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_NORMAL;
		t_tlv_settings.i_sampling_rate =			0b0001;
		t_tlv_settings.i_base_oversampling_rate =	0;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_8kHz_DAC_48kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_NORMAL;
		t_tlv_settings.i_sampling_rate =			0b0010;
		t_tlv_settings.i_base_oversampling_rate =	0;
		break;

	// --- 11.2896 MHz ---
	case TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_88_2kHz_DAC_88_2kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_NORMAL;
		t_tlv_settings.i_sampling_rate =			0b1111;
		t_tlv_settings.i_base_oversampling_rate =	0;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_44_1kHz_DAC_44_1kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_NORMAL;
		t_tlv_settings.i_sampling_rate =			0b1000;
		t_tlv_settings.i_base_oversampling_rate =	0;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_8_021kHz_DAC_8_021kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_NORMAL;
		t_tlv_settings.i_sampling_rate =			0b1011;
		t_tlv_settings.i_base_oversampling_rate =	0;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_44_1kHz_DAC_8_021kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_NORMAL;
		t_tlv_settings.i_sampling_rate =			0b1001;
		t_tlv_settings.i_base_oversampling_rate =	0;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_8_021kHz_DAC_44_1kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_NORMAL;
		t_tlv_settings.i_sampling_rate =			0b1010;
		t_tlv_settings.i_base_oversampling_rate =	0;
		break;

	// --- 18.432 MHz ---
	case TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_96kHz_DAC_96kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_NORMAL;
		t_tlv_settings.i_sampling_rate =			0b0111;
		t_tlv_settings.i_base_oversampling_rate =	1;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_48kHz_DAC_48kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_NORMAL;
		t_tlv_settings.i_sampling_rate =			0b0000;
		t_tlv_settings.i_base_oversampling_rate =	1;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_32kHz_DAC_32kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_NORMAL;
		t_tlv_settings.i_sampling_rate =			0b0110;
		t_tlv_settings.i_base_oversampling_rate =	1;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_8kHz_DAC_8kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_NORMAL;
		t_tlv_settings.i_sampling_rate =			0b0011;
		t_tlv_settings.i_base_oversampling_rate =	1;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_48kHz_DAC_8kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_NORMAL;
		t_tlv_settings.i_sampling_rate =			0b0001;
		t_tlv_settings.i_base_oversampling_rate =	1;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_8kHz_DAC_48kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_NORMAL;
		t_tlv_settings.i_sampling_rate =			0b0010;
		t_tlv_settings.i_base_oversampling_rate =	1;
		break;

	// --- 16.9344 MHz ---
	case TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_88_2kHz_DAC_88_2kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_NORMAL;
		t_tlv_settings.i_sampling_rate =			0b1111;
		t_tlv_settings.i_base_oversampling_rate =	1;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_44_1kHz_DAC_44_1kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_NORMAL;
		t_tlv_settings.i_sampling_rate =			0b1000;
		t_tlv_settings.i_base_oversampling_rate =	1;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_8_021kHz_DAC_8_021kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_NORMAL;
		t_tlv_settings.i_sampling_rate =			0b1011;
		t_tlv_settings.i_base_oversampling_rate =	1;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_44_1kHz_DAC_8_021kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_NORMAL;
		t_tlv_settings.i_sampling_rate =			0b1001;
		t_tlv_settings.i_base_oversampling_rate =	1;
		break;
	case TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_8_021kHz_DAC_44_1kHz:
		t_tlv_virtual_settings.i_sampling_rate_state =	i_sample_rate_state;
		t_tlv_settings.i_clock_mode_select =		TLV_SR_MODE_NORMAL;
		t_tlv_settings.i_sampling_rate =			0b1010;
		t_tlv_settings.i_base_oversampling_rate =	1;
		break;

	// If selected state not found
	default:
		return GD_INCORRECT_PARAMETER;
	}


	// If all OK
	return GD_SUCCESS;
}


/**
 * \brief Grab input value and recalculate it to value for TLV codec
 *
 * This function \b NOT \b SEND \b DATA \b TO \b TLV! Just set variables!
 *
 * @param i_sidetone_virtual_value 0: disabled; 1: -18dB ; 2: -12dB ; 3 -9dB;\n
 * 4 -6dB ; 5 0dB
 * @return GD_SUCCESS if all right
 */
GD_RES_CODE TLV_calc_sidetone_bits_for_TLV_register(
											uint8_t i_sidetone_virtual_value)
{
	// Now, according to value set sidetone variable for tlv
	switch(i_sidetone_virtual_value)
	{
	case 0:
		t_tlv_settings.i_mic_sidetone_to_in_and_out = TLV_SIDETONE_DISABLED;
		break;
	case 1:
		t_tlv_settings.i_mic_sidetone_to_in_and_out = TLV_SIDETONE_MINUS_18dB;
		break;
	case 2:
		t_tlv_settings.i_mic_sidetone_to_in_and_out = TLV_SIDETONE_MINUS_12dB;
		break;
	case 3:
		t_tlv_settings.i_mic_sidetone_to_in_and_out = TLV_SIDETONE_MINUS_9dB;
		break;
	case 4:
		t_tlv_settings.i_mic_sidetone_to_in_and_out = TLV_SIDETONE_MINUS_6dB;
		break;
	case 5:
		t_tlv_settings.i_mic_sidetone_to_in_and_out = TLV_SIDETONE_0dB;
		break;
	// If not defined -> incorrect parameter
	default:
		return GD_INCORRECT_PARAMETER;
	}

	return GD_SUCCESS;
}


































