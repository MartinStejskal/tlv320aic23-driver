/**
 * \file
 *
 * \brief Generic driver audio codec TLV320AIC23B
 *
 * Used for basic operations (init, set volume, mute, ...) with audio codec\n
 * TLV320AIC23B.\n
 * It need HAL driver (which depend on used architecture) to work properly.\n
 *
 * Created:  14.12.2013
 * Modified: 28.12.2013
 *
 * \version 0.1.1
 * \author Martin Stejskal
 */

/* Tested settings:
 * 48 kHz @ 16b @ 12.288 MHz
 * In level: 0dB (not muted)
 * Out level: 0dB (not muted, zero cross enabled)
 * Analog path:
 *   DAC enabled
 *   Bypass in and out - disabled
 *   Sidetone - disabled
 *   input - line in
 *   MIC muted
 *   MIC boost - disabled
 * Digital audio path:
 *   DAC soft mute - disabled
 *   De-emphasis control - disabled
 *   ADC high-pass filter - disabled
 * Power down control:
 *   device - on
 *   clock - on
 *   oscillator - off (if you want crystal instead, set this to ON)
 *   outputs - on
 *   DAC - on
 *   ADC - on
 *   MIC input - on
 *   LINE input - on
 * Audio interface format
 *   Master - true
 *   DAC left/right swap - disabled
 *   DAC left/right phase - disabled
 *   Input bit length - 16 bit
 *   Data format - I2S
 * Sample rate:
 *   CLK in  div - 1x (no change)
 *   CLK out div - 1/2x
 *   Sample rate - set according to datasheet (48 kHz)
 *   Base oversampling rate - 256
 *   USB/normal - normal
 */

#ifndef _tlv320aic23b_H_
#define _tlv320aic23b_H_

//===========================| Included libraries |============================
/* HAL for codec !!! Please include correct driver for used architecture!
 * Also in included driver should be defined enumeration tlv320aic23b_status_t
 */
#include "tlv320aic23b_HAL_AVR32_HW_interface.h"


//=================================| Options |=================================
/**
 * \name Basic TLV codec settings
 *
 * When options are 1 (true) and 0 (false) it is highly recommended use\n
 * 1 or 0 instead of true and false
 *
 * @{
 */
/// \brief Default value for input volume (0b10111 = 0dB)
#define TLV_DEFAULT_VOLUME_IN           0b10111

/**
 * \brief Left input mute
 *
 * Options: 1 (true),  0 (false)
 */
#define TLV_DEFAULT_L_IN_MUTE           0

/**
 * \brief Right input mute
 *
 * Options: 1 (true),  0 (false)
 */
#define TLV_DEFAULT_R_IN_MUTE           0

/// \brief Default value for output volume (0b1111001 = 0dB ; min: 0b0110001)
#define TLV_DEFAULT_VOLUME_OUT          0b1010101

/**
 * \brief Left headphone mute
 *
 * Options: 1 (true),  0 (false)
 */
#define TLV_DEFAULT_L_HEADPHONE_MUTE    0

/**
 * \brief Right headphone mute
 *
 * Options: 1 (true),  0 (false)
 */
#define TLV_DEFAULT_R_HEADPHONE_MUTE    0

/**
 * \brief Microphone mute
 *
 * Options: 1 (true),  0 (false)
 */
#define TLV_DEFAULT_MIC_MUTE            1

/**
 * \brief Microphone boost 20dB
 *
 * Options: 1 (true),  0 (false)
 */
#define TLV_DEFAULT_MIC_BOOST           0

/**
 * \brief Default codec input select
 *
 * Options: TLV_LINE_IN_CH, TLV_MIC_CH
 */
#define TLV_DEFAULT_INPUT_SELECT        TLV_LINE_IN_CH
/// @}





/**
 * \brief Option for generic driver support (1 (true), 0 (false))
 *
 * Metadata for generic driver: TLV320AIC23_metadata
 */
#define TLV_support_generic_driver              0

/// \brief Option for LCD display NOKIA 5110
#define TLV_support_N5110_LCD                   1









/**
 * \name Advanced default codec settings
 *
 * Because cadec offer many options it is good choice to define default values.
 *
 * @{
 */
/**
 * \brief Data format
 *
 *  Options: TLV_FRMT_MSB_FIRST_RIGHT_ALIGNED,\n
 *  TLV_FRMT_MSB_FIRST_LEFT_ALIGNED, TLV_FRMT_I2S_MSB_FIRST, TLV_FRMT_DSP
 */
#define TLV_DEFAULT_DATA_FORMAT         TLV_FRMT_I2S_MSB_FIRST

/**
 * \brief Bit length (word width)
 *
 * Options: TLV_16BIT, TLV_20BIT, TLV_24BIT, TLV_32BIT
 */
#define TLV_DEFAULT_INPUT_BIT_LENGTH    TLV_16BIT

/**
 * \brief Master/slave switch
 *
 * There programmer have two options:\n
 * 1) TLV will operate as master -> set 1 (true)\n
 * 2) TLV will operate as slave  -> set 0 (false)\n
 * \n
 * Important note: because is possible to change TLV master/slave mode, in\n
 * some designs that means to set I/O pins correctly. So, please, look to\n
 * your device board driver to avoid complications ;)
 */
#define TLV_DEFAULT_MASTER_MODE         1

/**
 * \brief Define witch sample rate state is default
 *
 * For complete list please search for "Sample rate states" in .h file
 */
#define TLV_DEFAULT_SAMPLE_RATE_STATE   \
        TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_48kHz_DAC_48kHz

/**
 * \brief Device power
 *
 * Options: TLV_PWR_ON (device will be on), TLV_PWR_OFF (power down mode)
 */
#define TLV_DEFAULT_DEVICE_PWR          TLV_PWR_ON

/**
 * \brief Clock power
 *
 * Options: TLV_PWR_ON, TLV_PWR_OFF
 */
#define TLV_DEFAULT_CLOCK_PWR           TLV_PWR_ON

/**
 * \brief Oscillator power
 *
 * Options: TLV_PWR_ON, TLV_PWR_OFF
 */
#define TLV_DEFAULT_OSCILLATOR_PWR      TLV_PWR_OFF

/**
 * \brief Outputs power
 *
 * Options: TLV_PWR_ON, TLV_PWR_OFF
 */
#define TLV_DEFAULT_OUTPUTS_PWR         TLV_PWR_ON

/**
 * \brief DAC power
 *
 * Options: TLV_PWR_ON, TLV_PWR_OFF
 */
#define TLV_DEFAULT_DAC_PWR             TLV_PWR_ON

/**
 * \brief ADC power
 *
 * Options: TLV_PWR_ON, TLV_PWR_OFF
 */
#define TLV_DEFAULT_ADC_PWR             TLV_PWR_ON

/**
 * \brief Microphone input power
 *
 * Options: TLV_PWR_ON, TLV_PWR_OFF
 */
#define TLV_DEFAULT_MIC_INPUT_PWR       TLV_PWR_OFF

/**
 * \brief Line input power
 */
#define TLV_DEFAULT_LINE_INPUT_PWR      TLV_PWR_ON

/**
 * \brief Clock divider for MCLK coming to pin XTI/MCLK
 *
 * Options: true (divider 2), false (no divider)
 */
#define TLV_DEFAULT_CLK_IN_DIVIDER      0

/**
 * \brief Clock divider for CLKOUT pin
 *
 * Options: true (divider 2), false (no divider)
 */
#define TLV_DEFAULT_CLK_OUT_DIVIDER     1

/**
 * \brief Left output headphone zero cross
 *
 * Options: 1 (true),  0 (false)
 */
#define TLV_DEFAULT_L_OUT_HEADPHONE_ZERO_CROSS          1

/**
 * \brief Right output headphone zero cross
 *
 * Options: 1 (true),  0 (false)
 */
#define TLV_DEFAULT_R_OUT_HEADPHONE_ZERO_CROSS          1

/**
 * \brief Bypass ("connect") input and output
 *
 * Options: 1 (true),  0 (false)
 */
#define TLV_DEFAULT_BYPASS_IN_OUT                       0

/**
 * \brief Sidetone from microphone
 *
 * Options: TLV_SIDETONE_DISABLED, TLV_SIDETONE_MINUS_18dB,\n
 * TLV_SIDETONE_MINUS_12dB, TLV_SIDETONE_MINUS_9dB, TLV_SIDETONE_MINUS_6dB,\n
 * TLV_SIDETONE_0dB
 */
#define TLV_DEFAULT_SIDETONE            TLV_SIDETONE_DISABLED

/**
 * \brief DAC soft mute
 *
 * Options: 1 (true),  0 (false)
 */
#define TLV_DEFAULT_DAC_SOFT_MUTE                       0

/**
 * \brief De-emphasis
 *
 * Options: TLV_DEEMP_DISABLED, TLV_DEEMP_32kHz, TLV_DEEMP_44_1kHz,\n
 * TLV_DEEMP_48kHz
 */
#define TLV_DEFAULT_DEEMP                       TLV_DEEMP_DISABLED

/**
 * \brief ADC high-pass filter
 *
 * Options: 1 (true),  0 (false)
 */
#define TLV_DEFAULT_ADC_HP_FILTER       0

/**
 * \brief DAC swap left and right channel
 *
 * Options: 1 (true),  0 (false)
 */
#define TLV_DEFAULT_SWAP_LR             0

/**
 * \brief DAC left/right phase
 *
 * Options: 1 (true), 0 (false)\n
 * 0 - Right channel on, LRCIN high\n
 * 1 - Right channel on, LRCIN low\n
 * DSP mode\n
 * 0 = MSB is available on 1st BCLK rising edge after LRCIN rising edge\n
 * 1 = MSB is available on 2nd BCLK rising edge after LRCIN rising edge *
 */
#define TLV_DEFAULT_PHASE_LR            0

/**
 * \brief DAC select (Analog audio path)
 *
 * Options: 1 (true) (recommended), 0 (false)
 */
#define TLV_DEFAULT_DAC_SELECT          1

/**
 * \brief Interface activation
 *
 * Options: 1 (true) (recommended), 0 (false)
 */
#define TLV_DEFAULT_INTERFACE_ACTIVATION        1

/// @}


//===============================| Structures |================================


typedef struct{
  // --- Additional variables - not directly connected with TLV registers ---

  /* Define in witch state is actual settings is. This number is just command
   * ID, so GUI just call get_settings(cmd id any of sample rate state)
   * and as value get command ID. When call get_settings(gotten cmd ID)
   * user get description of actual settings
   */
  uint8_t i_sampling_rate_state;
  /* Mute flag for left and right headphone. Because mute flag is not in
   * codec, driver make own virtual mute flag, so it can easily offer generic
   * driver simple information, instead of volume value
   */
  uint8_t i_L_headphone_mute_flag;
  uint8_t i_R_headphone_mute_flag;

  /* Because sidetone value on codec is not easy to set (bit combination is
   * little bit tricky), there is virtual variable in witch will be settings
   * more transparent. Functions then recalculate this value to value
   * suitable for TLV register
   */
  uint8_t i_mic_sidetone_to_in_and_out_virtual;
}t_tlv_virtual_options;


//===========================| Additional includes |===========================
#if TLV_support_N5110_LCD == 1
// When error occurs, then is shown to LCD
#include "LCD_5110.h"
#endif

#if TLV_support_generic_driver == 1
// If used generic driver, get actual structures
#include "ALPS_generic_driver.h"
// Also say compiler, that there exist settings on flash
extern const gd_config_struct TLV320AIC23_config_table_flash[] PROGMEM;
// And metadata
extern const gd_metadata TLV320AIC23_metadata;

#else
/**
 * \brief Error codes common with generic driver
 *
 * Please note, that this enum MUST be same with as enum in generic driver!
 */
#ifndef _GENERIC_DRIVER_H_
#ifndef GD_RES_CODE_DEFINED

#define GD_RES_CODE_DEFINED
typedef enum{
        GD_SUCCESS =                   0,
        GD_FAIL =                      1,
        GD_INCORRECT_PARAMETER =       2,
        GD_INCORRECT_CMD_ID =          3,
        GD_CMD_ID_NOT_EQUAL_IN_FLASH = 4,
        GD_INCORRECT_DEVICE_ID =       5
} GD_RES_CODE;
#endif
#endif

#endif
//===============================| Definitions |===============================
/**
 * \name TLV320AIC23B register addresses
 *
 * Because addresses for codec registers are 7 bit and their width is 9 bit\n
 * communication is a little bit complex. So, there are names for registers
 *
 * @{
 */
//! \brief Left line input channel volume control
#define TLV_REG_L_IN_VOL                0
//! \brief Right line input channel volume control
#define TLV_REG_R_IN_VOL                1
//! \brief Left channel headphone volume control
#define TLV_REG_L_OUT_VOL               2
//! \brief Right channel headphone volume control
#define TLV_REG_R_OUT_VOL               3
//! \brief Analog audio path control
#define TLV_REG_ANA_PATH                4
//! \brief Digital audio path control
#define TLV_REG_DIG_PATH                5
//! \brief Power down control
#define TLV_REG_PWR_DWN                 6
//! \brief Digital audio interface format
#define TLV_REG_INTERFCE_FRMT           7
//! \brief Sample rate control
#define TLV_REG_SMPL_RATE               8
//! \brief Digital interface activation
#define TLV_REG_INTERFCE_ACTIV          9
//! \brief Reset
#define TLV_REG_RESET                   15

/** \brief Define how many rotations must be done to shift address to "right\n
 * place". Also this number is width for data
 */
#define TLV_REG_SHIFT                   9
/// @}


/// \brief Define value, when codec set headphone output as muted
#define TLV_HEADPHONE_MUTE_VALUE                0b0110000

/**
 * \name Bit positions in TLV register and bit masks
 *
 * Instead of writing bit per bit we can use symbolic names. Also there are\n
 * defined bit masks for converting data from TLV codec format to\n
 * t_tlv_options structure. Mask just define how many LSB bits are valid\n
 * after shift by TLV_xyz_BIT
 *
 * @{
 */
//! \brief Settings check bit
#define TLV_SETTINGS_CHECK_BIT          15
//! \brief Left/right simultaneous volume/mute update: 0 = Disabled
#define TLV_LRS_BIT                     8
#define TLV_LRS_MASK            0x0001
//! \brief Left line input mute 0 = Normal 1 = Muted
#define TLV_LIM_BIT                     7
#define TLV_LIM_MASK            0x0001
//! \brief Left line input volume control (5 bits ; 10111 = 0 dB default)
#define TLV_LIV_BIT                     0
#define TLV_LIV_MASK            0x001F
//-----------------------------------------------------------------------------
//! \brief Right/left simultaneous volume/mute update: 0 = Disabled
#define TLV_RLS_BIT                     8
#define TLV_RLS_MASK            0x0001
//! \brief Right line input mute 0 = Normal 1 = Muted
#define TLV_RIM_BIT                     7
#define TLV_RIM_MASK            0x0001
//! \brief Right line input volume control (5 bits ; 10111 = 0 dB default)
#define TLV_RIV_BIT                     0
#define TLV_RIV_MASK            0x001F
//-----------------------------------------------------------------------------
//! \brief Left-channel zero-cross detect. Zero-cross detect 0 = Off 1 = On
#define TLV_LZC_BIT                     7
#define TLV_LZC_MASK            0x0001
//! \brief Left Headphone volume control (1111001 = 0 dB default)
#define TLV_LHV_BIT                     0
#define TLV_LHV_MASK            0x007F
//-----------------------------------------------------------------------------
//! \brief Right-channel zero-cross detect. Zero-cross detect 0 = Off 1 = On
#define TLV_RZC_BIT                     7
#define TLV_RZC_MASK            0x0001
//! \brief Right headphone volume control (1111001 = 0 dB default)
#define TLV_RHV_BIT                     0
#define TLV_RHV_MASK            0x007F
//-----------------------------------------------------------------------------
//! \brief STA[2:0] and STE define added sidetone. Please refer datasheet (p22)
#define TLV_STA_BIT                     6
#define TLV_STA_MASK            0x0007
//! \brief STA[2:0] and STE define added sidetone. Please refer datasheet (p22)
#define TLV_STE_BIT                     5
#define TLV_STE_MASK            0x0001
//! \brief STA[2:0] and STE together (can be used for one configure variable)
#define TLV_STE_STA_BIT         5
#define TLV_STE_STA_MASK        0x000F
//! \brief DAC select: 0 = DAC off 1 = DAC selected
#define TLV_DAC_BIT                     4
#define TLV_DAC_MASK            0x0001
//! \brief Bypass: 0 = Disabled 1 = Enabled
#define TLV_BYP_BIT                     3
#define TLV_BYP_MASK            0x0001
//! \brief Input select for ADC 0 = Line 1 = Microphone
#define TLV_INSEL_BIT           2
#define TLV_INSEL_MASK          0x0001
//! \brief Microphone mute 0 = Normal 1 = Muted
#define TLV_MICM_BIT            1
#define TLV_MICM_MASK           0x0001
//! \brief Microphone boost 0=dB 1 = 20dB
#define TLV_MICB_BIT            0
#define TLV_MICB_MASK           0x0001
//-----------------------------------------------------------------------------
//! \brief DAC soft mute: 0 = Disabled 1 = Enabled
#define TLV_DACM_BIT            3
#define TLV_DACM_MASK           0x0001
/** \brief De-emphasis control 00 = Disabled
 * 01 = 32 kHz 10 = 44.1 kHz 11 = 48 kHz (2 bits)
 */
#define TLV_DEEMP_BIT           1
#define TLV_DEEMP_MASK          0x0003
//! \brief ADC high-pass filter 1 = Disabled 0 = Enabled
#define TLV_ADCHP_BIT           0
#define TLV_ADCHP_MASK          0x0001
//-----------------------------------------------------------------------------
//! \brief Device power 0 = On 1 = Off
#define TLV_PWR_OFF_BIT                 7
#define TLV_PWR_OFF_MASK                0x0001
//! \brief Clock 0 = On 1 = Off
#define TLV_PWR_CLK_BIT                 6
#define TLV_PWR_CLK_MASK                0x0001
//! \brief Oscillator 0 = On 1 = Off
#define TLV_PWR_OSC_BIT                 5
#define TLV_PWR_OSC_MASK                0x0001
//! \brief Outputs 0 = On 1 = Off
#define TLV_PWR_OUT_BIT                 4
#define TLV_PWR_OUT_MASK                0x0001
//! \brief DAC 0 = On 1 = Off
#define TLV_PWR_DAC_BIT                 3
#define TLV_PWR_DAC_MASK                0x0001
//! \brief ADC 0 = On 1 = Off
#define TLV_PWR_ADC_BIT                 2
#define TLV_PWR_ADC_MASK                0x0001
//! \brief Microphone input 0 = On 1 = Off
#define TLV_PWR_MIC_BIT                 1
#define TLV_PWR_MIC_MASK                0x0001
//! \brief Line input 0 = On 1 = Off
#define TLV_PWR_LINE_BIT                0
#define TLV_PWR_LINE_MASK               0x0001
//-----------------------------------------------------------------------------
//! \brief Master/slave mode 0 = Slave 1 = Master
#define TLV_MS_BIT                      6
#define TLV_MS_MASK                     0x0001
//! \brief DAC left/right swap 0 = Disabled 1 = Enabled
#define TLV_LRSWAP_BIT          5
#define TLV_LRSWAP_MASK         0x0001
/**
 * \brief DAC left/right phase:\n
 * 0 = Right channel on, LRCIN high\n
 * 1 = Right channel on, LRCIN low
 */
#define TLV_LRP_BIT                     4
#define TLV_LRP_MASK            0x0001
//! \brief Input bit length 00 = 16 bit 01 = 20 bit 10 = 24 bit 11 = 32 bit
#define TLV_IWL_BIT                     2
#define TLV_IWL_MASK            0x0003
/**
 * \brief Data format\n
 * 11 = DSP format, frame sync followed by two data words\n
 * 10 = I2S format, MSB first, left 1 aligned\n
 * 01 = MSB first, left aligned\n
 * 00 = MSB first, right aligned\n
 */
#define TLV_FORMAT_BIT          0
#define TLV_FORMAT_MASK         0x0003
//-----------------------------------------------------------------------------
//! \brief Clock input divider 0 = MCLK 1 = MCLK/2
#define TLV_CLKIN_BIT           6
#define TLV_CLKIN_MASK          0x0001
//! \brief Clock output divider 0 = MCLK 1 = MCLK/2
#define TLV_CLKOUT_BIT          7
#define TLV_CLKOUT_MASK         0x0001
/**
 * \brief Sampling rate control (see Sections 3.3.2.1 AND 3.3.2.2 in \n
 * datasheet). 4 bits
 */
#define TLV_SR_BIT                      2
#define TLV_SR_MASK                     0x000F
/**
 * \brief Base oversampling rate:\n
 * USB mode: 0 = 250 fs 1 = 272 fs\n
 * Normal mode: 0 = 256 fs 1 = 384 fs\n
 */
#define TLV_BOSR_BIT            1
#define TLV_BOSR_MASK           0x0001
//! \brief USB/Normal Clock mode select: 0 = Normal 1 = USB
#define TLV_USB_NORMAL_BIT      0
//-----------------------------------------------------------------------------
//! \brief Activate digital interface
#define TLV_ACT_BIT                     0
#define TLV_ACT_MASK            0x0001
/// @}

/**
 * \name Names for left, right and both channels
 *
 * @{
 */
//! \brief Name for left channel
#define TLV_LEFT                1
//! \brief Name for right channel
#define TLV_RIGHT               2
//! \brief Name for both channels
#define TLV_BOTH                3
/// @}
/**
 * \name Symbolic name for inputs
 *
 * @{
 */
//! \brief Name for line in channel
#define TLV_LINE_IN_CH  0
//! \brief Name for mic channel
#define TLV_MIC_CH              1
/**
 * @}
 *
 * \name Power states
 *
 * @{
 */
#define TLV_PWR_ON                      0
#define TLV_PWR_OFF                     1
/// @}
/**
 * \name Input bit length
 *
 * For user comfort there are named binary values for codec settings
 * @{
 */
/// \brief 16 bit wide samples
#define TLV_16BIT               0b00
/// \brief 20 bit wide samples
#define TLV_20BIT               0b01
/// \brief 24 bit wide samples
#define TLV_24BIT               0b10
/// \brief 32 bit wide samples
#define TLV_32BIT               0b11
/// @}
/**
 * \name Data format
 *
 * Symbolic names for data format
 *
 * @{
 */
/// \brief MSB first, right aligned
#define TLV_FRMT_MSB_FIRST_RIGHT_ALIGNED                0b00
/// \brief MSB first, left aligned
#define TLV_FRMT_MSB_FIRST_LEFT_ALIGNED                 0b01
/// \brief I2S format, MSB first, left - 1 aligned
#define TLV_FRMT_I2S_MSB_FIRST                                  0b10
/// \brief DSP format, frame sync followed by two data words
#define TLV_FRMT_DSP                                                    0b11
/// @}
/**
 * \name Sampling rate modes
 *
 * @{
 */
/// \brief Clock for TLV - normal mode
#define TLV_SR_MODE_NORMAL                                              0
/// \brief Clock for TLV - USB mode
#define TLV_SR_MODE_USB                                                 1
/// @}
/**
 * \name Sidetone options (for TLV register)
 *
 * @{
 */
/// \brief Sidetone disabled
#define TLV_SIDETONE_DISABLED                                   0b0000
/// \brief Sidetone -18 dB
#define TLV_SIDETONE_MINUS_18dB                                 0b1011
/// \brief Sidetone -12 dB
#define TLV_SIDETONE_MINUS_12dB                                 0b1010
/// \brief Sidetone -9  dB
#define TLV_SIDETONE_MINUS_9dB                                  0b1001
/// \brief Sidetone -6  dB
#define TLV_SIDETONE_MINUS_6dB                                  0b1000
/// \brief Sidetone 0dB
#define TLV_SIDETONE_0dB                                                0b1100
/// @}
/**
 * \name De-emphasis options
 *
 * @{
 */
/// \brief De-emphasis disabled
#define TLV_DEEMP_DISABLED                                              0b00
/// \brief De-emphasis 32 kHz
#define TLV_DEEMP_32kHz                                                 0b01
/// \brief De-emphasis 44.1 kHz
#define TLV_DEEMP_44_1kHz                                               0b10
/// \brief De-emphasis 48 kHz
#define TLV_DEEMP_48kHz                                                 0b11
/// @}
/**
 * \name Sample rate states
 *
 * Because driver do not know on witch frequency will codec work, driver can\n
 * not determine possible sample rates. So there is list of possible sample\n
 * rate states and user select one of them (because user know on witch clock\n
 * will codec run). Number of every option is basically command ID. When this\n
 * number is used as command ID, called function set sample rate parameters\n
 * according this variable. For example if\n
 * TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_96kHz_DAC_96kHz is defined as 9, then\n
 * when user call set_settings(metadata, 9, 0) in generic driver, called\n
 * function set configuration bits for MCLK 12 MHz, Fadc = 96 kHz and\n
 * Fdac = 96 kHz.
 *
 * @{
 */
/// \brief MCLK = 12      MHz, Fadc = 96    kHz, Fdac = 96    kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_96kHz_DAC_96kHz    15   // CMD ID
/// \brief Define first option for sample rate list
#define TLV_SAMPLE_RATE_FIRST_OPTION    \
                TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_96kHz_DAC_96kHz

/// \brief MCLK = 12      MHz, Fadc = 88.2  kHz, Fdac = 88.2  kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_88_2kHz_DAC_88_2kHz        \
        TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_96kHz_DAC_96kHz+1

/// \brief MCLK = 12      MHz, Fadc = 48    kHz, Fdac = 48    kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_48kHz_DAC_48kHz            \
        TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_88_2kHz_DAC_88_2kHz+1

/// \brief MCLK = 12      MHz, Fadc = 44.1  kHz, Fdac = 44.1  kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_44_1kHz_DAC_44_1kHz        \
        TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_48kHz_DAC_48kHz+1

/// \brief MCLK = 12      MHz, Fadc = 32    kHz, Fdac = 32    kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_32kHz_DAC_32kHz            \
        TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_44_1kHz_DAC_44_1kHz+1

/// \brief MCLK = 12      MHz, Fadc = 8.021 kHz, Fdac = 8.021 kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8_021kHz_DAC_8_021kHz      \
        TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_32kHz_DAC_32kHz+1

/// \brief MCKL = 12      MHz, Fadc = 8     kHz, Fdac = 8     kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8kHz_DAC_8kHz              \
        TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8_021kHz_DAC_8_021kHz+1

/// \brief MCLK = 12      MHz, Fadc = 48    kHz, Fdac = 8     kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_48kHz_DAC_8kHz             \
        TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8kHz_DAC_8kHz+1

/// \brief MCLK = 12      MHz, Fadc = 44.1  kHz, Fdac = 8.021 kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_44_1kHz_DAC_8_021kHz       \
                TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_48kHz_DAC_8kHz+1

/// \brief MCLK = 12      MHz, Fadc = 8     kHz, Fdac = 48    kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8kHz_DAC_48kHz             \
        TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_44_1kHz_DAC_8_021kHz+1

/// \brief MCLK = 12      MHz, Fadc = 8.021 kHz, Fdac = 44.1  kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8_021kHz_DAC_44_1kHz       \
        TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8kHz_DAC_48kHz+1

/// \brief MCLK = 12.288  MHz, Fadc = 96    kHz, Fdac = 96    kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_96kHz_DAC_96kHz        \
        TLV_SAMPLE_RATE_STATE_MCLK_12MHz_ADC_8_021kHz_DAC_44_1kHz+1

/// \brief MCLK = 12.288  MHz, Fadc = 48    kHz, Fdac = 48    kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_48kHz_DAC_48kHz        \
        TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_96kHz_DAC_96kHz+1

/// \brief MCLK = 12.288  MHz, Fadc = 32    kHz, Fdac = 32    kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_32kHz_DAC_32kHz        \
        TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_48kHz_DAC_48kHz+1

/// \brief MCLK = 12.288  MHz, Fadc = 8     kHz, Fadc = 8     kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_8kHz_DAC_8kHz          \
        TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_32kHz_DAC_32kHz+1

/// \brief MCLK = 12.288  MHz, Fadc = 48    kHz, Fdac = 8     kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_48kHz_DAC_8kHz         \
        TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_8kHz_DAC_8kHz+1

/// \brief MCLK = 12.288  MHz, Fadc = 8     kHz, Fdac = 48    kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_8kHz_DAC_48kHz         \
        TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_48kHz_DAC_8kHz+1

/// \brief MCLK = 11.2896 MHz, Fadc = 88.2  kHz, Fdac = 88.2  kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_88_2kHz_DAC_88_2kHz   \
        TLV_SAMPLE_RATE_STATE_MCLK_12_288MHz_ADC_8kHz_DAC_48kHz+1

/// \brief MCLK = 11.2896 MHz, Fadc = 44.1  kHz, Fdac = 44.1  kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_44_1kHz_DAC_44_1kHz   \
        TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_88_2kHz_DAC_88_2kHz+1

/// \brief MCLK = 11.2896 MHz, Fadc = 8.021 kHz, Fdac = 8.021 kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_8_021kHz_DAC_8_021kHz \
        TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_44_1kHz_DAC_44_1kHz+1

/// \brief MCLK = 11.2896 MHz, Fadc = 44.1  kHz, Fdac = 8.021 kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_44_1kHz_DAC_8_021kHz  \
        TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_8_021kHz_DAC_8_021kHz+1

/// \brief MCLK = 11.2896 MHz, Fadc = 8.021 kHz, Fdac = 44.1  kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_8_021kHz_DAC_44_1kHz  \
        TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_44_1kHz_DAC_8_021kHz+1

/// \brief MCLK = 18.432  MHz, Fadc = 96    kHz, Fdac = 96    kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_96kHz_DAC_96kHz        \
        TLV_SAMPLE_RATE_STATE_MCLK_11_2896MHz_ADC_8_021kHz_DAC_44_1kHz+1

/// \brief MCLK = 18.432  MHz, Fadc = 48    kHz, Fdac = 48    kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_48kHz_DAC_48kHz        \
        TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_96kHz_DAC_96kHz+1

/// \brief MCLK = 18.432  MHz, Fadc = 32    kHz, Fdac = 32    kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_32kHz_DAC_32kHz        \
        TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_48kHz_DAC_48kHz+1

/// \brief MCLK = 18.432  MHz, Fadc = 8     kHz, Fdac = 8     kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_8kHz_DAC_8kHz          \
        TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_32kHz_DAC_32kHz+1

/// \brief MCLK = 18.432  MHz, Fadc = 48    kHz, Fdac = 8     kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_48kHz_DAC_8kHz         \
        TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_8kHz_DAC_8kHz+1

/// \brief MCLK = 18.432  MHz, Fadc = 8     kHz, Fdac = 48    kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_8kHz_DAC_48kHz         \
        TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_48kHz_DAC_8kHz+1

/// \brief MCLK = 16.9344 MHz, Fadc = 88.2  kHz, Fdac = 88.2  kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_88_2kHz_DAC_88_2kHz   \
        TLV_SAMPLE_RATE_STATE_MCLK_18_432MHz_ADC_8kHz_DAC_48kHz+1

/// \brief MCLK = 16.9344 MHz, Fadc = 44.1  kHz, Fdac = 44.1  kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_44_1kHz_DAC_44_1kHz   \
        TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_88_2kHz_DAC_88_2kHz+1

/// \brief MCLK = 16.9344 MHz, Fadc = 8.021 kHz, Fdac = 8.021 kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_8_021kHz_DAC_8_021kHz \
        TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_44_1kHz_DAC_44_1kHz+1

/// \brief MCLK = 16.9344 MHz, Fadc = 44.1  kHz, Fdac = 8.021 kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_44_1kHz_DAC_8_021kHz  \
        TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_8_021kHz_DAC_8_021kHz+1

/// \brief MCLK = 16.9344 MHz, Fadc = 8.021 kHz, Fdac = 44.1  kHz
#define TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_8_021kHz_DAC_44_1kHz  \
        TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_44_1kHz_DAC_8_021kHz+1

/// \brief Define last option in sample rate list
#define TLV_SAMPLE_RATE_LAST_OPTION     \
                TLV_SAMPLE_RATE_STATE_MCLK_16_9344MHz_ADC_8_021kHz_DAC_44_1kHz
/// @}

//=================================| Macros |==================================

//===========================| Functions for user |============================

//==========================| High level functions |===========================
GD_RES_CODE TLV_init(void);
/*---------------------------------------------------------------------------*
 |                                                                           |
 *---------------------------------------------------------------------------*/
GD_RES_CODE TLV_set_volume_in_L(uint8_t i_volume_value);
GD_RES_CODE TLV_set_volume_in_R(uint8_t i_volume_value);
GD_RES_CODE TLV_set_volume_in_LR(uint8_t i_volume_value);
GD_RES_CODE TLV_set_volume_in(uint8_t i_channel, uint8_t i_volume_value);

#endif
