/**
 * \file
 *
 * \brief Hardware Abstraction Layer for codec tlv320aic23b running on\n
 * AVR32 UC3A3 architecture
 *
 * Contains basic low level functions which depend on user architecture and\n
 * hardware. Thanks to this layer is possible use common higher level driver\n
 * and apply it thru many architectures and devices. Also this allow easy\n
 * updates independent to higher layer (new features and so on).
 *
 * Created:  10.03.2014\n
 * Modified: 11.03.2014
 *
 * \TODO UPDATE prescaller calculations
 * \version 0.1.1
 * \author Martin Stejskal
 */


#ifndef _TLV320AIC23B_HAL_AVR32_UC3A3_HW_INTERFACE_H_
#define _TLV320AIC23B_HAL_AVR32_UC3A3_HW_INTERFACE_H_

//===========================| Included libraries |============================
// Basic library (I/O)
#include <avr32/io.h>

// Data types
#include <inttypes.h>

// Allow to set I/O pins
///\todo Try to remove this dependency
#include "gpio.h"

// Because of common structures, which are shared -> cross include
#include "tlv320aic23b.h"

//==============================| User settings |==============================
/**
 * \name Basic settings for TWI
 *
 * @{
 */
/**
 * \brief TWI interface
 *
 * Aka (*((volatile avr32_twi_t*)AVR32_TWI_ADDRESS)) or (&AVR32_TWI)\n
 * Example: TLV320AIC23B_TWI_DEVICE (&AVR32_TWI)
 */
#define TLV320AIC23B_TWI_DEVICE         (&AVR32_TWIM0)
/**
 * \brief Clock speed in Hz
 *
 * User should define at least approximately speed of clock on which is TWI\n
 * module connected. This value is used to calculate correct values in\n
 * registers. After reset, MCU use slow oscillator, which in case UC3A0512\n
 * runs at 115 kHz. If your clock source is variable, then is recommended\n
 * set this constant to maximum used frequency. Anyway, if you want use\n
 * maximum possible speed, just set LCD_5110_SPI_BAUDRATE to 0 and\n
 * preprocessor do the rest :)\n
 * Example: TLV320AIC23B_TWI_CLK_SOURCE 115000UL
 */
#define TLV320AIC23B_TWI_CLK_SOURCE     12000000UL

/**
 * \brief TWI address of TLV320AIC23B
 *
 * Because this depend on hardware settings, it is up to user to set correct\n
 * address.\n
 * \note When CS=0 -> address is 0b0011010, else 0b0011011
 */
#define TLV320AIC23B_TWI_ADDRESS        0b0011010
/**
 * @}
 */

/**
 * \brief Definition of TWI (I2C) pins
 *
 * There are defined pins and their functions
 * @{
 */
/**
 * \brief Pin for TWI data signal
 *
 * Example: TLV320AIC23B_TWI_SDA_PIN AVR32_TWI_SDA_0_0_PIN
 */
#define TLV320AIC23B_TWI_SDA_PIN         AVR32_TWIMS0_TWD_0_0_PIN

/**
 * \brief Function for TWI data signal
 *
 * Example: TLV320AIC23B_TWI_SDA_FUNCTION AVR32_TWI_SDA_0_0_FUNCTION
 */
#define TLV320AIC23B_TWI_SDA_FUNCTION    AVR32_TWIMS0_TWD_0_0_FUNCTION

/**
 * \brief Pin for TWI clock signal
 *
 * Example: TLV320AIC23B_TWI_SCL_PIN AVR32_TWI_SCL_0_0_PIN
 */
#define TLV320AIC23B_TWI_SCL_PIN         AVR32_TWIMS0_TWCK_0_0_PIN

/**
 * \brief Function for TWI clock signal
 *
 * Example: TLV320AIC23B_TWI_SCL_FUNCTION AVR32_TWI_SCL_0_0_FUNCTION
 */
#define TLV320AIC23B_TWI_SCL_FUNCTION    AVR32_TWIMS0_TWCK_0_0_FUNCTION
/**
 * @}
 */



/** \name Advanced settings TWI interface
 * @{
 */
/**
 * \brief Baudrate in Hz - in real should not exceed 400 kb/s
 *
 * Too low speed decrease whole application performance.\n
 * For \b maximum \b performance is \b recommended to set this value \b to\n
 * \b 0. Library itself will try to set maximum possible baudrate.\n
 * However it can not be ideal solution. When any other devices are connected\n
 * to this TWI bus, they may not support this speed! So for better\n
 * compatibility is recommended to set this value to 100000UL. Performance\n
 * will be still good and any other devices should be able to work on this\n
 * speed.\n
 * Example: LCD_5110_SPI_BAUDRATE 100000UL
 */
#define TLV320AIC23B_TWI_BAUDRATE        100000UL




/**
 * @}
 */

//! \brief Define where settings will be saved (first word, length is 20B)
#define TLV320AIC23B_EEPROM_START_ADDRESS        0x02ED

//============================| Structures, enums |============================
/**
 * \brief Enumeration of states, which can function returns
 *
 * Generally if function return non-zero value then no problem occurs. This\n
 * enum also use tlv320aic23b driver (higher layer of this driver).
 */
typedef enum{
  TLV320AIC23B_ERROR = -1,       //!< Unspecified error
  TLV320AIC23B_OK = 0,           //!< All OK
  TLV320AIC23B_ERROR_TWI_TIMEOUT = 1,/**!< Timeout occurs - when waiting for\n
                                       * complete TX data
                                       */
  TLV320AIC23B_ERROR_TWI_INVALID_PARAMETER = 2,/**!< Invalid parameter when
                                            * configure TWI interface
                                            */
  TLV320AIC23B_ERROR_TWI_NACK = 3      //!< When NACK occurs
} tlv320aic23b_status_t;

//===============================| Structures |================================
/**
 * \brief Structure for TLV flags and settings
 *
 * Because saving configure flags as bytes would consume a lot of memory,\n
 * it is better use structure with masking flags.\n
 * Because it is hard to rewrite whole code to give functions this structure\n
 * as argument, this value is global (at least for this file). In the end it\n
 * save a little memory, because this variable is there "for ever" in\n
 * application run.
 */
typedef struct{
  // LEFT INPUT
  uint8_t i_L_in_volume;
  uint8_t i_L_in_mute;

  // RIGHT INPUT
  uint8_t i_R_in_volume;
  uint8_t i_R_in_mute;

  // LEFT OUTPUT
  uint8_t i_L_out_headphone_volume;
  uint8_t i_L_out_headphone_zero_cross;

  // RIGHT OUTPUT
  uint8_t i_R_out_headphone_volume;
  uint8_t i_R_out_headphone_zero_cross;

  // --- Analog audio path ---
  uint8_t i_dac_select;
  uint8_t i_bypass_in_to_out;
  uint8_t i_mic_sidetone_to_in_and_out;
  uint8_t i_input_select;
  uint8_t i_mic_mute;
  uint8_t i_mic_boost_20dB;

  // --- Digital audio path ---
  uint8_t i_dac_soft_mute;
  uint8_t i_deemp;
  uint8_t i_adc_high_pass_filter;

  // --- Power down control ---
  uint8_t i_device_pwr_down;
  uint8_t i_clock_pwr_down;
  uint8_t i_oscillator_pwr_down;
  uint8_t i_outputs_pwr_down;
  uint8_t i_dac_pwr_down;
  uint8_t i_adc_pwr_down;
  uint8_t i_mic_pwr_down;
  uint8_t i_line_pwr_down;

  // --- Audio interface format ---
  uint8_t i_master_mode;
  uint8_t i_dac_left_right_swap;
  // DAC LR PHASE -> please refer TLV320AIC23B datasheet
  uint8_t i_dac_left_right_phase;
  uint8_t i_input_bit_length;
  uint8_t i_data_format;

  // --- SAMPLE RATE ---
  uint8_t i_clk_in_divider;
  uint8_t i_clk_out_divider;
  // SAMPLING RATE CONTROLL -> please refer TLV320AIC23B datasheet
  uint8_t i_sampling_rate;
  // BASE OVERSAMPLING RATE -> please refer TLV320AIC23B datasheet
  uint8_t i_base_oversampling_rate;
  // CLOCK MODE SELECT -> please refer TLV320AIC23B datasheet
  uint8_t i_clock_mode_select;    // 0 - normal ; 1 - USB

  // --- Digital interface activation ---
  uint8_t i_digital_interface_activation;
}t_tlv_options;

//================| Some checks and preprocessor calculations |================
// Test TLV address
#if (TLV320AIC23B_TWI_ADDRESS != 0b0011010) &&\
    (TLV320AIC23B_TWI_ADDRESS != 0b0011011)
#warning "TLV320AIC23B_TWI_ADDRESS is not set to one of known settings. It is\
  highly possible that communication with codec will fail!"
#endif

// Test address - can not be higher than 7bits
#if TLV320AIC23B_TWI_ADDRESS > 127
#error "TLV320AIC23B_TWI_ADDRESS can not be higher than 127, because\
  address in TWI is 7 bit long. Please set correct value."
#endif



// Test if user want auto maximum possible speed
#if TLV320AIC23B_TWI_BAUDRATE == 0
#undef TLV320AIC23B_TWI_BAUDRATE
  /* Test if actual master clock is lower than maximum for TWI -> can not
   * work on maximal possible speed. So disable all dividers
   */
#if TLV320AIC23B_TWI_CLK_SOURCE < (400000*5)
  // If clock frequency is lower the max SCL frequency *2
#define TLV320AIC23B_TWI_BAUDRATE       (TLV320AIC23B_TWI_CLK_SOURCE/2)
#else
  // Else clock is higher -> set baudrate to maximum
#define TLV320AIC23B_TWI_BAUDRATE       400000UL
#endif
#endif


// Check for high-speed baudrate
#if TLV320AIC23B_TWI_BAUDRATE > 100000UL
#warning "TLV320AIC23B_TWI_BAUDRATE is higher than 100 000. Make sure that\
  your hardware is well designed and that all other devices connected on TWI\
  bus support higher speed than 100 000 kHz."
#endif
// Check if baudrate is not higher than 400000 -> may not work -> warning
#if TLV320AIC23B_TWI_BAUDRATE > 400000UL
#warning "TLV320AIC23B_TWI_BAUDRATE is higher than 400 000, so communication\
  with codec may fail!"
#endif

// Check for too low baudrate
#if TLV320AIC23B_TWI_BAUDRATE < 10000UL
#warning "TLV320AIC23B_TWI_BAUDRATE is lower than 10 000. This may cause, that\
  performance of whole application will be low! Please consider to use higher\
  speed for TWI."
#endif


  // Test if set baudrate is not higher than clock speed
#if TLV320AIC23B_TWI_BAUDRATE > (TLV320AIC23B_TWI_CLK_SOURCE*2)
#error "TWI baudrate can not be higher than (input clock)/2 for TWI module.\
  Please set TLV320AIC23B_TWI_BAUDRATE to lower value, or set it to 0 and\
  preprocessor try set maximum possible speed."
#endif


// Calculate prescaller
#if (TLV320AIC23B_TWI_CLK_SOURCE/TLV320AIC23B_TWI_BAUDRATE/2)<256
#define TLV320AIC23B_TWI_CKDIV        0
#define TLV320AIC23B_TWI_CLDIV        \
  (TLV320AIC23B_TWI_CLK_SOURCE/TLV320AIC23B_TWI_BAUDRATE/2)

#elif (TLV320AIC23B_TWI_CLK_SOURCE/TLV320AIC23B_TWI_BAUDRATE/4)<256
#define TLV320AIC23B_TWI_CKDIV        1
#define TLV320AIC23B_TWI_CLDIV        \
  (TLV320AIC23B_TWI_CLK_SOURCE/TLV320AIC23B_TWI_BAUDRATE/4)

#elif (TLV320AIC23B_TWI_CLK_SOURCE/TLV320AIC23B_TWI_BAUDRATE/8)<256
#define TLV320AIC23B_TWI_CKDIV        2
#define TLV320AIC23B_TWI_CLDIV        \
  (TLV320AIC23B_TWI_CLK_SOURCE/TLV320AIC23B_TWI_BAUDRATE/8)

#elif (TLV320AIC23B_TWI_CLK_SOURCE/TLV320AIC23B_TWI_BAUDRATE/16)<256
#define TLV320AIC23B_TWI_CKDIV        3
#define TLV320AIC23B_TWI_CLDIV        \
  (TLV320AIC23B_TWI_CLK_SOURCE/TLV320AIC23B_TWI_BAUDRATE/16)

#elif (TLV320AIC23B_TWI_CLK_SOURCE/TLV320AIC23B_TWI_BAUDRATE/32)<256
#define TLV320AIC23B_TWI_CKDIV        4
#define TLV320AIC23B_TWI_CLDIV        \
  (TLV320AIC23B_TWI_CLK_SOURCE/TLV320AIC23B_TWI_BAUDRATE/32)

#elif (TLV320AIC23B_TWI_CLK_SOURCE/TLV320AIC23B_TWI_BAUDRATE/64)<256
#define TLV320AIC23B_TWI_CKDIV        5
#define TLV320AIC23B_TWI_CLDIV        \
  (TLV320AIC23B_TWI_CLK_SOURCE/TLV320AIC23B_TWI_BAUDRATE/64)

#elif (TLV320AIC23B_TWI_CLK_SOURCE/TLV320AIC23B_TWI_BAUDRATE/128)<256
#define TLV320AIC23B_TWI_CKDIV        6
#define TLV320AIC23B_TWI_CLDIV        \
  (TLV320AIC23B_TWI_CLK_SOURCE/TLV320AIC23B_TWI_BAUDRATE/128)

#elif (TLV320AIC23B_TWI_CLK_SOURCE/TLV320AIC23B_TWI_BAUDRATE/256)<256
#define TLV320AIC23B_TWI_CKDIV        7
#define TLV320AIC23B_TWI_CLDIV        \
  (TLV320AIC23B_TWI_CLK_SOURCE/TLV320AIC23B_TWI_BAUDRATE/256)

#else
  // When maximum division is not enough
#error "TWI baudrate can not be set this low. Please set\
  TLV320AIC23B_TWI_BAUDRATE to higher value, or set it  to 0 and\
  preprocessor try set maximum possible speed."
#endif


//================================| Functions |================================
/**
 * \brief Initialize TWI module on chip
 *
 * Set TWI module as master. Set I/O, enable clock to module and so on
 * @return TLV320AIC23B_OK (0) if all OK
 */
tlv320aic23b_status_t tlv320aic23b_HAL_init(void);

/**
 * \brief Send 16bit data to codec
 * @param i_data Data which will be send to codec (register address + value)
 * @return TLV320AIC23B_OK (0) if all OK
 */
tlv320aic23b_status_t tlv320aic23b_HAL_send_data(uint16_t i_data);

/**
 * \brief Read configuration from EEPROM or flash memory
 *
 * Because 32bit AVR do not have EEPROM, there is incompatibility with 8 bit\n
 * AVR. So reading and writing settings is up to HAL to deal with it.
 *
 * @param p_tlv_options Pointer to configuration structure. To this structure\n
 *  will be settings loaded. Definition of t_tlv_options is in higher layer\n
 *  due to better compatibility.
 *
 * @return TLV320AIC23B_OK (0) if all OK. If non-valid configuration data are\n
 *  detected -> return TLV320AIC23B_ERROR (-1)
 */
tlv320aic23b_status_t tlv320aic23b_HAL_read_config(
                                                uint16_t *p_tlv_options);

/**
 * \brief Write configuration to EEPROM or flash memory
 *
 * Because 32bit AVR do not have EEPROM, there is incompatibility with 8 bit\n
 * AVR. So reading and writing settings is up to HAL to deal with it.
 *
 * @param p_tlv_options Pointer to configuration structure. From this\n
 * structure will be settings loaded. Definition of t_tlv_options is in\n
 * higher layer due to better compatibility.
 *
 * @return TLV320AIC23B_OK (0) if all OK. Else return TLV320AIC23B_ERROR (-1)\n
 * if there is some problem with writing to memory.
 */
tlv320aic23b_status_t tlv320aic23b_HAL_write_config(
                                                t_tlv_options *p_tlv_options);
#endif
