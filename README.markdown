# Driver for codec TLV320AIC23B

## Description
 This is generic library for codec from TI. Library is split into two parts:
 
 * High level driver
 * HAL driver
 
 Meanwhile high level driver is common for all platform, HAL is hardware
 depend. All HAL drivers should contain same functions, but implementation
 may be different.

## Files
 * tlv320aic23b.* - generic high level drivers for module. It need include HAL
 * tlv320aic23b_HAL_* - HAL driver

## Usage
 In tlv320aic23b.h, section "Included libraries" include correct HAL driver.
 For example: if you use AVR32 UC3A3, then type:
  #include "tlv320aic23b_HAL_AVR32_UC3A0_HW_interface.h"
 When there is not HAL driver for your favorite MCU, then try write one ;)

## Get source code! 
 `git clone https://MartinStejskal@bitbucket.org/MartinStejskal/tlv320aic23-driver.git`
 
## Notes
 * HAL driver should contain only basic libraries
