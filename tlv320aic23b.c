/**
 * \file
 *
 * \brief Generic driver audio codec TLV320AIC23B
 *
 * Used for basic operations (init, set volume, mute, ...) with audio codec\n
 * TLV320AIC23B.\n
 * It need HAL driver (which depend on used architecture) to work properly.\n
 *
 * Created:  14.12.2013
 * Modified: 28.12.2013
 *
 * \version 0.1.1
 * \author Martin Stejskal
 */

#include "tlv320aic23b.h"

//============================| Global variables |=============================
/// \brief Status "register". When not "0", then there is problem
static uint8_t i_status = 0;

/// \brief Structure for settings
static t_tlv_options t_tlv_settings;

/**
 * \brief Structure for "virtual settings" - data are not directly in TLV\n
 * registers
 */
static t_tlv_virtual_options t_tlv_virtual_settings;

/**
 * \name Backup variables
 *
 * \brief When we mute one of out channels, then we need backup original\n
 *  value (because mute means: "Set low volume"). So there they are...
 *
 * @{
 */
//! \brief Backup variable for left out channel
static uint8_t i_volume_out_backup_L;
//! \brief Backup variable for right out channel
static uint8_t i_volume_out_backup_R;
/// @}




#if TLV_support_generic_driver == 1
/**
 * \brief Configure table for device
 */
#ifndef __PGMSPACE_H_
// If not defined -> pgmspace not used -> just as constant
const gd_config_struct BRD_DRV_config_table_flash[] =

#else   // If pgmspace is included -> store data to flash (8 bit AVR)
const gd_config_struct BRD_DRV_config_table_flash[] PROGMEM =
#endif
// Configuration table for device

#endif

GD_RES_CODE TLV_init(void)
{
  i_status = 1;
  t_tlv_settings.i_L_out_headphone_zero_cross = 0;
  t_tlv_virtual_settings.i_L_headphone_mute_flag = 1;
  i_volume_out_backup_L = i_volume_out_backup_R = 25;
  return TLV320AIC23B_OK;
}
