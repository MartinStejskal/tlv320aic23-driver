/**
 * \file
 *
 * \brief Hardware Abstraction Layer for codec tlv320aic23b running on\n
 * AVR32 UC3A3 architecture
 *
 * Contains basic low level functions which depend on user architecture and\n
 * hardware. Thanks to this layer is possible use common higher level driver\n
 * and apply it thru many architectures and devices. Also this allow easy\n
 * updates independent to higher layer (new features and so on).
 *
 * Created:  10.03.2014\n
 * Modified: 11.03.2014
 *
 * \TODO UPDATE prescaller calculations
 * \version 0.1.1
 * \author Martin Stejskal
 */

#include "tlv320aic23b_HAL_AVR32_UC3A3_HW_interface.h"

//=================| Definition which user should not change |=================
/**
 * \brief Define when timeout occurs
 *
 * When waiting for send rest of TX data this constant define how long should\n
 * function should wait. Value should be calculated for most applications.\n
 * However user can set it explicitly here, but it is not recommended.
 */
#define TLV320AIC23B_TWI_TIMEOUT        \
  (250*(TLV320AIC23B_TWI_CLK_SOURCE/TLV320AIC23B_TWI_BAUDRATE))

//================================| Functions |================================
tlv320aic23b_status_t tlv320aic23b_HAL_init(void)
{
  // For backup IRQ flags
  uint32_t flags;

  // Variable as mask
  uint32_t   mask;

  // Pointer to TWI address
  volatile avr32_twim_t *p_twi;
  p_twi = TLV320AIC23B_TWI_DEVICE;


  //=================================| GPIO |==================================

  // Set pins for SPI0 device
  static const gpio_map_t TLV320AIC23B_GPIO_MAP = {
      {TLV320AIC23B_TWI_SDA_PIN,     TLV320AIC23B_TWI_SDA_FUNCTION},
      {TLV320AIC23B_TWI_SCL_PIN,    TLV320AIC23B_TWI_SCL_FUNCTION}
  };

  // Assign I/O to SPI
  if( gpio_enable_module(
      TLV320AIC23B_GPIO_MAP,
                sizeof(TLV320AIC23B_GPIO_MAP) / sizeof(TLV320AIC23B_GPIO_MAP[0]))
      != TLV320AIC23B_OK)
  {
    return TLV320AIC23B_ERROR_TWI_INVALID_PARAMETER;
  }

  //==================================| PM |===================================
  // Enable clock do device (in default should be on, but just for case)

  // Get and clear global interrupt
  flags = __builtin_mfsr(AVR32_SR);
  // Disable IRQ
  __builtin_ssrf(AVR32_SR_GM_OFFSET);
  asm volatile("" ::: "memory");

  /*
   * Poll MSKRDY before changing mask rather than after, as it's
   * highly unlikely to actually be cleared at this point.
   */
  while (!(AVR32_PM.poscsr & (1U << AVR32_PM_POSCSR_MSKRDY))) {
          /* Do nothing */
  }

  // Enable the clock to flash and PBA bridge
  mask = *(&AVR32_PM.cpumask + AVR32_PM_CLK_GRP_HSB);
  mask |= 1U << (AVR32_FLASHC_CLK_HSB % 32);
  mask |= 1U << (AVR32_HMATRIX_CLK_HSB_PBA_BRIDGE % 32);
  *(&AVR32_PM.cpumask + AVR32_PM_CLK_GRP_HSB) = mask;

  // Enable clock to TWI and GPIO in PBA
  mask = *(&AVR32_PM.cpumask + AVR32_PM_CLK_GRP_PBA);
  mask |= 1U << (AVR32_TWIM0_CLK_PBA % 32);
  mask |= 1U << (AVR32_GPIO_CLK_PBA % 32);
  *(&AVR32_PM.cpumask + AVR32_PM_CLK_GRP_PBA) = mask;

  // Restore global interrupt flags
  asm volatile("" ::: "memory");
  __builtin_csrf(AVR32_SR_GM_OFFSET);
  asm volatile("" ::: "memory");


  //==================================| TWI |==================================
  // Configure TWI module

  // Control register

  // Reset TWI module
  p_twi->CR.swrst = 1;

  // Disable all interrupts
  p_twi->ier = 0;
  p_twi->idr = ~0;

  // Disable SMBus
  p_twi->CR.smdis = 1;
  p_twi->CR.smen  = 0;

  // Set master mode
  p_twi->CR.mdis = 0;
  p_twi->CR.men  = 1;

  // Clock waveform generator register (set baudrate/speed)
  // TWI prescaller
  p_twi->CWGR.exp    = TLV320AIC23B_TWI_CKDIV;
  // Time space in start and stop condition. At least should be 1
  p_twi->CWGR.stasto = TLV320AIC23B_TWI_CLDIV;
  // Extra time on every data bit.
  p_twi->CWGR.data   = 0;
  // Time for high level
  p_twi->CWGR.high   = TLV320AIC23B_TWI_CLDIV;
  // Time for low level
  p_twi->CWGR.low    = TLV320AIC23B_TWI_CLDIV;

  // When in master read mode, then last byte should not be ACKed
  p_twi->CMDR.acklast = 0;
  // 7 bit addressing space
  p_twi->CMDR.tenbit = 0;

  // OK, all should be ready

  return TLV320AIC23B_OK;
}



tlv320aic23b_status_t tlv320aic23b_HAL_send_data(uint16_t i_data)
{
  // Pointer to TWI address
  volatile avr32_twim_t *p_twi;
  p_twi = TLV320AIC23B_TWI_DEVICE;

  // Counter for timeout
  volatile uint32_t i_cnt_timeout = 0;

  // Clear status register
  p_twi->scr = ~0;
  

  // Wait until device is idle
  while(!p_twi->SR.idle)
  {
    // Increase timeout counter
    i_cnt_timeout++;
    // Test timeout counter
    if(i_cnt_timeout > TLV320AIC23B_TWI_TIMEOUT)
    {
      // If higher -> return error value
      return TLV320AIC23B_ERROR_TWI_TIMEOUT;
    }
  }

  // Reset counter - just for case
  i_cnt_timeout = 0;




  // Backup slave address
  i_backup_address = p_twi->CMDR.sadr;

  // Set slave address - must calculate with option that TWI use another driver
  p_twi->CMDR.sadr = TLV320AIC23B_TWI_ADDRESS;

    // Set number of bytes to send
  p_twi->CMDR.nbytes = 2;
  
  p_twi->CMDR.start = 1;
  p_twi->CMDR.stop = 1;

  // Want transmit data
  p_twi->CMDR.read = 0;

  // Data in CMDR are valid
  p_twi->CMDR.valid = 1;

  // Send first byte (MSB first)
  p_twi->THR.txdata = i_data>>8;


  // Wait until TX is done - during this, check for NACK (data)
  while(!p_twi->SR.txrdy)
  {
    // Again, check for timeout
    // Increase timeout counter
    i_cnt_timeout++;
    // Test timeout counter
    if(i_cnt_timeout > TLV320AIC23B_TWI_TIMEOUT)
    {
      // Restore slave address
      p_twi->CMDR.sadr = i_backup_address;

      // If higher -> return error value
      return TLV320AIC23B_ERROR_TWI_TIMEOUT;
    }

    // Check for NACK
    if(p_twi->SR.anak || p_twi->SR.dnak)
    {
      // Restore slave address
      p_twi->CMDR.sadr = i_backup_address;

      return TLV320AIC23B_ERROR_TWI_NACK;
    }
  }
  // Reset counter - just for case
  i_cnt_timeout = 0;



  // Send next byte (LSB last)
  p_twi->THR.txdata = i_data & 0xFF;

  // Wait until TX is done - during this, check for NACK (data)
  while(!p_twi->SR.txrdy)
  {
    // Again, check for timeout
    // Increase timeout counter
    i_cnt_timeout++;
    // Test timeout counter
    if(i_cnt_timeout > TLV320AIC23B_TWI_TIMEOUT)
    {
      // Restore slave address
      p_twi->CMDR.sadr = i_backup_address;

      // If higher -> return error value
      return TLV320AIC23B_ERROR_TWI_TIMEOUT;
    }

    // Check for NACK
    if(p_twi->SR.anak || p_twi->SR.dnak)
    {
      // Restore slave address
      p_twi->CMDR.sadr = i_backup_address;

      return TLV320AIC23B_ERROR_TWI_NACK;
    }
  }

  // Restore slave address
  p_twi->CMDR.sadr = i_backup_address;

  return TLV320AIC23B_OK;
}




/*
tlv320aic23b_status_t tlv320aic23b_HAL_read_config(
                                                t_tlv_options *p_tlv_options)
{
  // So far there is not any implementation -> return error
  return TLV320AIC23B_ERROR;
}


tlv320aic23b_status_t tlv320aic23b_HAL_write_config(
                                                t_tlv_options *p_tlv_options)
{
  // So far there is not any implementation -> return error
  return TLV320AIC23B_ERROR;
}
*/

